#!/bin/sh

SCRIPT_NAME="$(basename ${0})"

TEST_DIR=""
CURR_PWD=""

show_help () {
    echo "Usage: ${SCRIPT_NAME} DIRECTORY"
    echo ""
    echo "Script to initialize directory in order to perform the base fsc test"
}

if [ ${#} -ne 1 ]; then
    show_help
    exit 1
fi

TEST_DIR="${1}"
CURR_PWD="${PWD}"

cd "${TEST_DIR}"

# file to remove
touch "file-to-del"

# directory to remove
mkdir "dir-to-del"

# directory to do 'rename' on
mkdir "dir-to-move"

# file of size 128 MB to do 'rename' on
dd if="/dev/urandom" of="file-to-move" bs=1M count=128

cd "${CURR_PWD}"
