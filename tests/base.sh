#!/bin/sh

SCRIPT_NAME="$(basename ${0})"

TEST_DIR=""
CURR_PWD=""

show_help () {
    echo "Usage: ${SCRIPT_NAME} DIRECTORY"
    echo ""
    echo "Script to execute the base fsc test"
}

if [ ${#} -ne 1 ]; then
    show_help
    exit 1
fi

TEST_DIR="${1}"
CURR_PWD="${PWD}"

cd "${TEST_DIR}"

# remove the file
rm "file-to-del"

# remove the directory
rmdir "dir-to-del"

# create new directory
mkdir "dir-new"

# rename the directory
mv "dir-to-move" "dir-new/dir-moved"

# rename the file
mv "file-to-move" "dir-new/file-moved"

cd "${CURR_PWD}"
