#ifndef FSC_OPERATIONS_H
#define FSC_OPERATIONS_H

struct _fsc_operation_t;
typedef struct _fsc_operation_t fsc_operation_t;

#include "fsc.h"

#include <sys/time.h>

enum FSC_OPERATION_TYPE {
    FSC_OPERATION_TYPE_MKDIR,           ///< Directory creation
    FSC_OPERATION_TYPE_UNLINK,          ///< File removal
    FSC_OPERATION_TYPE_RMDIR,           ///< Directory removal
    FSC_OPERATION_TYPE_RENAME           ///< File or directory renaming
};
typedef enum FSC_OPERATION_TYPE FSC_OPERATION_TYPE_T;

/* Type + arguments for all handled FUSE operations */
struct _fsc_operation_t {
    fsc_t *fsc;
    FSC_OPERATION_TYPE_T type;
    char *path, *newpath;
    mode_t mode;
    uid_t uid;
    gid_t gid;
    off_t offset;
    struct timeval atime, mtime;
};

/**
 * Operation struture constructor
 *
 * @param fsc               fsc structure
 * @param path              Path to the file
 * @param type              Type of the operation
 * @return                  Initialized operation structure on success, NULL otherwise
 */
fsc_operation_t* fsc_operation_new (fsc_t *fsc, const char *path,
                                    FSC_OPERATION_TYPE_T type);

/**
 * Operation structure destructor
 *
 * @param op                Operation structure to be freed
 */
void fsc_operation_free (fsc_operation_t *op);

/**
 * Add operation
 *
 * @param fsc               fsc structure
 * @param op                Operation structure to be added
 * @return                  0 on success, non-zero otherwise
 */
int fsc_operation_add (fsc_t *fsc, fsc_operation_t *op);

/**
 * Run the operation
 *
 * @param op                Operation to be run
 * @return                  0 on success, non-zero otherwise
 */
int fsc_operation_run (fsc_operation_t *op);

/**
 * Run all operations
 *
 * @param fsc               fsc structure
 * @return                  0 on success, non-zero otherwise
 */
int fsc_operations_run (fsc_t *fsc);

/**
 * Add directory creation to operations list
 *
 * @param fsc               fsc structure
 * @param path              Path to the directory
 * @param mode              Mode for new directory
 * @return                  0 on success, non-zero otherwise
 */
int fsc_operation_mkdir (fsc_t *fsc, const char *path, mode_t mode);

/**
 * Add file removal to operations list
 *
 * @param fsc               fsc structure
 * @param path              Path to the file
 * @return                  0 on success, non-zero otherwise
 */
int fsc_operation_unlink (fsc_t *fsc, const char *path);

/**
 * Add directory removal to operations list
 *
 * @param fsc               fsc structure
 * @param path              Path to the directory
 * @return                  0 on success, non-zero otherwise
 */
int fsc_operation_rmdir (fsc_t *fsc, const char *path);

/**
 * Add file or directory renaming to operations list
 *
 * @param fsc               fsc structure
 * @param path              Path to the file
 * @param newpath           New path to the file
 * @return                  0 on success, non-zero otherwise
 */
int fsc_operation_rename (fsc_t *fsc, const char *path,
                          const char *newpath);

#endif

