#ifndef FSC_SYSCALLS_H
#define FSC_SYSCALLS_H

struct fuse_operations fsc_operations;

/**
 * Get file attributes.
 *
 * Similar to stat().  The 'st_dev' and 'st_blksize' fields are
 * ignored.	 The 'st_ino' field is ignored except if the 'use_ino'
 * mount option is given.
 *
 * @param path          Path to the file
 * @param st            File information to be filled (see 'man stat')
 * @return              0 on success, negative error code otherwise
 */
int fsc_getattr (const char *path, struct stat *st);

/**
 * Read the target of a symbolic link
 *
 * The buffer should be filled with a null terminated string.  The
 * buffer size argument includes the space for the terminating
 * null character.	If the linkname is too long to fit in the
 * buffer, it should be truncated.	The return value should be 0
 * for success.
 *
 * @param path          Path to the file
 * @param buf           Buffer to be filled with resolved path
 * @param bufsize       Size of the buffer
 * @return              0 on success, negative error code otherwise
 */
int fsc_readlink (const char *path, char *buf, size_t bufsize);

/**
 * Create a file node
 *
 * This is called for creation of all non-directory, non-symlink
 * nodes.  If the filesystem defines a create() method, then for
 * regular files that will be called instead.
 *
 * @param path          Path to the file
 * @param mode          Mode and type of the new node
 * @param dev           Major and minor numbers of the newly created
 *                      device special file
 * @return              0 on success, negative error code otherwise
 */
int fsc_mknod (const char *path, mode_t mode, dev_t dev);

/**
 * Create a directory
 *
 * Note that the mode argument may not have the type specification
 * bits set, i.e. S_ISDIR(mode) can be false.  To obtain the
 * correct directory type bits use  mode|S_IFDIR
 *
 * @param path          Path to the file
 * @param mode          Mode of the new directory
 * @return              0 on success, negative error code otherwise
 */
int fsc_mkdir (const char *path, mode_t mode);

/**
 * Remove a file
 *
 * @param path          Path to the file
 * @return              0 on success, negative error code otherwise
 */
int fsc_unlink (const char *path);

/**
 * Remove a directory
 *
 * @param path          Path to the file
 * @return              0 on success, negative error code otherwise
 */
int fsc_rmdir (const char *path);

/**
 * Create a symbolic link
 *
 * @param path          Path to the file
 * @param newpath       Path to the new symbolic link
 * @return              0 on success, negative error code otherwise
 */
int fsc_symlink (const char *path, const char *newpath);

/** Rename a file
 *
 * @param path          Path to the file
 * @param newpath       New path to the file
 * @return              0 on success, negative error code otherwise
 */
int fsc_rename (const char *path, const char *newpath);

/**
 * Create a hard link to a file
 *
 * @param path          Path to the file
 * @param newpath       PAth to the new link
 * @return              0 on success, negative error code otherwise
 * */
int fsc_link (const char *path, const char *newpath);

/**
 * Change the permission bits of a file
 *
 * @param path          Path to the file
 * @param mode          New mode of the file
 * @return              0 on success, negative error code otherwise
 */
int fsc_chmod (const char *path, mode_t mode);

/**
 * Change the owner and group of a file
 *
 * @param path          Path to the file
 * @param uid           User ID to be set to the file
 * @param gid           Group ID to be set to the file
 * @return              0 on success, negative error code otherwise
 */
int fsc_chown (const char *path, uid_t uid, gid_t gid);

/**
 * Change the size of a file
 *
 * @param path          Path to the file
 * @param length        New size of the file
 * @return              0 on success, negative error code otherwise
 */
int fsc_truncate (const char *path, off_t length);

/**
 * File open operation
 *
 * No creation (O_CREAT, O_EXCL) and by default also no
 * truncation (O_TRUNC) flags will be passed to open(). If an
 * application specifies O_TRUNC, fuse first calls truncate()
 * and then open(). Only if 'atomic_o_trunc' has been
 * specified and kernel version is 2.6.24 or later, O_TRUNC is
 * passed on to open.
 *
 * Unless the 'default_permissions' mount option is given,
 * open should check if the operation is permitted for the
 * given flags. Optionally open may also return an arbitrary
 * filehandle in the fuse_file_info structure, which will be
 * passed to all file operations.
 *
 * Changed in version 2.2
 *
 * @param path          Path to the file
 * @param file_info     Pointer to FUSE file structure to be filled
 *                      and use later
 * @return              0 on success, negative error code otherwise
 */
int fsc_open (const char *path, struct fuse_file_info *file_info);

/**
 * Read data from an open file
 *
 * Read should return exactly the number of bytes requested except
 * on EOF or error, otherwise the rest of the data will be
 * substituted with zeroes.	 An exception to this is when the
 * 'direct_io' mount option is specified, in which case the return
 * value of the read system call will reflect the return value of
 * this operation.
 *
 * Changed in version 2.2
 *
 * @param path          Path to the file
 * @param buf           Buffer for data read from file
 * @param size          Requested amount of data to be read
 * @param offset        Offset in the file to be used
 * @param file_info     Pointer to FUSE file structure
 * @return              Number of read bytes on success, negative
 *                      error code otherwise
 */
int fsc_read (const char *path, char *buf, size_t size, off_t offset,
              struct fuse_file_info *file_info);

/**
 * Write data to an open file
 *
 * Write should return exactly the number of bytes requested
 * except on error.	 An exception to this is when the 'direct_io'
 * mount option is specified (see read operation).
 *
 * Changed in version 2.2
 *
 * @param path          Path to the file
 * @param buf           Buffer with data to be written to the file
 * @param size          Requested amount of data to be written
 * @param offset        Offset in the file to be used
 * @param file_info     Pointer to FUSE file structure
 * @return              Number of written bytes on success, negative
 *                      error code otherwise
 */
int fsc_write (const char *path, const char *buf, size_t size, off_t offset,
               struct fuse_file_info *file_info);

/**
 * Get file system statistics
 *
 * The 'f_favail', 'f_fsid' and 'f_flag' fields are ignored
 *
 * @param path          Path to the file
 * @param st            Filesystem information to be filled
 *                      (see 'man statfs')
 * @return              0 on success, negative error code otherwise
 */
int fsc_statfs (const char *path, struct statvfs *st);

/**
 * Possibly flush cached data
 *
 * BIG NOTE: This is not equivalent to fsync().  It's not a
 * request to sync dirty data.
 *
 * Flush is called on each close() of a file descriptor.  So if a
 * filesystem wants to return write errors in close() and the file
 * has cached dirty data, this is a good place to write back data
 * and return any errors.  Since many applications ignore close()
 * errors this is not always useful.
 *
 * NOTE: The flush() method may be called more than once for each
 * open().	This happens if more than one file descriptor refers
 * to an opened file due to dup(), dup2() or fork() calls.	It is
 * not possible to determine if a flush is final, so each flush
 * should be treated equally.  Multiple write-flush sequences are
 * relatively rare, so this shouldn't be a problem.
 *
 * Filesystems shouldn't assume that flush will always be called
 * after some writes, or that if will be called at all.
 *
 * Changed in version 2.2
 *
 * @param path          Path to the file
 * @param file_info     Pointer to FUSE file structure
 * @return              0 on success, negative error code otherwise
 */
int fsc_flush (const char *path, struct fuse_file_info *file_info);

/**
 * Release an open file
 *
 * Release is called when there are no more references to an open
 * file: all file descriptors are closed and all memory mappings
 * are unmapped.
 *
 * For every open() call there will be exactly one release() call
 * with the same flags and file descriptor.	 It is possible to
 * have a file opened more than once, in which case only the last
 * release will mean, that no more reads/writes will happen on the
 * file.  The return value of release is ignored.
 *
 * Changed in version 2.2
 *
 * @param path          Path to the file
 * @param file_info     Pointer to FUSE file structure
 * @return              0 on success, negative error code otherwise
 */
int fsc_release (const char *path, struct fuse_file_info *file_info);

/**
 * Synchronize file contents
 *
 * If the datasync parameter is non-zero, then only the user data
 * should be flushed, not the meta data.
 *
 * Changed in version 2.2
 *
 * @param path          Path to the file
 * @param datasync      Flag for meta data syncing
 * @param file_info     Pointer to FUSE file structure
 * @return              0 on success, negative error code otherwise
 */
int fsc_fsync (const char *path, int datasync,
               struct fuse_file_info *file_info);

/**
 * Open directory
 *
 * Unless the 'default_permissions' mount option is given,
 * this method should check if opendir is permitted for this
 * directory. Optionally opendir may also return an arbitrary
 * filehandle in the fuse_file_info structure, which will be
 * passed to readdir, closedir and fsyncdir.
 *
 * Introduced in version 2.3
 *
 * @param path          Path to the file
 * @param file_info     Pointer to FUSE file structure
 * @return              0 on success, negative error code otherwise
 */
int fsc_opendir (const char *path, struct fuse_file_info *file_info);

/**
 * Read directory
 *
 * The filesystem may choose between two modes of operation:
 *
 * 1) The readdir implementation ignores the offset parameter, and
 * passes zero to the filler function's offset.  The filler
 * function will not return '1' (unless an error happens), so the
 * whole directory is read in a single readdir operation.
 *
 * 2) The readdir implementation keeps track of the offsets of the
 * directory entries.  It uses the offset parameter and always
 * passes non-zero offset to the filler function.  When the buffer
 * is full (or an error happens) the filler function will return
 * '1'.
 *
 * Introduced in version 2.3
 *
 * @param path          Path to the file
 * @param buf           Buffer to be fillled with data
 * @param filler        Function to be used to fill the buffer
 * @param offset        Offset to be used (only used when filling the
 *                      buffer entry by entry)
 * @param file_info     Pointer to FUSE file structure
 * @return              0 on success, negative error code otherwise
 */
int fsc_readdir (const char *path, void *buf, fuse_fill_dir_t filler,
                 off_t offset, struct fuse_file_info *file_info);

/**
 * Release directory
 *
 * @param path          Path to the file
 * @param file_info     Pointer to FUSE file structure
 * @return              0 on success, negative error code otherwise
 */
int fsc_releasedir (const char *path, struct fuse_file_info *file_info);

/**
 * Initialize filesystem
 *
 * The return value will passed in the `private_data` field of
 * `struct fuse_context` to all file operations, and as a
 * parameter to the destroy() method. It overrides the initial
 * value provided to fuse_main() / fuse_new().
 *
 * @param conn          
 * @return              Pointer to internal filesystem structure
 */
void* fsc_fuse_init (struct fuse_conn_info *conn);

/**
 * Clean up filesystem
 *
 * Called on filesystem exit.
 *
 * Introduced in version 2.3
 *
 * @param ptr           Internal filesystem data (returned by init
 *                      function)
 */
void fsc_fuse_destroy (void *ptr);

/**
 * Check file access permissions
 *
 * This will be called for the access() system call.  If the
 * 'default_permissions' mount option is given, this method is not
 * called.
 *
 * This method is not called under Linux kernel versions 2.4.x
 *
 * Introduced in version 2.5
 *
 * @param path          Path to the file
 * @param mode          Mode to be tested
 * @return              0 on success, negative error code otherwise
 */
int fsc_access (const char *path, int mode);

/**
 * Create and open a file
 *
 * If the file does not exist, first create it with the specified
 * mode, and then open it.
 *
 * If this method is not implemented or under Linux kernel
 * versions earlier than 2.6.15, the mknod() and open() methods
 * will be called instead.
 *
 * Introduced in version 2.5
 *
 * @param path          Path to the file
 * @param mode          Mode of the new file
 * @param file_info     Pointer to FUSE file structure
 * @return              0 on success, negative error code otherwise
 */
int fsc_create (const char *path, mode_t mode,
                struct fuse_file_info *file_info);

/**
 * Get attributes from an open file
 *
 * This method is called instead of the getattr() method if the
 * file information is available.
 *
 * Currently this is only called after the create() method if that
 * is implemented (see above).  Later it may be called for
 * invocations of fstat() too.
 *
 * Introduced in version 2.5
 *
 * @param path          Path to the file
 * @param st            File information to be filled (see 'man stat')
 * @param file_info     Pointer to FUSE file structure
 * @return              0 on success, negative error code otherwise
 */
int fsc_fgetattr (const char *path, struct stat *st,
                  struct fuse_file_info *file_info);

/**
 * Change the access and modification times of a file with
 * nanosecond resolution
 *
 * This supersedes the old utime() interface.  New applications
 * should use this.
 *
 * See the utimensat(2) man page for details.
 *
 * Introduced in version 2.6
 *
 * @param path          Path to the file
 * @param tv            New atime and mtime for the file
 * @return              0 on success, negative error code otherwise
 */
int fsc_utimens (const char *path, const struct timespec tv[2]);

#endif
