#ifndef FSC_COMMAND_H
#define FSC_COMMAND_H

#include <fsc.h>

struct _fsc_command_t {
    char *exe;                  ///< Executable to be executed
    char **args;                ///< Arguments for the executable
    int sudo;                   ///< Flag indicating whether the command needs super user privileges
    int global;                 ///< Flag indicating whether to use the command globally
    int logic_or;               ///< Flag indicating whether to use || (true) or && (false) when adding conditions
    char *in_dir;               ///< Current directory name
    char *has_file;             ///< Does current directory contain this file
    char *has_dir;              ///< Does current directory contain this directory
    char *os_id;                ///< ID from /etc/os-release
    char *os_id_like;           ///< ID_LIKE from /etc/os-release
};
typedef struct _fsc_command_t fsc_command_t;

fsc_command_t* fsc_command_new (fsc_t *fsc,
                                const char *exe,
                                const char **args);
int fsc_command_check_os (fsc_t *fsc,
                          fsc_command_t *command);
int fsc_command_check_filetree (fsc_t *fsc,
                                fsc_command_t *command,
                                fsc_dir_t *dir);
int fsc_command_run (fsc_t *fsc, fsc_command_t *command, fsc_dir_t *dir);
int fsc_command_run_globals (fsc_t *fsc);
int fsc_command_run_locals (fsc_t *fsc);
void fsc_command_free (fsc_command_t *command);

#endif

