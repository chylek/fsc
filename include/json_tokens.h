#ifndef FSC_JSON_TOKENS_H
#define FSC_JSON_TOKENS_H

#include <fsc.h>

#include <jansson.h>

struct _fsc_json_handle_t {
    const char *key;
    json_type type;
    int(*set_value)(fsc_t *fsc, struct _fsc_json_handle_t *handle,
                    json_t *object);
};
typedef struct _fsc_json_handle_t fsc_json_handle_t;

/**
 * Export settings into JSON file
 *
 * @param fsc           fsc structure
 * @param path          File path
 * @return              0 on success, non-zero otherwise
 */
int fsc_json_export (fsc_t *fsc, const char *path);

int fsc_json_set_mountpoint (fsc_t *fsc,
                             struct _fsc_json_handle_t *handle,
                             json_t *object);
int fsc_json_set_logfile (fsc_t *fsc,
                          struct _fsc_json_handle_t *handle,
                          json_t *object);
int fsc_json_set_scan_root (fsc_t *fsc,
                            struct _fsc_json_handle_t *handle,
                            json_t *object);
int fsc_json_set_open_limit (fsc_t *fsc,
                             struct _fsc_json_handle_t *handle,
                             json_t *object);
int fsc_json_set_include (fsc_t *fsc,
                          struct _fsc_json_handle_t *handle,
                          json_t *object);
int fsc_json_set_debug (fsc_t *fsc,
                        struct _fsc_json_handle_t *handle,
                        json_t *object);
int fsc_json_set_verbose (fsc_t *fsc,
                          struct _fsc_json_handle_t *handle,
                          json_t *object);
int fsc_json_set_fuse_st (fsc_t *fsc,
                          struct _fsc_json_handle_t *handle,
                          json_t *object);
int fsc_json_set_fuse_debug (fsc_t *fsc,
                             struct _fsc_json_handle_t *handle,
                             json_t *object);
int fsc_json_set_commands_array (fsc_t *fsc,
                                 struct _fsc_json_handle_t *handle,
                                 json_t *object);

extern fsc_json_handle_t json_tokens[];
extern size_t json_tokens_size;

#endif

