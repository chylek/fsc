#ifndef FSC_FILETREE_H
#define FSC_FILETREE_H

struct _fsc_file_t;
typedef struct _fsc_file_t fsc_file_t;
struct _fsc_dir_t;
typedef struct _fsc_dir_t fsc_dir_t;
struct _fsc_regexp_t;
typedef struct _fsc_regexp_t fsc_regexp_t;

#include "fsc.h"
#include "filters.h"

#include <stdint.h>
#include <sys/stat.h>
#include <pthread.h>

struct _fsc_file_t {
    fsc_t *fsc;                     ///< fsc structure

    int fd;                         ///< File descriptor
    uint64_t open_counter;          ///< Counter to manage file descriptor
    char *orig_path;                ///< Original file path
    char *name;                     ///< File name
    char *link_path;                ///< Path of the link's destination
    struct stat st;                 ///< File information (see 'man stat')
    pthread_mutex_t lock;           ///< Mutex for concurrent operations
    fsc_dir_t *parent;              ///< Parent directory

    size_t fdata_num;               ///< Number of filters data entries
    size_t fdata_max;               ///< Currently allocated space for filters data entries
    void **filters_data;            ///< Filters data array
};

struct _fsc_dir_t {
    fsc_t *fsc;                     ///< fsc structure

    char *orig_path;                ///< Original directory path
    char *name;                     ///< Directory name
    char *link_path;                ///< Path of the link's destination
    struct stat st;                 ///< Directory information (see 'man stat')
    fsc_dir_t *parent;              ///< Parent directory (NULL for root)

    size_t nfiles;                  ///< Number of files in directory
    size_t maxfiles;                ///< Available space for files
    fsc_file_t **files;             ///< Files array

    size_t ndirs;                   ///< Number of directories in directory
    size_t maxdirs;                 ///< Currently allocated space for directories
    fsc_dir_t **dirs;               ///< Directories array
};

struct _fsc_regexp_t {
    int exclude;                    ///< Flag indicating whether pattern should be excluded (1) or not (0)
    char *pattern;                  ///< Pattern to compare with
};

/**
 * File constructor
 *
 * @param fsc           fsc structure
 * @param path          File path
 * @param st            File information (see 'man stat')
 * @return              Initialized file structure
 */
fsc_file_t* fsc_file_new (fsc_t *fsc, const char *path, const struct stat *st);

/**
 * File destructor
 *
 * @param fsc           fsc structure
 * @param file          File structure to be freed
 */
void fsc_file_free (fsc_file_t *file);

/**
 * Directory constructor
 *
 * @param fsc           fsc structure
 * @param path          Directory path
 * @param st            Directory information (see 'man stat')
 * @return              Initialized directory structure
 */
fsc_dir_t* fsc_dir_new (fsc_t *fsc, const char *path, const struct stat *st);

/**
 * Directory destructor
 *
 * @param fsc           fsc structure
 * @param dir           Directory structure to be freed
 */
void fsc_dir_free (fsc_dir_t *dir);

/**
 * Add file into given directory
 *
 * @param dir           Directory to put the file into
 * @param file          File to put into the given directory
 * @return              0 on success, non-zero otherwise
 */
int fsc_file_add (fsc_dir_t *dir, fsc_file_t *file);

/**
 * Add directory into given directory
 *
 * @param base_dir      Directory to put the directory into
 * @param dir           Directory to put into the given directory
 * @return              0 on success, non-zero otherwise
 */
int fsc_dir_add (fsc_dir_t *base_dir, fsc_dir_t *dir);

/**
 * Get filter-associated data from the file
 *
 * @param file          File to get data from
 * @param filter        Filter related with the data to get
 * @param data          Pointer to put filter related data to
 * @return              0 on success, non-zero otherwise
 */
int fsc_file_get_filter_data (fsc_file_t *file, fsc_filter_t *filter,
                              void **data);

/**
 * Set filter-associated data in the file
 *
 * @param file          File to set data in
 * @param filter        Filter related with the data to set
 * @param data          Data to be set
 * @return              0 on success, non-zero otherwise
 */
int fsc_file_set_filter_data (fsc_file_t *file, fsc_filter_t *filter,
                              void *data);

/**
 * Create several directories one in another (like mkdir -p)
 *
 * @param base_dir      Directory the path should be created from
 * @param path          Path with directories to be created
 * @return              The last directory from the path
 */
fsc_dir_t* fsc_mkpath (fsc_dir_t *base_dir, const char *path);

/**
 * Remove file from given directory
 *
 * @param dir           Directory to remove the file from
 * @param index         Index of the file in directory's files array
 */
void fsc_file_del (fsc_dir_t *dir, size_t index);

/**
 * Remove directory from given directory
 *
 * @param dir           Directory to remove the directory from
 * @param index         Index of the directory in directory's directories array
 */
void fsc_dir_del (fsc_dir_t *dir, size_t index);

/**
 * Get absolute path of given file
 *
 * @param file          File to get path of
 * @param buff          Buffer to be filled with the path
 * @param buffsize      Available space in the buffer
 * @return              File path on success, NULL otherwise
 */
char* fsc_file_get_path (fsc_file_t *file, char *buff, size_t buffsize);

/**
 * Get absolute path of given directory
 *
 * @param dir           Directory to get path of
 * @param buff          Buffer to be filled with the path
 * @param buffsize      Available space in the buffer
 * @return              Directory path on success, NULL otherwise
 */
char* fsc_dir_get_path (fsc_dir_t *dir, char *buff, size_t buffsize);

/**
 * Create new link to the file
 *
 * @param file          File to create link of
 * @return              Newly created link
 */
fsc_file_t* fsc_file_link (fsc_file_t *file);

/**
 * Create new link to the directory
 *
 * @param dir           Directory to create link of
 * @return              Newly created link
 */
fsc_dir_t* fsc_dir_link (fsc_dir_t *dir);

/**
 * Follow links until regular file is reached
 *
 * @param file          File to start from
 */
fsc_file_t* fsc_file_follow_links (fsc_file_t *file);

/**
 * Follow links until regular directory is reached
 *
 * @param dir           Directory to start from
 */
fsc_dir_t* fsc_dir_follow_links (fsc_dir_t *dir);

/**
 * Rename the file
 *
 * @param file          File to be renamed
 * @param newname       New name of the file
 * @return
 */
/* Changes only filename, does not move files between directories */
int fsc_file_rename (fsc_file_t *file, const char *newname);

/**
 * Rename the directory
 *
 * @param dir           Directory to be renamed
 * @param newname       New name of the directory
 * @return              0 on success, non-zero otherwise
 */
/* Changes only dirname, does not move directories between directories */
int fsc_dir_rename (fsc_dir_t *dir, const char *newname);

/**
 * Move the file
 *
 * @param file          File to be moved
 * @param dest_dir      Directory for the file to be placed into
 * @return              0 on success, non-zero otherwise
 */
int fsc_file_mv (fsc_file_t *file, fsc_dir_t *dest_dir);

/**
 * Move the directory
 *
 * @param dir           Directory to be moved
 * @param dest_dir      Directory for the directory to be placed into
 * @return              0 on success, non-zero otherwise
 */
int fsc_dir_mv (fsc_dir_t *dir, fsc_dir_t *dest_dir);

/**
 * File comparision function for fsc_find_entry()
 *
 * @param index         Index of the file being tested in directory's files array
 * @param file_name     Name of the file to be found
 * @return              Negative if index should be lower, 0 if the file has been found or positive if index should be higher
 */
int fsc_file_cmp (const void *files, size_t index, const char *file_name);

/**
 * Directory comparision function for fsc_find_entry()
 *
 * @param index         Index of the directory being tested in directory's directories array
 * @param dir_name      Name of the directory to be found
 * @return              Negative if index should be lower, 0 if the directory has been found or positive if index should be higher
 */
int fsc_dir_cmp (const void *dirs, size_t index, const char *dir_name);

/**
 * File comparision function for sort()
 *
 * @param ptr1          File to be compared
 * @param ptr2          File to be compared
 * @return              Negative if index should be lower, 0 if the directory has been found or positive if index should be higher
 */
int fsc_file_cmp_sort (const void *ptr1, const void *ptr2);

/**
 * Directory comparision function for sort()
 *
 * @param ptr1          Directory to be compared
 * @param ptr2          Directory to be compared
 * @return              Negative if ptr1 is before, 0 if directories are equal or positive if ptr2 is later in alphabetical order
 */
int fsc_dir_cmp_sort (const void *ptr1, const void *ptr2);

/**
 * Exclude the path from the scan
 *
 * @param fsc           fsc structure
 * @param path          Path to be excluded from the scan
 * @return              0 on success, non-zero otherwise
 */
int fsc_exclude_path (fsc_t *fsc, const char *path);

/**
 * Include the path in the scan
 *
 * @param fsc           fsc structure
 * @param path          Path to be included in the scan
 * @return              0 on success, non-zero otherwise
 */
int fsc_include_path (fsc_t *fsc, const char *path);

/**
 * Check whether the path is excluded from the scan
 *
 * @param fsc           fsc structure
 * @param path          Path to check the exclusion of
 * @return              0        - path is not excluded
 *                      positive - path is excluded
 *                      nagtive  - error
 */
int fsc_is_excluded (fsc_t *fsc, const char *path);

/**
 * Load local filesystem into internal filetree
 *
 * @param fsc           fsc structure
 * @param path          Real filesystem path to start scanning from
 * @return              0 on success, non-zero otherwise
 */
int fsc_scan (fsc_t *fsc, const char *path);

/**
 * Follow given path in the internal filetree
 *
 * @param fsc           fsc structure
 * @param path          Path to be found
 * @param dir           Directory found (untouched on failure)
 * @return              0 on success, non-zero otherwise
 */
int fsc_ftw (fsc_t *fsc, const char *path, fsc_dir_t **dir);

#endif

