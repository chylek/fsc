#ifndef FSC_FILTERS_H
#define FSC_FILTERS_H

struct _fsc_filter_t;
typedef struct _fsc_filter_t fsc_filter_t;

#include "fsc.h"
#include "filetree.h"

#define FSC_FILTER_DESC_LEN         256
#define FSC_FILTER_DIR_NAME_LEN     64

enum FSC_FILTER_TYPE {
    FSC_FILTER_TYPE_ZERO_FILE,       ///< Remove zero sized files
    FSC_FILTER_TYPE_SPARSE_FILE,     ///< Remove sparse files
    FSC_FILTER_TYPE_MEDIA_TYPE,      ///< Check file type
    FSC_FILTER_TYPE_SAME_FILES,      ///< Find files duplicates
    FSC_FILTER_TYPE_USER_DEFINED     ///< Created by user
};
typedef enum FSC_FILTER_TYPE FSC_FILTER_TYPE_T;

typedef int(*fsc_filter_cb_init)(fsc_t *fsc, fsc_filter_t *filter);
typedef int(*fsc_filter_cb_run)(fsc_t *fsc, fsc_filter_t *filter,
                                fsc_file_t *file, fsc_dir_t *dir);
typedef int(*fsc_filter_cb_finish)(fsc_t *fsc, fsc_filter_t *filter);
typedef void(*fsc_filter_cb_file_data_free)(fsc_t *fsc, fsc_filter_t *filter,
                                            void *data);
typedef void(*fsc_filter_cb_clean)(fsc_t *fsc, fsc_filter_t *filter);

struct _fsc_filter_t {
    fsc_t *fsc;                                     ///< fsc structure
    size_t index;                                   ///< Filter's index for file's filters data array
    FSC_FILTER_TYPE_T type;                         ///< Filter type
    char type_desc[FSC_FILTER_DESC_LEN];            ///< Additional filter type description
    char dir_name[FSC_FILTER_DIR_NAME_LEN];         ///< Name of dedicated directory
    void *data;                                     ///< Filter dedicated data
    fsc_dir_t *dir;                                 ///< Dedicated directory
    fsc_filter_cb_init init;                        ///< Single-run filter initialization callback
    fsc_filter_cb_run run;                          ///< Multi-run filter per-file / per-directory callback
    fsc_filter_cb_finish finish;                    ///< Single-run filter post-scan callback
    fsc_filter_cb_file_data_free file_data_free;    ///< Multi-run filter file cleanup callback
    fsc_filter_cb_clean clean;                      ///< Single-run filter cleanup callback
};

extern fsc_filter_t FSC_FILTER_ZERO_FILE;
extern fsc_filter_t FSC_FILTER_SPARSE_FILE;
extern fsc_filter_t FSC_FILTER_MEDIA_TYPE;
extern fsc_filter_t FSC_FILTER_SAME_FILES;

/**
 * Create new filter
 *
 * @param fsc       fsc_structure
 * @param desc      Unique filter description
 * @param dir_name  Unique name for dedicated directory
 * @param data      Data for filter usage
 * @param init      Filter initialization callback
 * @param run       Callback to be run on each file and/or directory during scan
 * @param finish    Callback to be run after filesystem scan
 * @param fd_free   Callback to be run in file desctructor
 * @param clean     Callback to be run in filter destructor
 * @return          New filter instance
 */
fsc_filter_t* fsc_filter_new (fsc_t *fsc,
                              const char *desc,
                              const char *dir_name,
                              void *data,
                              fsc_filter_cb_init init,
                              fsc_filter_cb_run run,
                              fsc_filter_cb_finish finish,
                              fsc_filter_cb_file_data_free fd_free,
                              fsc_filter_cb_clean clean);

/**
 * Filter destructor
 *
 * @param filter    Filter to be freed
 */
void fsc_filter_free (fsc_filter_t *filter);

/**
 * Filter copy constructor
 *
 * @param filter    Filter to be copied
 * @return          New filter instance
 */
fsc_filter_t* fsc_filter_dup (fsc_filter_t *filter);

/**
 * Filters comparator
 *
 * @param filter1   First filter to compare
 * @param filter2   Second filter to compare
 * @return          1 if filters are the same, 0 otherwise
 */
int fsc_filter_same (fsc_filter_t *filter1, fsc_filter_t *filter2);

/**
 * Create filter's dedicated directory
 *
 * @param filter    Filter to create dedicated directory for
 * @return          0 on success, non-zero otherwise
 */
int fsc_filter_add_dir (fsc_filter_t *filter);

/**
 * General function for simplest filters to conditionally link files
 *
 * @param filter    Filter to work for
 * @param file      File to check and link
 * @param cond      Callback to check whether to link the file
 *                  Return values:
 *                      positive - file accepted
 *                      0        - file denied
 *                      negative - error (should be set by user)
 * @return          0 on success, non-zero otherwise
 */
int fsc_filter_cond_file_link (fsc_filter_t *filter, fsc_file_t *file,
                               int(*cond)(fsc_t *fsc, fsc_filter_t *filter,
                                          fsc_file_t*));

/**
 * Get filter's description
 *
 * @param filter    Filter to get name of
 * @return          Filter's description
 */
const char* fsc_filter_get_desc (fsc_filter_t *filter);

/**
 * Add new filter
 *
 * @param fsc       fsc_structure
 * @param filter    Filter to be added
 * @return          0 on success, non-zero otherwise
 */
int fsc_filter_add (fsc_t *fsc, fsc_filter_t *filter);

/**
 * Add all default filters
 *
 * @param fsc       fsc_structure
 * @return          0 on success, non-zero otherwise
 */
int fsc_filter_add_all (fsc_t *fsc);

/**
 * Initialize default filters
 *
 * @param fsc       fsc_structure
 * @return          0 on succes, non-zero otherwise
 */
int fsc_filters_init (fsc_t *fsc);

/**
 * Clean up default filters
 */
void fsc_filters_clean (void);

/**
 * Add default fsc filter
 *
 * @param fsc           fsc structure
 * @param type          Type of the filter to be added
 * @return              0 on succes, non-zero otherwise
 */
int fsc_filter_add_default (fsc_t *fsc, FSC_FILTER_TYPE_T type);

#endif

