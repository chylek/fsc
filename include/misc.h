#ifndef FSC_MISC_H
#define FSC_MISC_H

#include "fsc.h"

#include <errno.h> /* For error messages */
#include <limits.h>
#include <sys/types.h>
#include <pthread.h> /* This header contains macros using pthread library */

//#define FSC_VALGRIND_FIX

#define FSC_LOG_LEVEL_NORMAL        0x1
#define FSC_LOG_LEVEL_DEBUG         0x2
#define FSC_LOG_LEVEL_VERBOSE       0x3
#define FSC_LOG_LEVEL_ERROR         0x4

#define FSC_LOG(...)                fsc_log(fsc, __func__, FSC_LOG_LEVEL_NORMAL, __VA_ARGS__)
#define FSC_DEBUG(...)              fsc_log(fsc, __func__, FSC_LOG_LEVEL_DEBUG, __VA_ARGS__)
#define FSC_VERBOSE(...)            fsc_log(fsc, __func__, FSC_LOG_LEVEL_VERBOSE, __VA_ARGS__)
#define FSC_ERROR(...)              fsc_log(fsc, __func__, FSC_LOG_LEVEL_ERROR, __VA_ARGS__)
#define FSC_ERR_SET(ERRNUM, ...)    fsc_set_err(fsc, ERRNUM, __VA_ARGS__);  \
                                    FSC_ERROR(__VA_ARGS__)
#define FSC_INTERNAL_ERR()          FSC_ERROR("INTERNAL ERROR - Please report it to the software maintainer")

#define FSC_ALLOC_ENTRIES           100
#define FSC_READ_SIZE               4096

#define lenof(str)                  (sizeof(str) - 1)
#define sizeof_array(array)         (sizeof(array) / sizeof(array[0]))
#define FSC_STRING_IS_EMPTY(str)    (*(str) == '\0')
#define FSC_OBJECT_LOCK(obj)        pthread_mutex_lock(&(obj)->lock)
#define FSC_OBJECT_UNLOCK(obj)      pthread_mutex_unlock(&(obj)->lock)

#define MIN(a, b)                   ((a) < (b) ? (a) : (b))
#define MAX(a, b)                   ((a) > (b) ? (a) : (b))

#define FSC_MAIN_FILETREE_NAME      "filetree"

#define FSC_FILE_MODE               (S_IFREG | S_IRUSR | S_IWUSR |          \
                                     S_IRGRP | S_IWGRP | S_IROTH |          \
                                     S_IWOTH)
                                    /* -rw-rw-rw- */
#define FSC_DIR_MODE                (S_IFDIR | S_IRWXU | S_IRGRP |          \
                                     S_IXGRP | S_IROTH | S_IXOTH)
                                    /* drwxr-xr-x */

#define FSC_TEMP_DIR                "/var/tmp"

#define FSC_ABORT_FILE_PATH         "/abort"
#define FSC_ABORT_FILE_FD           (fsc->fh_limit + 1)

extern struct stat FSC_FILE_ST;
extern struct stat FSC_DIR_ST;

/**
 * Write into logfile
 *
 * @param fsc           fsc structure
 * @param fun_name      Name of the function the log is called from
 * @param log_level     Level of the message
 * @param format_string Message format
 */
void fsc_log (fsc_t *fsc, const char *fun_name, int log_level,
              const char *format_string, ...) __attribute__((format(printf, 4, 5)));

/**
 * Safely resize array
 *
 * @param ptr           Array to be resized
 * @param size          Current number of entries
 * @param elsize        Size of single entry
 * @return              0 on success, non-zero otherwise
 */
int fsc_resize (void *ptr, size_t *size, size_t elsize);

/**
 * Find entry in given array
 *
 * @param array         Array to search in
 * @param size          Number of entries in array
 * @param name          Name of entry to be found
 * @param compare       Comparison function
 * @return              Index of found entry on success, -1 otherwise
 */
ssize_t fsc_find_entry (const void *array, size_t size, const char *name,
                        int (*compare)(const void*, size_t, const char*));

/**
 * Create absolute path based on current directory and link path
 *
 * @param fsc           fsc structure
 * @param orig_path     Absolute path to the file being a link
 * @param link_path     Absolute or relative path to follow (will be updated on success)
 * @return              0 on success, non-zero otherwise
 */
int fsc_merge_paths (fsc_t *fsc, const char *orig_path,
                     char link_path[PATH_MAX]);

/**
 * Copy string
 *
 * @param dest          Destination string
 * @param src           Source string
 * @param length        Maximum number of characters to be copied (including terminating null)
 * @return              Number of copied characters
 */
size_t fsc_strncpy (char *dest, const char *src, size_t length);

/**
 * Copy file
 *
 * @param fsc           fsc structure
 * @param src           Source file path
 * @param dest          Destination file path
 * @return              0 on success, non-zero otherwise
 */
int fsc_copy_file (fsc_t *fsc, const char *source, const char *dest);

/**
 * Move file
 *
 * @param fsc           fsc structure
 * @param src           Source file path
 * @param dest          Destination file path
 * @return              0 on success, non-zero otherwise
 */
int fsc_move_file (fsc_t *fsc, const char *source, const char *dest);

#endif

