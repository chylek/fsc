#ifndef FSC_H
#define FSC_H

#define _DEFAULT_SOURCE
#define _XOPEN_SOURCE       500
#define _GNU_SOURCE

#ifdef __APPLE__
#define _DARWIN_C_SOURCE
#endif

struct _fsc_t;
typedef struct _fsc_t fsc_t;

#include "cmdline.h"

#include "misc.h"
#include "filetree.h"
#include "filters.h"
#include "operations.h"
#include <command.h>
#include <json_tokens.h>

#include <stdint.h>
#include <sys/stat.h>
#include <magic.h>
#include <pthread.h>

#define FUSE_USE_VERSION 26 /* This has to be defined before fuse.h */
#include <fuse.h>

#define FSC_DATA ((fsc_t*) fuse_get_context()->private_data)

struct _fsc_t {
    int abort;                              ///< Flag indicating whether to run operations
    int error;                              ///< Last error number
    char error_msg[65536];                  ///< Last error message
    char *logfile_path;                     ///< Logfile path
    FILE *logfile;                          ///< Logfile structure

    char *scan_root_path;                   ///< Path to start scan from
    char *mountpoint;                       ///< Mount point path
    fsc_dir_t *root;                        ///< Root directory of internal filetree

    char *os_id;                            ///< ID from /etc/os-release
    size_t os_id_like_num;                  ///< Number of ID_LIKE entries in /etc/os-release
    size_t os_id_like_max;                  ///< Currently allocated space for ID_LIKE
    char **os_id_like;                      ///< ID_LIKE from /etc/os-release

    magic_t magic_cookie;                   ///< Magic structure to check files types
    struct gengetopt_args_info args;        ///< Structure for command line arguments handling
    struct fuse_args fargs;                 ///< FUSE arguments structure
    uint64_t mutex_init;                    ///< Bitmap to keep track of mutexes initialization
    pthread_mutex_t fh_mutex;               ///< Mutex for fh_* fields

    size_t fh_limit;                        ///< Open files limit
    fsc_file_t **fh_files;                  ///< Array of open files
    fsc_dir_t **fh_dirs;                    ///< Array of open directories

    size_t exl_num;                         ///< Number of registered path patterns
    size_t exl_max;                         ///< Currently allocated space for path patterns
    fsc_regexp_t **exclusion_list;          ///< Array of path patterns to be excluded from or included in the scan

    size_t filters_num;                     ///< Number of registered filters
    size_t filters_max;                     ///< Currently allocated space for filters
    fsc_filter_t **filters;                 ///< Filters array

    size_t op_num;                          ///< Number of registered file operations
    size_t op_max;                          ///< Currently allocated space for file operations
    fsc_operation_t **operations;           ///< Operations array

    size_t cmd_num;                         ///< Number of registered commands
    size_t cmd_max;                         ///< Currently allocated space for commands
    fsc_command_t **commands;               ///< Commands array
};

/**
 * fsc internal structure constructor
 *
 * @param argc          Number of command line arguments
 * @param argv          Command line arguments
 * @return              Initialized fsc structure on success, NULL otherwise
 */
fsc_t* fsc_init (int argc, char **argv);

/**
 * fsc internal structure destructor
 *
 * @param fsc           fsc structure to be freed
 */
void fsc_free (fsc_t *fsc);

/**
 * Load config from text file
 *
 * @param fsc           fsc structure
 * @param path          Path to configuration file
 * @return              0 on success, non-zero otherwise
 */
int fsc_cfg_load_file (fsc_t *fsc, const char *path);

/**
 * Set error number and message
 *
 * @param fsc           fsc structure
 * @param error         Error number
 * @param format        Error message format
 */
void fsc_set_err (fsc_t *fsc, int error, const char *format, ...) __attribute__((format(printf, 3, 4)));

/**
 * Clear error number and message
 *
 * @param fsc           fsc structure
 */
void fsc_clear_err (fsc_t *fsc);

/**
 * Get last error number
 *
 * @param fsc           fsc structure
 * @return              Last error number
 */
int fsc_get_err (fsc_t *fsc);

/**
 * Get last error message
 *
 * @param fsc           fsc structure
 * @return              Last error message
 */
const char* fsc_get_err_msg (fsc_t *fsc);

/**
 * Get root directory structure
 *
 * @param fsc           fsc structure
 * @return              Root directory structure
 */
fsc_dir_t* fsc_get_root (fsc_t *fsc);

#endif

