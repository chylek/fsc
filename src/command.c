#include <fsc.h>
#include <command.h>

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

fsc_command_t* fsc_command_new (fsc_t *fsc,
                                const char *exe,
                                const char **args) {
    size_t i;
    fsc_command_t *command, *ret = NULL;

    if(!fsc) {
        return ret;
    }
    if(!exe) {
        FSC_ERR_SET(EINVAL, "No command set");
        return ret;
    }

    command = (fsc_command_t*)calloc(1, sizeof(fsc_command_t));
    if(!command) {
        FSC_ERR_SET(ENOMEM, "Out of memory");
        return ret;
    }
    command->exe = strdup(exe);
    if(!command->exe) {
        FSC_ERR_SET(ENOMEM, "Out of memory");
        goto fsc_command_new_err;
    }

    i = 0;
    if(args) {
        while(args[i]) {
            i++;
        }
    }
    command->args = (char**)calloc(i + 1, sizeof(char*));
    if(!command->args) {
        FSC_ERR_SET(ENOMEM, "Out of memory");
        goto fsc_command_new_err;
    }

    for(i=0; args[i]; i++) {
        command->args[i] = strdup(args[i]);
        if(!command->args[i]) {
            FSC_ERR_SET(ENOMEM, "Out of memory");
            goto fsc_command_new_err;
        }
    }
    command->args[i] = NULL;

    ret = command;
    command = NULL;
fsc_command_new_err:
    fsc_command_free(command);
    return ret;
} /* fsc_command_new */

int fsc_command_check_os (fsc_t *fsc,
                          fsc_command_t *command) {
    int found = 0; /* handle && logic (command->logic_or == 0) */
    size_t i;

    if(!fsc) {
        return -1;
    }
    if(!command) {
        FSC_ERR_SET(EINVAL, "NULL command");
        return -1;
    }

    if(!command->os_id && !command->os_id_like) {
        /* This check is not required, work as if it was verified */
        FSC_VERBOSE("No OS requirement for '%s' command", command->exe);
        return 0;
    }

    if(command->os_id) {
        if(!strcmp(fsc->os_id, command->os_id)) {
            FSC_VERBOSE("OS ID matched: %s", command->os_id);
            if(command->logic_or) {
                return 0;
            }
            found = 1;
        } else if(!command->logic_or) {
            return 1;
        }
    }

    if(command->os_id_like) {
        for(i=0; i<fsc->os_id_like_num; i++) {
            if(!strcmp(fsc->os_id_like[i], command->os_id_like)) {
                FSC_VERBOSE("OS ID_LIKE matched: %s", command->os_id_like);
                if(command->logic_or) {
                    return 0;
                }
                found = 1;
            } else if(!command->logic_or) {
                return 1;
            }
        }
    }

    return found ? 0 : 1;
} /* fsc_command_check_os */

int fsc_command_check_filetree (fsc_t *fsc,
                                fsc_command_t *command,
                                fsc_dir_t *dir) {
    int found = 0; /* handle && logic (command->logic_or == 0) */
    ssize_t index;

    if(!fsc) {
        return -1;
    }
    if(!command) {
        FSC_ERR_SET(EINVAL, "NULL command");
        return -1;
    }
    if(!dir) {
        FSC_VERBOSE("NULL directory - skipping this check");
        return 0;
    }

    if(!command->in_dir && !command->has_file && !command->has_dir) {
        /* This check is not required, work as if it was verified */
        FSC_VERBOSE("No filetree requirement for '%s' command", command->exe);
        return 0;
    }

    if(command->in_dir) {
        if(!strcmp(dir->name, command->in_dir)) {
            FSC_VERBOSE("Parent directory matched: %s", command->in_dir);
            if(command->logic_or) {
                return 0;
            }
            found = 1;
        } else if (!command->logic_or) {
            return 1;
        }
    }

    if(command->has_file) {
        index = fsc_find_entry(dir->files, dir->nfiles, command->has_file,
                               fsc_file_cmp);
        if(index >= 0) {
            FSC_VERBOSE("'%s' file found in '%s'", command->has_file,
                        dir->name);
            if(command->logic_or) {
                return 0;
            }
            found = 1;
        } else if(!command->logic_or) {
            return 1;
        }
    }

    if(command->has_dir) {
        index = fsc_find_entry(dir->dirs, dir->ndirs, command->has_dir,
                               fsc_dir_cmp);
        if(index >= 0) {
            FSC_VERBOSE("'%s' directory found in '%s'", command->has_dir,
                        dir->name);
            if(command->logic_or) {
                return 0;
            }
            found = 1;
        } else if(!command->logic_or) {
            return 1;
        }
    }

    return found ? 0 : 1;
} /* fsc_command_check_filetree */

static int fsc_command_verify (fsc_t *fsc,
                               fsc_command_t *command,
                               fsc_dir_t *dir) {
    if(command->sudo && geteuid() != 0) {
        FSC_ERROR("Privileged user is required for '%s' command", command->exe);
        /* Do not fail here - running fsc as root may be dangerous */
        return 1;
    }

    switch(fsc_command_check_os(fsc, command)) {
        case -1:
            FSC_ERROR("Cannot check OS");
            return -1;
        case 0:
            FSC_VERBOSE("OS correctly verified");
            break;
        case 1:
            FSC_VERBOSE("Wrong OS - skipping this command");
            return 1;
        default:
            FSC_ERROR("[Internal] Unhandled return code of OS check");
            FSC_INTERNAL_ERR();
            return -1;
    }

    switch(fsc_command_check_filetree(fsc, command, dir)) {
        case -1:
            FSC_ERROR("Cannot check filetree");
            return -1;
        case 0:
            FSC_VERBOSE("Filetree correctly verified");
            break;
        case 1:
            FSC_VERBOSE("Wrong filetree - skipping this command");
            return 1;
        default:
            FSC_ERROR("[Internal] Unhandled return code of filetree check");
            FSC_INTERNAL_ERR();
            return -1;
    }

    return 0;
} /* fsc_command_verify */

static int fsc_redirect_to_fd (fsc_t *fsc, const char *exe_name,
                               const char *stream_name, int fd) {
    int ret = -1, file_fd = -1;
    char *file_path = NULL;

    file_path = (char*)malloc(lenof(FSC_TEMP_DIR) + 1 + lenof("fsc-") +
                              strlen(exe_name) + 1 + strlen(stream_name) +
                              lenof("-XXXXXX") + 1);
    if(!file_path) {
        FSC_ERROR("Out of memory");
        return ret;
    }
    sprintf(file_path, "%s/fsc-%s-%s-XXXXXX", FSC_TEMP_DIR, exe_name,
            stream_name);
    file_fd = mkstemp(file_path);
    if(file_fd < 0) {
        FSC_ERROR("Cannot create '%s' file: %s", file_path, strerror(errno));
        goto fsc_redirect_to_fd_err;
    }
    if(dup2(file_fd, fd) < 0) {
        FSC_ERROR("Cannot redirect stream to the '%s' file: %s", file_path,
                  strerror(errno));
        goto fsc_redirect_to_fd_err;
    }
    file_fd = -1;

    ret = 0;
fsc_redirect_to_fd_err:
    if(file_fd >= 0) {
        if(close(file_fd)) {
            FSC_ERROR("Cannot close '%s' file: %s", file_path,
                      strerror(errno));
        }
        if(unlink(file_path)) {
            FSC_ERROR("Cannot unlink '%s' file: %s", file_path,
                      strerror(errno));
        }
    }
    free(file_path);
    return ret;
} /* fsc_redirect_to_fd */

int fsc_command_run (fsc_t *fsc, fsc_command_t *command, fsc_dir_t *dir) {
    int status;
    pid_t pid;
    char cwd_path[1024];

    if(!fsc) {
        return -1;
    }
    if(!command) {
        FSC_ERR_SET(EINVAL, "NULL command");
        return -1;
    }

    if(fsc_command_verify(fsc, command, dir)) {
        FSC_DEBUG("Requirements of '%s' command has not been reached",
                  command->exe);
        /* It does not matter whether error was returned or requirements has
         * not been reached */
        return 0;
    }

    pid = fork();
    switch(pid) {
        case -1:
            FSC_ERR_SET(errno, "Cannot create new process: %s",
                        strerror(errno));
            return -1;
        case 0:
            /* New process */
            /* Redirect stdout to the file */
            if(fsc_redirect_to_fd(fsc, command->exe, "stdout", 1)) {
                FSC_ERROR("Cannot redirect 'stdout' to the file");
                _exit(-1);
            }
            /* Redirect stderr to the file */
            if(fsc_redirect_to_fd(fsc, command->exe, "stderr", 2)) {
                FSC_ERROR("Cannot redirect 'stderr' to the file");
                _exit(-1);
            }

            execvp(command->exe, command->args);
            FSC_ERROR("Cannot run '%s' command: %s", command->exe,
                      strerror(errno));
            _exit(-1);
        default:
            /* Parent process */
            if(!getcwd(cwd_path, sizeof(cwd_path))) {
                snprintf(cwd_path, sizeof(cwd_path), "<cwd>");
                FSC_ERROR("Cannot get current working directory: %s",
                          strerror(errno));
            }
            FSC_VERBOSE("Command '%s' executed in '%s', PID: %d",
                        command->exe, cwd_path, pid);
            if(waitpid(pid, &status, 0) < 0) {
                FSC_ERR_SET(errno, "Cannot wait for children process: %s",
                            strerror(errno));
                return -1;
            }
            break;
    }

    return 0;
} /* fsc_command_run */

int fsc_command_run_globals (fsc_t *fsc) {
    size_t i;

    if(!fsc) {
        return -1;
    }

    for(i=0; i<fsc->cmd_num; i++) {
        if(fsc->commands[i]->global) {
            if(fsc_command_run(fsc, fsc->commands[i], NULL)) {
                FSC_ERROR("Global command failed");
                return -1;
            }
        }
    }

    return 0;
} /* fsc_command_run_globals */

static int fsc_command_run_locals_ftw (fsc_t *fsc, fsc_dir_t *dir) {
    size_t i;
    const char *dir_name;

    /* Change current working directory */
    if(dir == fsc->root) {
        dir_name = fsc->mountpoint;
    } else {
        dir_name = dir->name;
    }
    if(chdir(dir_name)) { /* TODO: valgrind hangs here */
        FSC_ERR_SET(errno, "Cannot change working directory to '%s': %s",
                    dir_name, strerror(errno));
        return -1;
    }

    for(i=0; i<fsc->cmd_num; i++) {
        if(!fsc->commands[i]->global) {
            if(fsc_command_run(fsc, fsc->commands[i], dir)) {
                FSC_ERROR("Local command failed");
                return -1;
            }
        }
    }

    /* Move to the next directories */
    for(i=0; i<dir->ndirs; i++) {
        if(fsc_command_run_locals_ftw(fsc, dir->dirs[i])) {
            FSC_ERROR("Cannot run local commands in '%s'", dir->dirs[i]->name);
            return -1;
        }
    }

    /* Get back to be able to go to other directories on the same level */
    if(chdir("..")) {
        FSC_ERR_SET(errno, "Cannot change working directory from '%s' to "
                    "parent: %s", dir_name, strerror(errno));
        return -1;
    }

    return 0;
} /* fsc_command_run_locals_ftw */

static void* fsc_command_run_locals_thread (void *ptr) {
    int *ret = (int*)calloc(1, sizeof(int));
    fsc_t *fsc;

    if(!ret) {
        return NULL;
    }
    *ret = -1;

    if(!ptr) {
        fprintf(stderr, "NULL pointer passed to thread\n");
        return ret;
    }

    fsc = (fsc_t*)ptr;
    if(fsc_command_run_locals_ftw(fsc, fsc->root)) {
        FSC_ERROR("Cannot run local system commands");
        return ret;
    }

    *ret = 0;
    return ret;
} /* fsc_command_run_locals_thread */

int fsc_command_run_locals (fsc_t *fsc) {
    int err;
    pthread_t thread;
    pthread_attr_t attr;

    if(!fsc) {
        return -1;
    }

    memset(&thread, 0, sizeof(pthread_t));
    if((err = pthread_attr_init(&attr))) {
        FSC_ERROR("Cannot initialize thread attributes: %s", strerror(err));
        return -1;
    }
    if((err = pthread_create(&thread, &attr, fsc_command_run_locals_thread,
                             (void*)fsc))) {
	pthread_attr_destroy(&attr);
        FSC_ERROR("Cannot start new thread: %s", strerror(err));
        return -1;
    }
    pthread_attr_destroy(&attr);

    if((err = pthread_detach(thread))) {
        FSC_ERROR("Cannot detach new thread: %s", strerror(err));
    }

    FSC_VERBOSE("Local commands' thread created successfuly");

    return 0;
} /* fsc_command_run_locals */

void fsc_command_free (fsc_command_t *command) {
    size_t i;

    if(!command) {
        return;
    }

    free(command->exe);
    if(command->args) {
        for(i=0; command->args[i]; i++) {
            free(command->args[i]);
        }
        free(command->args);
    }
    free(command->in_dir);
    free(command->has_file);
    free(command->has_dir);
    free(command->os_id);
    free(command->os_id_like);
    free(command);
} /* fsc_command_free */

