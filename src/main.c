#include <fsc.h>
#include <misc.h>
#include <filetree.h>
#include <filters.h>
#include <syscalls.h>

#include <errno.h>

int main (int argc, char **argv) {
    int ret;
    fsc_t *fsc;

    fsc = fsc_init(argc, argv);
    if(!fsc) {
        ret = errno;
        goto main_err;
    }

    if(fsc_filter_add_all(fsc)) {
        fprintf(stderr, "ERROR: Cannot add default filters: %s\n",
                fsc_get_err_msg(fsc));
        FSC_ERROR("Cannot add default filters: %s", fsc_get_err_msg(fsc));
        ret = fsc_get_err(fsc);
        goto main_err;
    }

    if(fsc_scan(fsc, NULL)) {
        fprintf(stderr, "ERROR: Cannot scan filesystem: %s\n",
                fsc_get_err_msg(fsc));
        FSC_ERROR("Cannot scan filesystem: %s", fsc_get_err_msg(fsc));
        ret = fsc_get_err(fsc);
        goto main_err;
    }

    ret = fuse_main(fsc->fargs.argc, fsc->fargs.argv, &fsc_operations, fsc);
    if(ret) {
        fprintf(stderr, "ERROR: FUSE failed\n");
        FSC_ERROR("FUSE failed");
        goto main_err;
    }

    ret = 0;
main_err:
    fsc_free(fsc);
    return ret;
}

