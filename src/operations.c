#include <operations.h>

#include <stdlib.h>
#include <string.h>
#include <unistd.h>

fsc_operation_t* fsc_operation_new (fsc_t *fsc, const char *path,
                                    FSC_OPERATION_TYPE_T type) {
    fsc_operation_t *op;

    op = (fsc_operation_t*)calloc(1, sizeof(fsc_operation_t));
    if(!op) {
        FSC_ERR_SET(ENOMEM, "Out of memory");
        return NULL;
    }
    op->fsc = fsc; /* It is better not to leave it empty */
    op->type = type;
    op->path = strdup(path);
    if(!op->path) {
        FSC_ERR_SET(ENOMEM, "Out of memory");
        free(op);
        return NULL;
    }
    return op;
} /* fsc_operation_new */

void fsc_operation_free (fsc_operation_t *op) {
    if(!op) {
        return;
    }

    free(op->path);
    free(op->newpath);
    free(op);
} /* fsc_operation_free */

int fsc_operation_add (fsc_t *fsc, fsc_operation_t *op) {
    if(!fsc) {
        return -1;
    }
    if(!op) {
        FSC_ERR_SET(EINVAL, "NULL operation");
        return -1;
    }

    if(fsc->op_num == fsc->op_max && fsc_resize(&fsc->operations, &fsc->op_max,
                                                sizeof(fsc_operation_t*))) {
        FSC_ERR_SET(ENOMEM, "Out of memory");
        return -1;
    }
    fsc->operations[fsc->op_num] = op;
    fsc->op_num++;
    /* Be sure this operation uses correct fsc instance */
    op->fsc = fsc;

    return 0;
} /* fsc_operation_add */

int fsc_operation_run (fsc_operation_t *op) {
    fsc_t *fsc;

    if(!op) {
        return -1;
    }
    fsc = op->fsc;

    switch(op->type) {
        case FSC_OPERATION_TYPE_MKDIR:
            FSC_DEBUG("Executing: mkdir(%s, %d)", op->path, op->mode);
            if(mkdir(op->path, op->mode)) {
                FSC_ERR_SET(errno, "Cannot create '%s' directory: %s", op->path,
                            strerror(errno));
                return -1;
            }
            break;
        case FSC_OPERATION_TYPE_UNLINK:
            FSC_DEBUG("Executing: unlink(%s)", op->path);
            if(unlink(op->path)) {
                FSC_ERR_SET(errno, "Cannot unlink '%s' file: %s", op->path,
                            strerror(errno));
                return -1;
            }
            break;
        case FSC_OPERATION_TYPE_RMDIR:
            FSC_DEBUG("Executing: rmdir(%s)", op->path);
            if(rmdir(op->path)) {
                FSC_ERROR("Cannot remove '%s' directory: %s", op->path,
                          strerror(errno));
                return -1;
            }
            break;
        case FSC_OPERATION_TYPE_RENAME:
            FSC_DEBUG("Executing: move(%s, %s)", op->path, op->newpath);
            if(fsc_move_file(fsc, op->path, op->newpath)) {
                FSC_ERROR("Cannot move file '%s' to '%s'", op->path,
                          op->newpath);
                return -1;
            }
            break;
        default:
            FSC_ERR_SET(EINVAL, "Unknown operation type: %d", op->type);
            return -1;
    }

    return 0;
} /* fsc_operation_run */

int fsc_operations_run (fsc_t *fsc) {
    int ret = 0;
    size_t i;
    fsc_operation_t *op;

    if(!fsc) {
        return -1;
    }

    if(fsc->args.debug_flag) {
        FSC_DEBUG("List of registered operations:");
        for(i=0; i<fsc->op_num; i++) {
            op = fsc->operations[i];
            switch(op->type) {
                case FSC_OPERATION_TYPE_MKDIR:
                    FSC_DEBUG("mkdir(%s, %d)", op->path, op->mode);
                    break;
                case FSC_OPERATION_TYPE_UNLINK:
                    FSC_DEBUG("unlink(%s)", op->path);
                    break;
                case FSC_OPERATION_TYPE_RMDIR:
                    FSC_DEBUG("rmdir(%s)", op->path);
                    break;
                case FSC_OPERATION_TYPE_RENAME:
                    FSC_DEBUG("move(%s, %s)", op->path, op->newpath);
                    break;
                default:
                    FSC_DEBUG("Unknown operation type: %d", op->type);
                    return -1;
            }
        }
    }

    if(fsc->abort) {
        FSC_LOG("Operations execution has been disabled");
        return 0;
    }

    if(fsc->op_num) {
        for(i=0; i<fsc->op_num; i++) {
            if(fsc_operation_run(fsc->operations[i])) {
                FSC_ERROR("Cannot run the operation");
                ret = -1;
                /* Try to run all the operations */
            }
        }
    }

    return ret;
}

int fsc_operation_mkdir (fsc_t *fsc, const char *path, mode_t mode) {
    char *new_path = NULL;
    fsc_operation_t *op = NULL;

    if(!fsc) {
        return -1;
    }
    if(!path) {
        FSC_ERR_SET(EINVAL, "NULL path");
        return -1;
    }
    if(strlen(path) < lenof(FSC_MAIN_FILETREE_NAME) + 2) {
        FSC_ERR_SET(EINVAL, "Path too short");
        return -1;
    }

    new_path = (char*)malloc(strlen(fsc->scan_root_path) + 1 +
                             strlen(path) + 1 -
                             (lenof(FSC_MAIN_FILETREE_NAME) + 2));
                             /* Last one is for '/filetree/' */
    if(!new_path) {
        FSC_ERR_SET(ENOMEM, "Out of memory");
        return -1;
    }
    sprintf(new_path, "%s/%s", fsc->scan_root_path,
            path + lenof(FSC_MAIN_FILETREE_NAME) + 2);

    op = fsc_operation_new(fsc, new_path, FSC_OPERATION_TYPE_MKDIR);
    if(!op) {
        FSC_ERROR("Cannot create operation: %s", fsc_get_err_msg(fsc));
        free(new_path);
        return -1;
    }
    free(new_path);

    op->mode = mode;
    if(fsc_operation_add(fsc, op)) {
        fsc_operation_free(op);
        return -1;
    }

    return 0;
} /* fsc_operation_mkdir */

int fsc_operation_unlink (fsc_t *fsc, const char *path) {
    fsc_operation_t *op = NULL;

    if(!fsc) {
        return -1;
    }
    if(!path) {
        FSC_ERR_SET(EINVAL, "NULL path");
        return -1;
    }

    op = fsc_operation_new(fsc, path, FSC_OPERATION_TYPE_UNLINK);
    if(!op) {
        FSC_ERROR("Cannot create operation: %s", fsc_get_err_msg(fsc));
        return -1;
    }
    /* Type and path is enough to unlink a file */

    if(fsc_operation_add(fsc, op)) {
        fsc_operation_free(op);
        return -1;
    }

    return 0;
} /* fsc_operation_unlink */

int fsc_operation_rmdir (fsc_t *fsc, const char *path) {
    fsc_operation_t *op = NULL;

    if(!fsc) {
        return -1;
    }
    if(!path) {
        FSC_ERR_SET(EINVAL, "NULL path");
        return -1;
    }

    op = fsc_operation_new(fsc, path, FSC_OPERATION_TYPE_RMDIR);
    if(!op) {
        FSC_ERROR("Cannot create operation: %s", fsc_get_err_msg(fsc));
        return -1;
    }
    /* Type and path is enough to remove a directory */

    if(fsc_operation_add(fsc, op)) {
        fsc_operation_free(op);
        return -1;
    }

    return 0;
} /* fsc_operation_rmdir */

int fsc_operation_rename (fsc_t *fsc, const char *path,
                          const char *newpath){
    int ret = -1;
    char *path_from = NULL, *path_to = NULL;
    fsc_operation_t *op = NULL;

    if(!fsc) {
        return ret;
    }
    if(!path || !newpath) {
        FSC_ERR_SET(EINVAL, "NULL path");
        return ret;
    }
    if(strlen(path) < lenof(FSC_MAIN_FILETREE_NAME) + 2 ||
       strlen(newpath) < lenof(FSC_MAIN_FILETREE_NAME) + 2) {
        FSC_ERR_SET(EINVAL, "Path too short");
        return ret;
    }

    path_from = (char*)malloc(strlen(fsc->scan_root_path) + 1 +
                              strlen(path) + 1 -
                              (lenof(FSC_MAIN_FILETREE_NAME) + 2));
                              /* Last one is for '/filetree/' */
    if(!path_from) {
        FSC_ERR_SET(ENOMEM, "Out of memory");
        goto fsc_operation_rename_err;
    }
    sprintf(path_from, "%s/%s", fsc->scan_root_path,
            path + lenof(FSC_MAIN_FILETREE_NAME) + 2);

    path_to = (char*)malloc(strlen(fsc->scan_root_path) + 1 +
                            strlen(newpath) + 1 -
                            (lenof(FSC_MAIN_FILETREE_NAME) + 2));
                            /* Last one is for '/filetree/' */
    if(!path_to) {
        FSC_ERR_SET(ENOMEM, "Out of memory");
        goto fsc_operation_rename_err;
    }
    sprintf(path_to, "%s/%s", fsc->scan_root_path,
            newpath + lenof(FSC_MAIN_FILETREE_NAME) + 2);

    op = fsc_operation_new(fsc, path_from, FSC_OPERATION_TYPE_RENAME);
    if(!op) {
        FSC_ERROR("Cannot create operation: %s", fsc_get_err_msg(fsc));
        return ret;
    }
    op->newpath = path_to;
    path_to = NULL;

    if(fsc_operation_add(fsc, op)) {
        FSC_ERROR("Cannot add new operation");
        goto fsc_operation_rename_err;
    }
    op = NULL;

    ret = 0;
fsc_operation_rename_err:
    free(path_from);
    free(path_to);
    fsc_operation_free(op);
    return 0;
} /* fsc_operation_rename */

