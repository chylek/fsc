#include <fsc.h>
#include <filters.h>

static int fsc_f_init (fsc_t *fsc, fsc_filter_t *filter) {
    if(fsc_filter_add_dir(filter)) {
        FSC_ERROR("Cannot create filter's directory");
        return -1;
    }

    return 0;
} /* fsc_f_init */

static int sparse_file_cond (fsc_t *fsc, fsc_filter_t *filter,
                             fsc_file_t *file) {
    if(!S_ISLNK(file->st.st_mode) &&
       file->st.st_blocks * 512 < file->st.st_size) {
        return 1;
    } else {
        return 0;
    }
} /* sparse_file_cond */

static int fsc_f_run (fsc_t *fsc, fsc_filter_t *filter, fsc_file_t *file,
                           fsc_dir_t *dir) {
    if(file && fsc_filter_cond_file_link(filter, file, sparse_file_cond)) {
        FSC_ERROR("Cannot link file to filter's directory");
        return -1;
    }

    return 0;
} /* fsc_f_run */

fsc_filter_t FSC_FILTER_SPARSE_FILE = {
    .type = FSC_FILTER_TYPE_SPARSE_FILE,
    .type_desc = "sparse files",
    .dir_name = "sparse",
    .data = NULL,
    .dir = NULL,
    .init = fsc_f_init,
    .run = fsc_f_run,
    .finish = NULL,
    .file_data_free = NULL,
    .clean = NULL
};

