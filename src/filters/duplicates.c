#include <fsc.h>
#include <filters.h>

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <openssl/sha.h>

/* 1 kiB should be enough to differ most of files */
#define FSC_FILTER_SAME_FILES_READ_SIZE 1024

struct _fsc_f_data_t {
    size_t num;                 ///< Currently used space in files list
    size_t max;                 ///< Currently allocated space for files list
    fsc_file_t **files;         ///< List of files
};
typedef struct _fsc_f_data_t fsc_f_data_t;

struct _fsc_f_file_data_t {
    char *checksum_partial;     ///< Checksum calculated using small amount of data
    char *checksum;             ///< Checksum calculated using whole file
};
typedef struct _fsc_f_file_data_t fsc_f_file_data_t;

static int fsc_f_init (fsc_t *fsc, fsc_filter_t *filter) {
    fsc_f_data_t *fdata;

    fdata = (fsc_f_data_t*)malloc(sizeof(fsc_f_data_t));
    if(!fdata) {
        FSC_ERR_SET(ENOMEM, "Out of memory");
        return -1;
    }
    fdata->num = 0;
    fdata->max = FSC_ALLOC_ENTRIES;

    fdata->files = (fsc_file_t**)malloc(fdata->max * sizeof(fsc_file_t*));
    if(!fdata->files) {
        FSC_ERR_SET(ENOMEM, "Out of memory");
        free(fdata);
        return -1;
    }

    if(fsc_filter_add_dir(filter)) {
        FSC_ERROR("Cannot create filter's directory");
        free(fdata->files);
        free(fdata);
        return -1;
    }

    filter->data = (void*)fdata;

    return 0;
} /* fsc_f_init */

static int dups_check_perms (const struct stat *st) {
    uid_t uid = getuid();

    if(!uid) {
        /* Root can open and read every regular file */
        return 0;
    }
    if(st->st_uid == uid &&
      (st->st_mode & S_IRUSR || st->st_mode & S_IRWXU)) {
        return 0;
    }
    if(st->st_gid == getgid() &&
       (st->st_mode & S_IRGRP || st->st_mode & S_IRWXG)) {
        return 0;
    }
    if(st->st_mode & S_IROTH || st->st_mode & S_IRWXO) {
        return 0;
    }
    return -1;
} /* dups_check_perms */

static int fsc_f_run (fsc_t *fsc, fsc_filter_t *filter, fsc_file_t *file,
                      fsc_dir_t *dir) {
    fsc_f_data_t *fdata = (fsc_f_data_t*)filter->data;
    fsc_f_file_data_t *file_data;

    if(file) {
        /* 1 - 0 sized files are all the same and can have some special purpose
         * 2 - We are only interested in regular files
         * 3 - Avoid files we cannot open due to permissions */
        if(!file->st.st_size || !S_ISREG(file->st.st_mode) ||
           dups_check_perms(&file->st)) {
            return 0;
        }

        if(fdata->num == fdata->max &&
           fsc_resize(&fdata->files, &fdata->max, sizeof(fsc_file_t*))) {
            FSC_ERR_SET(ENOMEM, "Out of memory");
            return -1;
        }

        if(fsc_file_get_filter_data(file, filter, (void**)(&file_data))) {
            FSC_ERROR("Cannot get filter's data from file");
            return -1;
        }
        if(!file_data) {
            /* File data not set yet */
            file_data = (fsc_f_file_data_t*)calloc(1, sizeof(fsc_f_file_data_t));
            if(!file_data) {
                FSC_ERR_SET(ENOMEM, "Out of memory");
                return -1;
            }
            if(fsc_file_set_filter_data(file, filter, (void*)file_data)) {
                FSC_ERROR("Cannot set filter's data in file");
                free(file_data);
                return -1;
            }
        }

        /* Just insert new file, the array will be sorted at the end of the
         * scan */
        fdata->files[fdata->num] = file;
        fdata->num++;
    }

    return 0;
} /* fsc_f_run */

static int dups_cmp_size (const void *ptr1, const void *ptr2) {
    off_t size_diff;
    const fsc_file_t *file1 = *(const fsc_file_t* const*)ptr1;
    const fsc_file_t *file2 = *(const fsc_file_t* const*)ptr2;

    size_diff = file1->st.st_size - file2->st.st_size;

    return (int)size_diff;
} /* fsc_filter_cmp */

static int fsc_f_cmp_csum_part (const void *ptr1, const void *ptr2,
                                void *data) {
    off_t size_diff;
    size_t index = *(size_t*)data;
    const fsc_file_t *file1 = *(const fsc_file_t* const*)ptr1;
    const fsc_file_t *file2 = *(const fsc_file_t* const*)ptr2;
    fsc_f_file_data_t *data1 = (fsc_f_file_data_t*)file1->filters_data[index];
    fsc_f_file_data_t *data2 = (fsc_f_file_data_t*)file2->filters_data[index];
    const char *checksum1 = (const char*)data1->checksum_partial;
    const char *checksum2 = (const char*)data2->checksum_partial;

    if(file1->st.st_size == file2->st.st_size) {
        return strcmp(checksum1, checksum2);
    }

    size_diff = file1->st.st_size - file2->st.st_size;
    return (int)size_diff;
} /* fsc_f_cmp_csum_part */

static int fsc_f_cmp_csum (const void *ptr1, const void *ptr2, void *data) {
    off_t size_diff;
    size_t index = *(size_t*)data;
    const fsc_file_t *file1 = *(const fsc_file_t* const*)ptr1;
    const fsc_file_t *file2 = *(const fsc_file_t* const*)ptr2;
    fsc_f_file_data_t *data1 = (fsc_f_file_data_t*)file1->filters_data[index];
    fsc_f_file_data_t *data2 = (fsc_f_file_data_t*)file2->filters_data[index];
    const char *checksum1 = (const char*)data1->checksum;
    const char *checksum2 = (const char*)data2->checksum;

    if(file1->st.st_size == file2->st.st_size) {
        return strcmp(checksum1, checksum2);
    }

    size_diff = file1->st.st_size - file2->st.st_size;
    return (int)size_diff;
} /* fsc_f_cmp_csum */

static int dups_checksum (fsc_t *fsc, fsc_filter_t *filter, fsc_file_t *file,
                          int is_partial) {
    int ret = -1, fd = -1, file_removed = 0;
    size_t to_read_all, to_read_single; /* Do not allocate too big buffer, see
                                           comment below */
    ssize_t bytes;
    unsigned char *buff, *hash;
    fsc_f_file_data_t *file_data;
    SHA_CTX sha_ctx;

    if(fsc_file_get_filter_data(file, filter, (void**)(&file_data))) {
        FSC_ERROR("Cannot get filter's data from file");
        return ret;
    }
    if(!file_data) {
        FSC_ERROR("[Internal] NULL data in file");
        FSC_INTERNAL_ERR();
        return ret;
    }

    /* Check if we already have checksum of whole file */
    if(file->st.st_size <= FSC_FILTER_SAME_FILES_READ_SIZE &&
       file_data->checksum_partial) {
        file_data->checksum = strdup(file_data->checksum_partial);
        if(!file_data->checksum) {
            FSC_ERR_SET(ENOMEM, "Out of memory");
            goto dups_checksum_err;
        }
        return 0;
    }

    if(is_partial) {
        to_read_all = MIN(file->st.st_size, FSC_FILTER_SAME_FILES_READ_SIZE);
        to_read_single = to_read_all;
    } else {
        to_read_all = file->st.st_size;
        /* Do not use buffer as large as whole file
         * We want to avoid allocating GBs of memory */
        to_read_single = FSC_READ_SIZE;
    }

    buff = (unsigned char*)malloc(to_read_single);
    if(!buff) {
        FSC_ERR_SET(ENOMEM, "Out of memory");
        return ret;
    }
    hash = (unsigned char*)malloc(SHA_DIGEST_LENGTH + 1); /* '\0' at the end */
    if(!hash) {
        FSC_ERR_SET(ENOMEM, "Out of memory");
        goto dups_checksum_err;
    }
    if(!SHA1_Init(&sha_ctx)) { /* Returns 1 for success, 0 otherwise */
        FSC_ERR_SET(ENOMSG, "Cannot intialize SHA1 context");
        goto dups_checksum_err;
    }

    fd = open(file->orig_path, O_RDONLY);
    if(fd == -1) {
        if(errno == ENOENT) {
            /* Race condition - file has been removed after it was scanned */
            hash[0] = '\0';
            file_removed = 1;
            /* File is just ignored, cannot be removed from filetree as it
             * could be already used e.g. by another filter
             * fsc is, by design, exposed to race conditions like this */
        } else {
            FSC_ERR_SET(errno, "Cannot open '%s' file: %s", file->orig_path,
                        strerror(errno));
            goto dups_checksum_err;
        }
    }
    if(!file_removed) {
        while(to_read_all) {
            bytes = read(fd, buff, MIN(to_read_single, to_read_all));
            if(bytes <= 0) {
                if(bytes) {
                    FSC_ERR_SET(errno, "Cannot read '%s' file: %s",
                                file->orig_path, strerror(errno));
                    goto dups_checksum_err;
                }
                /* There is nothing more to read
                 * Can occure when file has been modified since it was scanned */
                break;
            }
            to_read_all -= bytes;

            if(!SHA1_Update(&sha_ctx, buff, bytes)) {
                FSC_ERR_SET(ENOMSG, "Cannot update SHA1 context");
                goto dups_checksum_err;
            }
        }

        if(!SHA1_Final(hash, &sha_ctx)) {
            FSC_ERR_SET(ENOMSG, "Cannot calculate SHA1 checksum");
            goto dups_checksum_err;
        }
        hash[SHA_DIGEST_LENGTH] = '\0';
    }

    if(is_partial) {
        if(file_data->checksum_partial) {
            FSC_DEBUG("Freeing partial checksum");
            free(file_data->checksum_partial);
        }
        file_data->checksum_partial = (char*)hash;
        hash = NULL;
    } else {
        if(file_data->checksum) {
            FSC_DEBUG("Freeing checksum");
            free(file_data->checksum);
        }
        file_data->checksum = (char*)hash;
        hash = NULL;
    }

    ret = 0;
dups_checksum_err:
    if(fd >= 0) {
        close(fd);
    }
    free(buff);
    free(hash);
    return ret;
} /* dups_checksum */

static int fsc_f_finish (fsc_t *fsc, fsc_filter_t *filter) {
    int ret = -1, dup_found;
    size_t i, j, dup_dir_num = 0, dup_file_num;
    char *checksum1, *checksum2, *checksum3, tmp_name[PATH_MAX];
    fsc_file_t *link;
    fsc_dir_t *base_dir, *dup_dir;
    fsc_f_file_data_t *file_data;
    fsc_f_data_t *fdata = (fsc_f_data_t*)filter->data;

    /* No files found or single file found = no duplicated files */
    if(fdata->num < 2) {
        return 0;
    }

    base_dir = filter->dir;
    if(!base_dir) {
        FSC_ERROR("[Internal] NULL filter's directory");
        FSC_INTERNAL_ERR();
        return ret;
    }

    /* Sort files by size */
    qsort((void*)fdata->files, fdata->num, sizeof(*fdata->files),
          dups_cmp_size);

    for(i=0; i<fdata->num; i++) {
        if((i == 0 || fdata->files[i - 1]->st.st_size != fdata->files[i]->st.st_size) &&
           (i + 1 >= fdata->num || fdata->files[i]->st.st_size != fdata->files[i + 1]->st.st_size)) {
            for(j=i+1; j<fdata->num; j++) {
                fdata->files[j - 1] = fdata->files[j];
            }
            fdata->num--;
        }
    }
    if(fdata->num < 2) {
        /* There are no possible duplicates */
        ret = 0;
        goto fsc_f_finish_err;
    }

    /* Calculate checksums using only a part of the file
     * This can eventually readuce number of reads improving overall
     * performance as we already can distinguish many different files
     * without reading their whole content */
    for(i=0; i<fdata->num; i++) {
        if(dups_checksum(fsc, filter, fdata->files[i], 1)) {
            FSC_ERROR("Cannot count partial checksum");
            goto fsc_f_finish_err;
        }
    }

    /* Sort files by partial checksum */
    qsort_r(fdata->files, fdata->num, sizeof(fdata->files[0]),
            fsc_f_cmp_csum_part, &filter->index);

    /* Drop files with unique partial checksum */
    if(fsc_file_get_filter_data(fdata->files[0], filter,
                                (void**)(&file_data))) {
        FSC_ERROR("Cannot get filter's data from file");
        goto fsc_f_finish_err;
    }
    if(!file_data) {
        FSC_ERROR("[Internal] NULL data in file");
        FSC_INTERNAL_ERR();
        goto fsc_f_finish_err;
    }
    checksum1 = NULL;
    checksum2 = NULL;
    checksum3 = file_data->checksum_partial;
    for(i=0; i<fdata->num; i++) {
        checksum1 = checksum2;
        checksum2 = checksum3;
        if(i < fdata->num - 1) {
            if(fsc_file_get_filter_data(fdata->files[i + 1], filter,
                                        (void**)(&file_data))) {
                FSC_ERROR("Cannot get filter's data from file");
                goto fsc_f_finish_err;
            }
            if(!file_data) {
                FSC_ERROR("[Internal] NULL data in file");
                FSC_INTERNAL_ERR();
                goto fsc_f_finish_err;
            }
            checksum3 = file_data->checksum_partial;
        } else {
            file_data = NULL;
            checksum3 = NULL;
        }

        while((!checksum1 || strcmp(checksum1, checksum2)) &&
              (!checksum3 || strcmp(checksum2, checksum3))) {
            for(j=i+1; j<fdata->num; j++) {
                fdata->files[j - 1] = fdata->files[j];
            }
            fdata->num--;

            if(i >= fdata->num - 1) {
                break;
            }
            checksum2 = checksum3;
            if(fsc_file_get_filter_data(fdata->files[i + 1], filter,
                                        (void**)(&file_data))) {
                FSC_ERROR("Cannot get filter's data from file");
                goto fsc_f_finish_err;
            }
            if(!file_data) {
                FSC_ERROR("[Internal] NULL data in file");
                FSC_INTERNAL_ERR();
                goto fsc_f_finish_err;
            }
            checksum3 = file_data->checksum_partial;
        }
    }
    if(fdata->num < 2) {
        /* There are no possible duplicates */
        ret = 0;
        goto fsc_f_finish_err;
    }

    /* Calculate checksums using whole file
     * Do not go that much into performance improvements to try to read
     * more and more up to the size of the file */
    for(i=0; i<fdata->num; i++) {
        if(dups_checksum(fsc, filter, fdata->files[i], 0)) {
            FSC_ERROR("Cannot count full checksum");
            goto fsc_f_finish_err;
        }
    }

    /* Sort files by full checksum */
    qsort_r(fdata->files, fdata->num, sizeof(fdata->files[0]),
            fsc_f_cmp_csum, &filter->index);

    /* Drop files with unique full checksum */
    if(fsc_file_get_filter_data(fdata->files[0], filter,
                                (void**)(&file_data))) {
        FSC_ERROR("Cannot get filter's data from file");
        goto fsc_f_finish_err;
    }
    if(!file_data) {
        FSC_ERROR("[Internal] NULL data in file");
        FSC_INTERNAL_ERR();
        goto fsc_f_finish_err;
    }
    checksum1 = NULL;
    checksum2 = NULL;
    checksum3 = file_data->checksum;
    for(i=0; i<fdata->num; i++) {
        checksum1 = checksum2;
        checksum2 = checksum3;
        if(i < fdata->num - 1) {
            if(fsc_file_get_filter_data(fdata->files[i + 1], filter,
                                        (void**)(&file_data))) {
                FSC_ERROR("Cannot get filter's data from file");
                goto fsc_f_finish_err;
            }
            if(!file_data) {
                FSC_ERROR("[Internal] NULL data in file");
                FSC_INTERNAL_ERR();
                goto fsc_f_finish_err;
            }
            checksum3 = file_data->checksum;
        } else {
            file_data = NULL;
            checksum3 = NULL;
        }

        while((!checksum1 || strcmp(checksum1, checksum2)) &&
              (!checksum3 || strcmp(checksum2, checksum3))) {
            for(j=i+1; j<fdata->num; j++) {
                fdata->files[j - 1] = fdata->files[j];
            }
            fdata->num--;

            if(i >= fdata->num - 1) {
                break;
            }
            checksum2 = checksum3;
            if(fsc_file_get_filter_data(fdata->files[i + 1], filter,
                                        (void**)(&file_data))) {
                FSC_ERROR("Cannot get filter's data from file");
                goto fsc_f_finish_err;
            }
            if(!file_data) {
                FSC_ERROR("[Internal] NULL data in file");
                FSC_INTERNAL_ERR();
                goto fsc_f_finish_err;
            }
            checksum3 = file_data->checksum;
        }
    }

    if(fdata->num > 1) {
        /* Link duplicated files into the same directories */
        dup_found = 0;
        if(fsc_file_get_filter_data(fdata->files[0], filter,
                                    (void**)(&file_data))) {
            FSC_ERROR("Cannot get filter's data from file");
            goto fsc_f_finish_err;
        }
        if(!file_data) {
            FSC_ERROR("[Internal] NULL data in file");
            FSC_INTERNAL_ERR();
            goto fsc_f_finish_err;
        }
        checksum2 = file_data->checksum;
        for(i=1; i<fdata->num; i++) {
            checksum1 = checksum2;
            if(fsc_file_get_filter_data(fdata->files[i], filter,
                                        (void**)(&file_data))) {
                FSC_ERROR("Cannot get filter's data from file");
                goto fsc_f_finish_err;
            }
            if(!file_data) {
                FSC_ERROR("[Internal] NULL data in file");
                FSC_INTERNAL_ERR();
                goto fsc_f_finish_err;
            }
            checksum2 = file_data->checksum;

            while(i < fdata->num && !strcmp(checksum1, checksum2)) {
                if(!dup_found) {
                    snprintf(tmp_name, sizeof(tmp_name), "%lu", dup_dir_num);
                    dup_dir = fsc_dir_new(fsc, tmp_name, &FSC_DIR_ST);
                    if(!dup_dir) {
                        FSC_ERROR("Cannot create virtual directory");
                        goto fsc_f_finish_err;
                    }
                    if(fsc_dir_add(base_dir, dup_dir)) {
                        FSC_ERROR("Cannot add virtual directory");
                        fsc_dir_free(dup_dir);
                        goto fsc_f_finish_err;
                    }

                    dup_file_num = 0;
                    link = fsc_file_link(fdata->files[i-1]);
                    if(!link) {
                        FSC_ERROR("Cannot create virtual link");
                        goto fsc_f_finish_err;
                    }
                    snprintf(tmp_name, sizeof(tmp_name), "%lu", dup_file_num);
                    free(link->name);
                    link->name = strdup(tmp_name);
                    if(!link->name) {
                        FSC_ERR_SET(ENOMEM, "Out of memory");
                        fsc_file_free(link);
                        goto fsc_f_finish_err;
                    }
                    if(fsc_file_add(dup_dir, link)) {
                        FSC_ERROR("Cannot add virtual link");
                        fsc_file_free(link);
                        goto fsc_f_finish_err;
                    }
                    dup_file_num++;
                }

                link = fsc_file_link(fdata->files[i]);
                if(!link) {
                    FSC_ERROR("Cannot create virtual link");
                    goto fsc_f_finish_err;
                }
                snprintf(tmp_name, sizeof(tmp_name), "%lu", dup_file_num);
                free(link->name);
                link->name = strdup(tmp_name);
                if(!link->name) {
                    FSC_ERR_SET(ENOMEM, "Out of memory");
                    fsc_file_free(link);
                    goto fsc_f_finish_err;
                }
                if(fsc_file_add(dup_dir, link)) {
                    FSC_ERROR("Cannot add virtual link");
                    fsc_file_free(link);
                    goto fsc_f_finish_err;
                }
                dup_file_num++;

                dup_found = 1;
                i++;

                checksum1 = checksum2;
                if(fsc_file_get_filter_data(fdata->files[i], filter,
                                            (void**)(&file_data))) {
                    FSC_ERROR("Cannot get filter's data from file");
                    goto fsc_f_finish_err;
                }
                if(!file_data) {
                    FSC_ERROR("[Internal] NULL data in file");
                    FSC_INTERNAL_ERR();
                    goto fsc_f_finish_err;
                }
                checksum2 = file_data->checksum;
            }
            if(dup_found) {
                dup_dir_num++;
            }
            dup_found = 0;
        }
    }

    FSC_LOG("Found %lu files that have duplicates", dup_dir_num);

    ret = 0;
fsc_f_finish_err:
    free(fdata->files);
    free(fdata); /* Equivalent of free(filter->data) */
    filter->data = NULL;
    return ret;
} /* fsc_f_finish */

static void fsc_f_file_free (fsc_t *fsc, fsc_filter_t *filter, void *data) {
    fsc_f_file_data_t *file_data;

    if(!data) {
        return;
    }
    file_data = (fsc_f_file_data_t*)data;
    free(file_data->checksum_partial);
    free(file_data->checksum);
    free(file_data);
} /* fsc_f_file_free */

static void fsc_f_clean (fsc_t *fsc, fsc_filter_t *filter) {
    fsc_f_data_t *fdata = filter->data;

    if(!fdata) {
        return;
    }
    free(fdata->files);
    free(fdata); /* Equivalent of free(filter->data) */
    filter->data = NULL;
} /* fsc_f_clean */

fsc_filter_t FSC_FILTER_SAME_FILES = {
    .type = FSC_FILTER_TYPE_SAME_FILES,
    .type_desc = "files duplicates",
    .dir_name = "duplicates",
    .data = NULL, /* Will be set on init */
    .dir = NULL,
    .init = fsc_f_init,
    .run = fsc_f_run,
    .finish = fsc_f_finish,
    .file_data_free = fsc_f_file_free,
    .clean = fsc_f_clean
};

