#include <fsc.h>
#include <filters.h>

static int fsc_f_init (fsc_t *fsc, fsc_filter_t *filter) {
    if(fsc_filter_add_dir(filter)) {
        FSC_ERROR("Cannot create filter's directory");
        return -1;
    }

    return 0;
} /* fsc_f_init */

static int zero_file_cond (fsc_t *fsc, fsc_filter_t *filter, fsc_file_t *file) {
    if(file->st.st_size == 0) {
        return 1;
    } else {
        return 0;
    }
} /* zero_file_cond */

static int fsc_f_run (fsc_t *fsc, fsc_filter_t *filter, fsc_file_t *file,
                      fsc_dir_t *dir) {
    if(file && fsc_filter_cond_file_link(filter, file, zero_file_cond)) {
        FSC_ERROR("Cannot link file to filter's directory");
        return -1;
    }

    return 0;
} /* fsc_f_run */

fsc_filter_t FSC_FILTER_ZERO_FILE = {
    .type = FSC_FILTER_TYPE_ZERO_FILE,
    .type_desc = "zero sized files",
    .dir_name = "zero-size",
    .data = NULL,
    .dir = NULL,
    .init = fsc_f_init,
    .run = fsc_f_run,
    .finish = NULL,
    .file_data_free = NULL,
    .clean = NULL
};

