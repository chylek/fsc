#include <fsc.h>
#include <filters.h>

#include <stdlib.h>
#include <string.h>

fsc_filter_t* fsc_filter_new (fsc_t *fsc,
                              const char *desc,
                              const char *dir_name,
                              void *data,
                              fsc_filter_cb_init init,
                              fsc_filter_cb_run run,
                              fsc_filter_cb_finish finish,
                              fsc_filter_cb_file_data_free fd_free,
                              fsc_filter_cb_clean clean) {
    fsc_filter_t *filter;

    if(!desc) {
        FSC_ERR_SET(EINVAL, "NULL filter description");
        return NULL;
    }
    if(!init && !run && !finish && !clean) {
        FSC_ERR_SET(EINVAL, "No operation defined");
        return NULL;
    }
    if(data && !fd_free) {
        FSC_LOG("WARNING: Filter's data registered without callback"
                " to free it - be sure what you are doing");
    }

    /* Do not use fsc_filter_free() as it calls ->clean() callback */
    filter = (fsc_filter_t*)malloc(sizeof(fsc_filter_t));
    if(!filter) {
        FSC_ERR_SET(ENOMEM, "Out of memory");
        return NULL;
    }
    filter->index = -1;

    filter->fsc = fsc;
    filter->type = FSC_FILTER_TYPE_USER_DEFINED;
    if(desc) {
        fsc_strncpy(filter->type_desc, desc, FSC_FILTER_DESC_LEN);
    } else {
        filter->type_desc[0] = '\0';
    }
    if(dir_name) {
        fsc_strncpy(filter->dir_name, dir_name, FSC_FILTER_DIR_NAME_LEN);
    } else {
        filter->dir_name[0] = '\0';
    }

    filter->data = data;
    filter->init = init;
    filter->run = run;
    filter->finish = finish;
    filter->file_data_free = fd_free;
    filter->clean = clean;

    return filter;
} /* fsc_filter_new */

void fsc_filter_free (fsc_filter_t *filter) {
    if(!filter) {
        return;
    }

    if(filter->clean) {
        filter->clean(filter->fsc, filter);
    }

    free(filter);
} /* fsc_filter_free */

fsc_filter_t* fsc_filter_dup (fsc_filter_t *filter) {
    fsc_t *fsc;
    fsc_filter_t *filter_copy;

    if(!filter) {
        /* No error set since we cannot get fsc_t* anyway */
        return NULL;
    }
    fsc = filter->fsc;

    filter_copy = (fsc_filter_t*)malloc(sizeof(fsc_filter_t));
    if(!filter_copy) {
        FSC_ERR_SET(EINVAL, "Out of memory");
        return NULL;
    }
    memcpy(filter_copy, filter, sizeof(fsc_filter_t));

    fsc_strncpy(filter_copy->type_desc, filter->type_desc, FSC_FILTER_DESC_LEN);
    fsc_strncpy(filter_copy->dir_name, filter->dir_name,
                FSC_FILTER_DIR_NAME_LEN);

    return filter_copy;
} /* fsc_filter_dup */

int fsc_filter_same (fsc_filter_t *filter1, fsc_filter_t *filter2) {
    if(filter1->type != filter2->type) {
        return 0;
    }

    if(filter1->type == FSC_FILTER_TYPE_USER_DEFINED) {
        if(!strncmp(filter1->type_desc, filter2->type_desc,
                    FSC_FILTER_DESC_LEN)) {
            return 1;
        } else {
            return 0;
        }
    }

    return 1;
} /* fsc_filter_same */

int fsc_filter_add_dir (fsc_filter_t *filter) {
    fsc_t *fsc;
    fsc_dir_t *filter_dir;

    if(!filter) {
        return -1;
    }
    fsc = filter->fsc;

    if(FSC_STRING_IS_EMPTY(filter->dir_name)) {
        FSC_LOG("WARNING: Filter without directory name");
        return 0;
    }

    filter_dir = fsc_dir_new(fsc, filter->dir_name, &FSC_DIR_ST);
    if(!filter_dir) {
        FSC_ERROR("Cannot create virtual directory");
        return -1;
    }
    if(fsc_dir_add(fsc->root, filter_dir)) {
        FSC_ERROR("Cannot add virtual directory");
        fsc_dir_free(filter_dir);
        return -1;
    }

    filter->dir = filter_dir;

    return 0;
} /* fsc_filter_add_dir */

int fsc_filter_cond_file_link (fsc_filter_t *filter, fsc_file_t *file,
                               int(*cond)(fsc_t *fsc, fsc_filter_t *filter,
                                          fsc_file_t*)) {
    int ret = -1, ret_val;
    long long int nr = -1;
    char *new_name, path[PATH_MAX];
    fsc_file_t *link = NULL;
    fsc_t *fsc = NULL;

    if(!filter) {
        return -1;
    }
    fsc = filter->fsc;
    if(!file) {
        FSC_ERR_SET(EINVAL, "NULL file");
        return ret;
    }
    if(!cond) {
        FSC_ERR_SET(EINVAL, "NULL callback");
        return ret;
    }
    if(!filter->dir) {
        FSC_ERR_SET(EINVAL, "NULL filter's directory");
        return ret;
    }

    if((ret_val = cond(fsc, filter, file))) {
        if(ret_val < 0) {
            FSC_ERROR("Filter's condition check failed");
            return ret;
        }

        link = fsc_file_link(file);
        if(!link) {
            FSC_ERROR("Cannot create virtual link");
            return ret;
        }

        /* Let's assume there will not be more than LLONG_MAX entries with the
         * same name */
        snprintf(path, sizeof(path), "%s", file->name);
        while(fsc_find_entry(filter->dir->files, filter->dir->nfiles, path,
                             fsc_file_cmp) >= 0) {
            nr++;
            snprintf(path, sizeof(path), "%s (%lld)", file->name, nr);
        }

        if(nr >= 0) { /* Avoid unnecessary work */
            new_name = strdup(path);
            if(!new_name) {
                FSC_ERR_SET(ENOMEM, "Out of memory");
                goto fsc_filter_cond_file_link_err;
            }
            free(link->name);
            link->name = new_name;
            new_name = NULL;
        }

        if(fsc_file_add(filter->dir, link)) {
            FSC_ERROR("Cannot add link to '%s' directory", filter->dir_name);
            goto fsc_filter_cond_file_link_err;
        }
        link = NULL;
    } else {
        FSC_VERBOSE("File filtered out: %s", file->orig_path ? file->orig_path :
                    fsc_file_get_path(file, path, sizeof(path)));
    }

    ret = 0;
fsc_filter_cond_file_link_err:
    fsc_file_free(link);
    return ret;
} /* fsc_filter_cond_link_file */

const char* fsc_filter_get_desc (fsc_filter_t *filter) {
    fsc_t *fsc;

    if(!filter) {
        return "(NULL filter)";
    }
    fsc = filter->fsc;

    switch(filter->type) {
        case FSC_FILTER_TYPE_ZERO_FILE:
        case FSC_FILTER_TYPE_SPARSE_FILE:
        case FSC_FILTER_TYPE_MEDIA_TYPE:
            if(!FSC_STRING_IS_EMPTY(filter->type_desc)) {
                return filter->type_desc;
            } else {
                return "(not set)";
            }
        case FSC_FILTER_TYPE_USER_DEFINED:
            if(!FSC_STRING_IS_EMPTY(filter->type_desc)) {
                return filter->type_desc;
            } else {
                return "(not set by user)";
            }
        default:
            FSC_ERR_SET(EINVAL, "Unknown filter type");
            return "(unknown filter)";
    }

    /* no return required here */
} /* fsc_filter_get_desc */

int fsc_filter_add (fsc_t *fsc, fsc_filter_t *filter) {
    size_t i;

    if(!fsc) {
        return -1;
    }
    if(!filter) {
        FSC_ERR_SET(EINVAL, "NULL filter");
        return -1;
    }

    for(i=0; i<fsc->filters_num; i++) {
        if(fsc_filter_same(fsc->filters[i], filter)) {
            FSC_ERR_SET(EINVAL, "Filter already registered");
            return -1;
        }

        if(!strncmp(filter->dir_name, fsc->filters[i]->dir_name,
                    FSC_FILTER_DIR_NAME_LEN)) {
            FSC_ERR_SET(EINVAL, "Filter's directory name already in use: %s",
                        filter->dir_name);
            return -1;
        }
    }
    if(!strncmp(FSC_MAIN_FILETREE_NAME, filter->dir_name,
                FSC_FILTER_DIR_NAME_LEN)) {
        FSC_ERR_SET(EINVAL, "Reserved filter's dir name: %s",
                    FSC_MAIN_FILETREE_NAME);
        return -1;
    }

    if(fsc->filters_num == fsc->filters_max &&
       fsc_resize(&fsc->filters, &fsc->filters_max, sizeof(fsc_filter_t))) {
        FSC_ERR_SET(ENOMEM, "Out of memory");
        return -1;
    }

    filter->index = fsc->filters_num;
    fsc->filters[fsc->filters_num] = filter;
    fsc->filters_num++;

    return 0;
} /* fsc_filter_add */

int fsc_filter_add_all (fsc_t *fsc) {
    size_t i;
    const FSC_FILTER_TYPE_T filter_types[] = {
                        FSC_FILTER_TYPE_ZERO_FILE,
                        FSC_FILTER_TYPE_SPARSE_FILE,
                        FSC_FILTER_TYPE_MEDIA_TYPE,
                        FSC_FILTER_TYPE_SAME_FILES
                    };

    for(i=0; i<sizeof_array(filter_types); i++) {
        if(fsc_filter_add_default(fsc, filter_types[i])) {
            FSC_ERROR("Cannot add default filter");
            return -1;
        }
    }

    return 0;
} /* fsc_filter_add_all */

int fsc_filters_init (fsc_t *fsc) {
    FSC_FILTER_ZERO_FILE.fsc = fsc;
    FSC_FILTER_SPARSE_FILE.fsc = fsc;
    FSC_FILTER_MEDIA_TYPE.fsc = fsc;
    FSC_FILTER_SAME_FILES.fsc = fsc;

    return 0;
} /* fsc_filters_init */

void fsc_filters_clean (void) {
    /* Do not drop this immediately - this is a good placeholder for potential
     * future needs */
} /* fsc_filters_clean */

int fsc_filter_add_default (fsc_t *fsc, FSC_FILTER_TYPE_T type) {
    int ret = -1;
    fsc_filter_t *filter_ptr = NULL, *new_filter;

    switch(type) {
        case FSC_FILTER_TYPE_ZERO_FILE:
            filter_ptr = &FSC_FILTER_ZERO_FILE;
            break;
        case FSC_FILTER_TYPE_SPARSE_FILE:
            filter_ptr = &FSC_FILTER_SPARSE_FILE;
            break;
        case FSC_FILTER_TYPE_MEDIA_TYPE:
            filter_ptr = &FSC_FILTER_MEDIA_TYPE;
            break;
        case FSC_FILTER_TYPE_SAME_FILES:
            filter_ptr = &FSC_FILTER_SAME_FILES;
            break;
        case FSC_FILTER_TYPE_USER_DEFINED:
            FSC_ERR_SET(EINVAL, "User defined filter is not a default one");
            goto fsc_filter_add_default_err;
            /* No break needed */
        default:
            FSC_ERR_SET(EINVAL, "Unknown filter type");
            goto fsc_filter_add_default_err;
            /* No break needed */
    }

    new_filter = fsc_filter_dup(filter_ptr);
    if(!new_filter) {
        FSC_ERROR("Cannot copy filter");
        goto fsc_filter_add_default_err;
    }

    if((ret = fsc_filter_add(fsc, new_filter))) {
        FSC_ERROR("Cannot add default filter");
        fsc_filter_free(new_filter);
    }
fsc_filter_add_default_err:
    return ret;
} /* fsc_filter_add_default */

