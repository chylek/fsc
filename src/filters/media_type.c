#include <fsc.h>
#include <filters.h>

#include <stdlib.h>
#include <string.h>

static int fsc_file_categorize (fsc_t *fsc, fsc_filter_t *filter,
                                fsc_file_t *file, const char *mime_type) {
    int ret = -1;
    long long int nr = -1;
    char *new_name, path[PATH_MAX];
    fsc_file_t *link;
    fsc_dir_t *media_dir;

    link = fsc_file_link(file);
    if(!link) {
        FSC_ERROR("Cannot create virtual link");
        return ret;
    }

    FSC_LOG("%s: %s", file->name, mime_type);
    snprintf(path, sizeof(path), "/%s/%s/", filter->dir_name, mime_type);
    media_dir = fsc_mkpath(fsc->root, path);
    if(!media_dir) {
        FSC_ERROR("Cannot create path: '%s'", path);
        goto fsc_file_categorize_err;
    }

    /* Let's assume there will not be more than LLONG_MAX entries with the same
     * name */
    snprintf(path, sizeof(path), "%s", file->name);
    while(fsc_find_entry(media_dir->files, media_dir->nfiles, path,
                         fsc_file_cmp) >= 0) {
        nr++;
        snprintf(path, sizeof(path), "%s (%lld)", file->name, nr);
    }

    if(nr >= 0) { /* Avoid unnecessary work */
        new_name = strdup(path);
        if(!new_name) {
            FSC_ERR_SET(ENOMEM, "Out of memory");
            goto fsc_file_categorize_err;
        }
        free(link->name);
        link->name = new_name;
        new_name = NULL;
    }

    if(fsc_file_add(media_dir, link)) {
        FSC_ERROR("Cannot add link to '%s' directory", filter->dir_name);
        goto fsc_file_categorize_err;
    }
    link = NULL;

    ret = 0;
fsc_file_categorize_err:
    fsc_file_free(link);
    return ret;
} /* fsc_file_categorize */

static int fsc_f_run (fsc_t *fsc, fsc_filter_t *filter, fsc_file_t *file,
                      fsc_dir_t *dir) {
    char *mime_type;
    const char *mime_type_const;

    if(file) {
        mime_type_const = magic_file(fsc->magic_cookie, file->orig_path);
        if(!mime_type_const) {
            FSC_ERR_SET(magic_errno(fsc->magic_cookie),
                        "Cannot get '%s' mime type: %s",
                        file->orig_path, magic_error(fsc->magic_cookie));
            return -1;
        }
        mime_type = strdup(mime_type_const);
        if(!mime_type) {
            FSC_ERR_SET(ENOMEM, "Out of memory");
            return -1;
        }

        if(fsc_file_set_filter_data(file, filter, (void*)mime_type)) {
            FSC_ERROR("Cannot set filter's data in file");
            free(mime_type);
            return -1;
        }

        if(fsc_file_categorize(fsc, filter, file, mime_type)) {
            FSC_ERROR("Cannot categorize the file");
            return -1;
        }
    }
    return 0;
} /* fsc_f_run */

static void fsc_f_file_free (fsc_t *fsc, fsc_filter_t *filter, void *data) {
    char *mime_type;

    if(!data) {
        return;
    }
    mime_type = (char*)data;
    free(mime_type);
} /* fsc_f_file_free */

fsc_filter_t FSC_FILTER_MEDIA_TYPE = {
    .type = FSC_FILTER_TYPE_MEDIA_TYPE,
    .type_desc = "media type",
    .dir_name = "media",
    .data = NULL,
    .dir = NULL,
    .init = NULL,
    .run = fsc_f_run,
    .finish = NULL,
    .file_data_free = fsc_f_file_free,
    .clean = NULL
};

