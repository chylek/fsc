#include <fsc.h>
#include <json_tokens.h>

#include <string.h>
#include <jansson.h>

int fsc_json_export (fsc_t *fsc, const char *path) {
    int ret = -1;
    size_t i, j;
    fsc_command_t *command;
    json_t *json = NULL, *value = NULL, *include = NULL, *commands = NULL,
           *command_args = NULL;
    json_error_t err;

    if(!fsc) {
        return ret;
    }
    if(!path) {
        fprintf(stderr, "ERROR: NULL filename\n");
        return EINVAL;
    }

    if(fsc->exl_num > 0) {
        include = json_array();
        if(!include) {
            fprintf(stderr, "ERROR: Cannot create 'include' JSON array\n");
            goto fsc_json_export_err;
        }

        for(i=0; i<fsc->exl_num; i++) {
            value = json_pack_ex(&err, 0, "s+",
                                 fsc->exclusion_list[i]->exclude ? "-" : "+",
                                 fsc->exclusion_list[i]->pattern);
            if(!value) {
                fprintf(stderr, "ERROR: Cannot create 'include' JSON object: "
                        "%s\n", err.text);
                goto fsc_json_export_err;
            }

            if(json_array_append_new(include, value)) {
                fprintf(stderr, "ERROR: Cannot add new inclusion pattern to "
                        "array\n");
                goto fsc_json_export_err;
            }
            /* Inner value of the 'value' has been stolen by the array [ref] */
            value = NULL;
        }
    }

    if(fsc->cmd_num > 0) {
        commands = json_array();
        if(!commands) {
            fprintf(stderr, "ERROR: Cannot create 'commands' JSON array\n");
            goto fsc_json_export_err;
        }

        for(i=0; i<fsc->cmd_num; i++) {
            command = fsc->commands[i];

            if(command->args[1]) {
                /* There is at least one argument
                 * The very first argument should be the executable name
                 */
                command_args = json_array();
                if(!command_args) {
                    fprintf(stderr, "ERROR: Cannot create 'args' JSON array\n");
                    goto fsc_json_export_err;
                }

                for(j=1; command->args[j]; j++) {
                    value = json_pack_ex(&err, 0, "s", command->args[j]);
                    if(!value) {
                        fprintf(stderr, "ERROR: Cannot create 'arg' JSON "
                                "object: %s\n", err.text);
                        goto fsc_json_export_err;
                    }

                    if(json_array_append_new(command_args, value)) {
                        fprintf(stderr, "ERROR: Cannot add new argument to "
                                "array\n");
                        goto fsc_json_export_err;
                    }
                    value = NULL; /* See above [ref] */
                }
            }

            value = json_pack_ex(&err, 0, "{s:s, s:o*, s:b, s:b, s:s, s:s*, "
                                 "s:s*, s:s*, s:s*, s:s*}",
                                 "exe", command->exe,
                                 "args", command_args,
                                 "sudo", command->sudo,
                                 "global", command->global,
                                 "logic", command->logic_or ? "or" : "and",
                                 "in dir", command->in_dir,
                                 "has file", command->has_file,
                                 "has dir", command->has_dir,
                                 "os id", command->os_id,
                                 "os id like", command->os_id_like);
            if(!value) {
                fprintf(stderr, "ERROR: Cannot create 'command' JSON object: "
                        "%s\n", err.text);
                goto fsc_json_export_err;
            }
            command_args = NULL;

            if(json_array_append_new(commands, value)) {
                fprintf(stderr, "ERROR: Cannot add new command to array\n");
                goto fsc_json_export_err;
            }
            value = NULL; /* See above [ref] */
        }
    }

    json = json_pack_ex(&err, 0, "{s:s, s:s, s:s, s:i, s:o*, s:b, s:b, s:b, "
                        "s:b, s:o*}",
                        "mountpoint", fsc->mountpoint,
                        "logfile", fsc->logfile_path,
                        "scan root", fsc->scan_root_path,
                        /* See setting the value below or in fsc.c:fsc_init */
                        "open limit", fsc->fh_limit - 1,
                        "include", include,
                        "debug", fsc->args.debug_flag,
                        "verbose", fsc->args.verbose_flag,
                        "fuse single threaded",
                        fsc->args.fuse_single_threaded_flag,
                        "fuse debug", fsc->args.fuse_debug_flag,
                        "commands", commands);
    if(!json) {
        fprintf(stderr, "ERROR: Cannot create new JSON object: %s\n",
                err.text);
        goto fsc_json_export_err;
    }
    include = NULL; /* See above [ref] */
    commands = NULL; /* See above [ref] */

    if(json_dump_file(json, path, JSON_INDENT(4))) {
        fprintf(stderr, "ERROR: Cannot export JSON to '%s' file\n",
                path);
        goto fsc_json_export_err;
    }

    ret = 0;
fsc_json_export_err:
    if(json) {
        json_decref(json);
    }
    if(value) {
        json_decref(value);
    }
    if(include) {
        json_decref(include);
    }
    if(commands) {
        json_decref(commands);
    }
    if(command_args) {
        json_decref(command_args);
    }
    return ret;
} /* fsc_json_export */

int fsc_json_set_mountpoint (fsc_t *fsc,
                             struct _fsc_json_handle_t *handle,
                             json_t *object) {
    int err;
    const char *mountpoint_str;
    char *mountpoint_path;

    if(fsc->args.inputs_num > 0) {
        /* Commandline options have higher priority than configuration files */
        return 0;
    }

    mountpoint_str = json_string_value(object);
    if(!mountpoint_str) {
        fprintf(stderr, "ERROR: Out of memory\n");
        return ENOMEM;
    }

    mountpoint_path = realpath(mountpoint_str, NULL);
    if(!mountpoint_path) {
        err = errno;
        fprintf(stderr, "ERROR: Cannot simplify mountpoint path: '%s': %s\n",
                mountpoint_str, strerror(err));
        return err;
    }

    if(fsc->mountpoint) {
        free(fsc->mountpoint);
    }
    fsc->mountpoint = mountpoint_path;

    return 0;
} /* fsc_json_set_mountpoint */

int fsc_json_set_logfile (fsc_t *fsc,
                          struct _fsc_json_handle_t *handle,
                          json_t *object) {
    char *logfile_str;

    if(fsc->args.logfile_given) {
        /* Commandline options have higher priority than configuration files */
        return 0;
    }

    logfile_str = strdup(json_string_value(object));
    if(!logfile_str) {
        fprintf(stderr, "ERROR: Out of memory\n");
        return ENOMEM;
    }

    if(fsc->logfile_path) {
        free(fsc->logfile_path);
    }
    fsc->logfile_path = logfile_str;

    return 0;
} /* fsc_json_set_logfile */

int fsc_json_set_scan_root (fsc_t *fsc,
                            struct _fsc_json_handle_t *handle,
                            json_t *object) {
    char *scan_root_str;

    if(fsc->args.scan_root_given) {
        /* Commandline options have higher priority than configuration files */
        return 0;
    }

    scan_root_str = strdup(json_string_value(object));
    if(!scan_root_str) {
        fprintf(stderr, "ERROR: Out of memory\n");
        return ENOMEM;
    }

    if(fsc->scan_root_path) {
        free(fsc->scan_root_path);
    }
    fsc->scan_root_path = scan_root_str;

    return 0;
} /* fsc_json_set_scan_root */

int fsc_json_set_open_limit (fsc_t *fsc,
                             struct _fsc_json_handle_t *handle,
                             json_t *object) {
    int open_limit;

    if(fsc->args.open_limit_given) {
        /* Commandline options have higher priority than configuration files */
        return 0;
    }

    open_limit = json_integer_value(object);
    fsc->fh_limit = open_limit + 1; /* 0 is used as 'not set' */

    return 0;
} /* fsc_json_set_open_limit */

int fsc_json_set_include (fsc_t *fsc,
                          struct _fsc_json_handle_t *handle,
                          json_t *object) {
    int ret;
    size_t i, len;
    const char *include_str;
    json_t *include;

    len = json_array_size(object);
    for(i=0; i<len; i++) {
        include = json_array_get(object, i);
        if(!json_is_string(include)) {
            fprintf(stderr, "ERROR: Non-string value in '%s' array\n",
                    handle->key);
            return EINVAL;
        } else {
            include_str = json_string_value(include);
            switch(include_str[0]) {
                case '+':
                    if((ret = fsc_include_path(fsc, include_str + 1))) {
                        fprintf(stderr, "ERROR: Cannot include '%s' path\n",
                                include_str + 1);
                        return ret;
                    }
                    break;
                case '-':
                    if((ret = fsc_exclude_path(fsc, include_str + 1))) {
                        fprintf(stderr, "ERROR: Cannot exclude '%s' path\n",
                                include_str + 1);
                        return ret;
                    }
                    break;
                default:
                    fprintf(stderr, "ERROR: Invalid inclusion pattern: %s\n",
                            include_str);
                    return EINVAL;
            }
        }
    }

    return 0;
} /* fsc_json_set_include */

int fsc_json_set_debug (fsc_t *fsc,
                        struct _fsc_json_handle_t *handle,
                        json_t *object) {
    int debug;

    if(fsc->args.debug_given || fsc->args.verbose_given) {
        /* Commandline options have higher priority than configuration files */
        return 0;
    }

    debug = json_boolean_value(object);
    if(!fsc->args.verbose_flag) {
        fsc->args.debug_flag = debug;
    }

    return 0;
} /* fsc_json_set_debug */

int fsc_json_set_verbose (fsc_t *fsc,
                          struct _fsc_json_handle_t *handle,
                          json_t *object) {
    int verbose;

    if(fsc->args.verbose_given) {
        /* Commandline options have higher priority than configuration files */
        return 0;
    }

    verbose = json_boolean_value(object);
    fsc->args.verbose_flag = verbose;
    if(verbose) {
        fsc->args.debug_flag = 1;
    }

    return 0;
} /* fsc_json_set_verbose */

int fsc_json_set_fuse_st (fsc_t *fsc,
                          struct _fsc_json_handle_t *handle,
                          json_t *object) {
    int single_threaded;

    if(fsc->args.fuse_single_threaded_given) {
        /* Commandline options have higher priority than configuration files */
        return 0;
    }

    single_threaded = json_boolean_value(object);
    fsc->args.fuse_single_threaded_flag = single_threaded;

    return 0;
} /* fsc_json_set_fuse_st */

int fsc_json_set_fuse_debug (fsc_t *fsc,
                             struct _fsc_json_handle_t *handle,
                             json_t *object) {
    int debug;

    if(fsc->args.fuse_debug_given) {
        /* Commandline options have higher priority than configuration files */
        return 0;
    }

    debug = json_boolean_value(object);
    fsc->args.fuse_debug_flag = debug;

    return 0;
} /* fsc_json_set_fuse_debug */

#define JSON_CMD_GET_VAL(var, json_key, type)                               \
            obj = json_object_get(object, json_key);                        \
            if(obj) {                                                       \
                if(!json_is_##type(obj)) {                                  \
                    fprintf(stderr, "ERROR: '%s/' " json_key                \
                            "key has wrong value type\n", handle->key);     \
                    return ret;                                             \
                } else {                                                    \
                    var = json_##type##_value(obj);                         \
                }                                                           \
            }

#define CMD_SET_FIELD(var, json_key, type)                                  \
            JSON_CMD_GET_VAL(var, json_key, type);                          \
            if(var) {                                                       \
                command->var = var;                                         \
            }

#define CMD_SET_FIELD_STR(var, json_key)                                    \
            JSON_CMD_GET_VAL(var, json_key, string);                        \
            if(var) {                                                       \
                command->var = strdup(var);                                 \
                if(!command->var) {                                         \
                    fprintf(stderr, "ERROR: Out of memory\n");              \
                    goto fsc_json_set_command_err;                          \
                }                                                           \
            }

static int fsc_json_set_command (fsc_t *fsc,
                                 struct _fsc_json_handle_t *handle,
                                 json_t *object) {
    int ret = -1, sudo, global, logic_or;
    size_t i, len;
    const char *exe = NULL, **args = NULL, *logic = NULL, *in_dir = NULL,
               *has_file = NULL, *has_dir = NULL, *os_id = NULL,
               *os_id_like = NULL;
    json_t *obj, *entry;
    fsc_command_t *command = NULL;

    if(fsc->cmd_num == fsc->cmd_max &&
       fsc_resize(&fsc->commands, &fsc->cmd_max, sizeof(fsc_command_t*))) {
        fprintf(stderr, "ERROR: Out of memory\n");
        return ret;
    }

    JSON_CMD_GET_VAL(exe, "exe", string);
    if(!exe) {
        fprintf(stderr, "ERROR: No 'exe' key in command configuration\n");
        return ret;
    }

    obj = json_object_get(object, "args");
    if(!obj) {
        args = (const char**)malloc(2 * sizeof(const char*));
        if(!args) {
            fprintf(stderr, "ERROR: Out of memory\n");
            return ret;
        }
        args[0] = exe;
        args[1] = NULL;
    } else {
        if(!json_is_array(obj)) {
            fprintf(stderr, "ERROR: '%s/args' is not an array type\n",
                    handle->key);
            return ret;
        }

        len = json_array_size(obj);
        args = (const char**)calloc(len + 2, sizeof(const char*));
        if(!args) {
            fprintf(stderr, "ERROR: Out of memory\n");
            return ret;
        }

        args[0] = exe;
        for(i=0; i<len; i++) {
            entry = json_array_get(obj, i);
            if(!json_is_string(entry)) {
                fprintf(stderr, "ERROR: '%s/args' array has non string value\n",
                        handle->key);
                goto fsc_json_set_command_err;
            }
            args[i + 1] = json_string_value(entry);
        }
        args[len + 1] = NULL;
    }

    command = fsc_command_new(fsc, exe, args);
    if(!command) {
        fprintf(stderr, "ERROR: Cannot create new command\n");
        goto fsc_json_set_command_err;
    }

    CMD_SET_FIELD(sudo, "sudo", boolean);
    CMD_SET_FIELD(global, "global", boolean);

    CMD_SET_FIELD_STR(in_dir, "in dir");
    CMD_SET_FIELD_STR(has_file, "has file");
    CMD_SET_FIELD_STR(has_dir, "has dir");
    CMD_SET_FIELD_STR(os_id, "os id");
    CMD_SET_FIELD_STR(os_id_like, "os id like");

    JSON_CMD_GET_VAL(logic, "logic", string);
    if(logic) {
        if(!strcasecmp(logic, "or")) {
            logic_or = 1;
        } else if(!strcasecmp(logic, "and")) {
            logic_or = 0;
        } else {
            fprintf(stderr, "ERROR: Invalid logic for '%s' command\n",
                    exe);
            goto fsc_json_set_command_err;
        }
    } else {
        /* By default require all conditions to be verified */
        logic_or = 0;
    }
    command->logic_or = logic_or;

    fsc->commands[fsc->cmd_num] = command;
    fsc->cmd_num++;
    command = NULL;

    ret = 0;
fsc_json_set_command_err:
    free(args);
    fsc_command_free(command);
    return ret;
} /* fsc_json_set_command */

int fsc_json_set_commands_array (fsc_t *fsc,
                                 struct _fsc_json_handle_t *handle,
                                 json_t *object) {
    size_t i, len;
    json_t *entry;

    len = json_array_size(object);
    for(i=0; i<len; i++) {
        entry = json_array_get(object, i);
        if(!json_is_object(entry)) {
            fprintf(stderr, "ERROR: Command configuration is not a JSON "
                    "object\n");
            return -1;
        }
        if(fsc_json_set_command(fsc, handle, entry)) {
            fprintf(stderr, "ERROR: Cannot register new command\n");
            return -1;
        }
    }

    return 0;
} /* fsc_json_set_commands_array */

fsc_json_handle_t json_tokens[] = {
    /* There is no such type as JSON_BOOLEAN so JSON_TRUE or JSON_FALSE
     * has to be used and it requires special handling */
    {"mountpoint",      JSON_STRING,    fsc_json_set_mountpoint},
    {"logfile",         JSON_STRING,    fsc_json_set_logfile},
    {"scan root",       JSON_STRING,    fsc_json_set_scan_root},
    {"open limit",      JSON_INTEGER,   fsc_json_set_open_limit},
    {"include",         JSON_ARRAY,     fsc_json_set_include},
    /* 'verbose' must be checked before 'debug' because if 'verbose' is set,
     * 'debug' also is */
    {"verbose",         JSON_TRUE,      fsc_json_set_verbose},
    {"debug",           JSON_TRUE,      fsc_json_set_debug},
    {"fuse single threaded", JSON_TRUE, fsc_json_set_fuse_st},
    {"fuse debug",      JSON_TRUE,      fsc_json_set_fuse_debug},
    {"commands",        JSON_ARRAY,     fsc_json_set_commands_array}
};

size_t json_tokens_size = sizeof_array(json_tokens);

