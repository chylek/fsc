#include <fsc.h>
#include <misc.h>
#include <filetree.h>
#include <operations.h>
#include <syscalls.h>

#include <string.h>
#include <unistd.h>
#include <limits.h>
#include <errno.h>

struct fuse_operations fsc_operations = {
    .getattr = fsc_getattr,
    .readlink = fsc_readlink,
    .getdir = NULL, /* Deprecated, use readdir() */
    .mknod = fsc_mknod,
    .mkdir = fsc_mkdir,
    .unlink = fsc_unlink,
    .rmdir = fsc_rmdir,
    .symlink = fsc_symlink,
    .rename = fsc_rename,
    .link = fsc_link,
    .chmod = fsc_chmod,
    .chown = fsc_chown,
    .truncate = fsc_truncate,
    .utime = NULL, /* Deprecated, use utimens() */
    .open = fsc_open,
    .read = fsc_read,
    .write = fsc_write,
    .statfs = fsc_statfs,
    .flush = fsc_flush,
    .release = fsc_release,
    .fsync = fsc_fsync,
    .opendir = fsc_opendir,
    .readdir = fsc_readdir,
    .releasedir = fsc_releasedir,
    .fsyncdir = NULL,
    .init = fsc_fuse_init,
    .destroy = fsc_fuse_destroy,
    .access = fsc_access,
    .create = fsc_create,
    .ftruncate = NULL,
    .fgetattr = fsc_fgetattr,
    .lock = NULL,
    .utimens = fsc_utimens,
#ifndef __APPLE__
#if FUSE_VERSION >= 29
    .flag_nopath = 0,
#endif
#if FUSE_VERSION == 28 || FUSE_VERSION == 29
    .flag_nullpath_ok = 1,
#endif
#endif
};

int fsc_getattr (const char *path, struct stat *st) {
    ssize_t index;
    char *name;
    fsc_t *fsc = FSC_DATA;
    fsc_dir_t *dir;

    if(!path) {
        FSC_ERROR("NULL path");
        return -EINVAL;
    }
    if(*path == '\0') {
        FSC_ERROR("Empty path");
        return -ENOENT;
    }
    if(!st) {
        FSC_ERROR("NULL stat structure");
        return -EINVAL;
    }
    FSC_DEBUG("%s", path);

    /* Corner case - root directory */
    if(!strcmp(path, "/")) {
        *st = fsc->root->st;
        return 0;
    }

    name = strrchr(path, '/');
    if(!name) {
        FSC_ERROR("'/' not found in '%s'", path);
        return -EINVAL;
    }
    name++;

    if(fsc_ftw(fsc, path, &dir)) {
        FSC_ERROR("Cannot load filetree");
        return -fsc_get_err(fsc);
    }
    index = fsc_find_entry(dir->files, dir->nfiles, name, fsc_file_cmp);
    if(index >= 0) {
        *st = dir->files[index]->st;
    } else {
        index = fsc_find_entry(dir->dirs, dir->ndirs, name, fsc_dir_cmp);
        if(index >= 0) {
            *st = dir->dirs[index]->st;
        } else {
            FSC_ERROR("No such file or directory");
            return -ENOENT;
        }
    }

    return 0;
} /* fsc_getattr */

int fsc_readlink (const char *path, char *buf, size_t bufsize) {
    ssize_t index;
    char *name;
    fsc_t *fsc = FSC_DATA;
    fsc_file_t *file;
    fsc_dir_t *dir, *subdir;

    if(!path) {
        FSC_ERROR("NULL path");
        return -EINVAL;
    }
    if(*path == '\0') {
        FSC_ERROR("Empty path");
        return -ENOENT;
    }
    if(!buf) {
        FSC_ERROR("NULL buffer");
        return -EINVAL;
     }
    if(!bufsize) {
        FSC_ERROR("Buffer size of zero");
        return -EINVAL;
    }
    FSC_DEBUG("%s", path);

    name = strrchr(path, '/');
    if(!name) {
        FSC_ERROR("'/' not found in '%s'", path);
        return -EINVAL;
    }
    name++;

    if(fsc_ftw(fsc, path, &dir)) {
        FSC_ERROR("Cannot load filetree");
        return -fsc_get_err(fsc);
    }
    index = fsc_find_entry(dir->files, dir->nfiles, name, fsc_file_cmp);
    if(index >= 0) {
        file = dir->files[index];
        if(S_ISLNK(file->st.st_mode)) {
            if(file->link_path) {
                strncpy(buf, file->link_path, bufsize);
            } else {
                FSC_ERROR("[Internal] Dangling file link: %s", path);
                FSC_INTERNAL_ERR();
            }
        } else {
            FSC_ERROR("Not a symbolic link");
            return -EINVAL;
        }
    } else {
        index = fsc_find_entry(dir->dirs, dir->ndirs, name, fsc_dir_cmp);
        if(index >= 0) {
            subdir = dir->dirs[index];
            if(S_ISLNK(subdir->st.st_mode)) {
                if(subdir->link_path) {
                    strncpy(buf, subdir->link_path, bufsize);
                } else {
                    FSC_ERROR("[Internal] Dangling directory link: %s", path);
                    FSC_INTERNAL_ERR();
                }
            } else {
                FSC_ERROR("Not a symbolic link");
                return -EINVAL;
            }
        } else {
            FSC_ERROR("No such file or directory");
            return -ENOENT;
        }
    }

    return 0;
} /* fsc_readlink */

int fsc_mknod (const char *path, mode_t mode, dev_t dev) {
    return -ENOTSUP;
} /* fsc_mknod */

int fsc_mkdir (const char *path, mode_t mode) {
    char *name;
    struct stat st = FSC_DIR_ST;
    fsc_t *fsc = FSC_DATA;
    fsc_dir_t *dir, *new_dir = NULL;

    if(!path) {
        FSC_ERROR("NULL path");
        return -EINVAL;
    }
    if(*path == '\0') {
        FSC_ERROR("Empty path");
        return -ENOENT;
    }
    FSC_DEBUG("%s", path);

    if(!S_ISDIR(mode)) {
        /* Do not report an error. While creating new directory it is highly
         * expected to have S_IFDIR included in mode so just fix it.
         */
        mode |= S_IFDIR;
    }
    st.st_mode = mode;

    name = strrchr(path, '/');
    if(!name) {
        FSC_ERROR("'/' not found in '%s'", path);
        return -EINVAL;
    }
    name++;

    if(fsc_ftw(fsc, path, &dir)) {
        FSC_ERROR("Cannot load filetree");
        return -fsc_get_err(fsc);
    }

    if(fsc_find_entry(dir->files, dir->nfiles, name, fsc_file_cmp) >= 0) {
        FSC_ERROR("'%s' is a file", path);
        return -EISDIR;
    }
    if(fsc_find_entry(dir->dirs, dir->ndirs, name, fsc_dir_cmp) >= 0) {
        FSC_ERROR("'%s' directory already exists", path);
        return -EEXIST;
    }

    new_dir = fsc_dir_new(fsc, name, &st);
    if(!new_dir) {
        FSC_ERROR("Cannot create new directory");
        return -fsc_get_err(fsc);
    }

    if(fsc_operation_mkdir(fsc, path, mode)) {
        FSC_ERROR("Cannot add mkdir() operation");
        fsc_dir_free(new_dir);
        return -fsc_get_err(fsc);
    }

    if(fsc_dir_add(dir, new_dir)) {
        FSC_ERROR("Cannot add new directory to the filetreen");
        fsc_dir_free(new_dir);
        return -fsc_get_err(fsc);
    }

    return 0;
} /* fsc_mkdir */

int fsc_unlink (const char *path) {
    ssize_t index;
    char *name;
    fsc_t *fsc = FSC_DATA;
    fsc_file_t *file;
    fsc_dir_t *dir;

    if(!path) {
        FSC_ERROR("NULL path");
        return -EINVAL;
    }
    if(*path == '\0') {
        FSC_ERROR("Empty path");
        return -ENOENT;
    }
    FSC_DEBUG("%s", path);

    name = strrchr(path, '/');
    if(!name) {
        FSC_ERROR("'/' not found in '%s'", path);
        return -EINVAL;
    }
    name++;

    if(fsc_ftw(fsc, path, &dir)) {
        FSC_ERROR("Cannot load filetree");
        return -fsc_get_err(fsc);
    }

    if(fsc_find_entry(dir->dirs, dir->ndirs, name, fsc_dir_cmp) >= 0) {
        FSC_ERROR("'%s' is a directory", path);
        return -EISDIR;
    }

    index = fsc_find_entry(dir->files, dir->nfiles, name, fsc_file_cmp);
    if(index < 0) {
        FSC_ERROR("No such file");
        return -ENOENT;
    }

    file = dir->files[index];

    if(!S_ISLNK(file->st.st_mode)) {
        if(!file->orig_path) {
            FSC_ERROR("[Internal] File without original path");
            FSC_INTERNAL_ERR();
            return -ENOMSG;
        }

        if(fsc_operation_unlink(fsc, file->orig_path)) {
            FSC_ERROR("Cannot add unlink operation");
            return -fsc_get_err(fsc);
        }
    }

    fsc_file_del(dir, index);
    fsc_file_free(file);

    return 0;
} /* fsc_unlink */

int fsc_rmdir (const char *path) {
    ssize_t index;
    char *name, *dir_path = NULL;
    fsc_t *fsc = FSC_DATA;
    fsc_dir_t *dir, *subdir;

    if(!path) {
        FSC_ERROR("NULL path");
        return -EINVAL;
    }
    if(*path == '\0') {
        FSC_ERROR("Empty path");
        return -ENOENT;
    }
    FSC_DEBUG("%s", path);

    name = strrchr(path, '/');
    if(!name) {
        FSC_ERROR("'/' not found in '%s'", path);
        return -EINVAL;
    }
    name++;

    if(fsc_ftw(fsc, path, &dir)) {
        FSC_ERROR("Cannot load filetree");
        return -fsc_get_err(fsc);
    }

    if(fsc_find_entry(dir->files, dir->nfiles, name, fsc_file_cmp) >= 0) {
        FSC_ERROR("'%s' is a file", path);
        return -EISDIR;
    }
    index = fsc_find_entry(dir->dirs, dir->ndirs, name, fsc_dir_cmp);
    if(index < 0) {
        FSC_ERROR("No such directory");
        return -ENOENT;
    }

    subdir = dir->dirs[index];

    if(subdir->nfiles || subdir->ndirs) {
        FSC_ERROR("'%s' directory is not empty", path);
        return -ENOTEMPTY;
    }

    /* This directory can be a newly created virtual one so there will be no
     * 'orig_path' set
     */
    if(subdir->orig_path) {
        if(fsc_operation_rmdir(fsc, subdir->orig_path)) {
            FSC_ERROR("Cannot add rmdir operation");
            return -fsc_get_err(fsc);
        }
    } else {
        dir_path = (char*)malloc(strlen(fsc->scan_root_path) + 1 +
                                 strlen(path) + 1 -
                                 (lenof(FSC_MAIN_FILETREE_NAME) + 2));
                                 /* Last one is for '/filetree/' */
        if(!dir_path) {
            FSC_ERR_SET(ENOMEM, "Out of memory");
            return -1;
        }
        sprintf(dir_path, "%s/%s", fsc->scan_root_path,
                path + lenof(FSC_MAIN_FILETREE_NAME) + 2);

        if(fsc_operation_rmdir(fsc, dir_path)) {
            FSC_ERROR("Cannot add rmdir operation");
            free(dir_path);
            return -fsc_get_err(fsc);
        }

        free(dir_path);
    }

    fsc_dir_del(dir, index);
    fsc_dir_free(subdir);

    return 0;
} /* fsc_rmdir */

int fsc_symlink (const char *path, const char *newpath) {
    return -ENOTSUP;
} /* fsc_symlink */

int fsc_rename (const char *path, const char *newpath) {
    ssize_t index;
    char *name_from, *name_to;
    fsc_t *fsc = FSC_DATA;
    fsc_file_t *file = NULL;
    fsc_dir_t *dir_from, *dir_to, *dir = NULL;

    if(!path || !newpath) {
        FSC_ERROR("NULL path");
        return -EINVAL;
    }
    if(*path == '\0' || *newpath == '\0') {
        FSC_ERROR("Empty path");
        return -ENOENT;
    }
    FSC_DEBUG("%s -> %s", path, newpath);

    name_from = strrchr(path, '/');
    if(!name_from) {
        FSC_ERROR("'/' not found in '%s'", path);
        return -EINVAL;
    }
    name_from++;

    name_to = strrchr(newpath, '/');
    if(!name_to) {
        FSC_ERROR("'/' not found in '%s'", newpath);
        return -EINVAL;
    }
    name_to++;

    if(fsc_ftw(fsc, path, &dir_from)) {
        FSC_ERROR("Cannot load filetree");
        return -fsc_get_err(fsc);
    }
    if(fsc_ftw(fsc, newpath, &dir_to)) {
        FSC_ERROR("Cannot load filetree");
        return -fsc_get_err(fsc);
    }

    index = fsc_find_entry(dir_from->files, dir_from->nfiles, name_from,
                           fsc_file_cmp);
    if(index >= 0) {
        file = dir_from->files[index];
    } else {
        index = fsc_find_entry(dir_from->dirs, dir_from->ndirs, name_from,
                               fsc_dir_cmp);
        if(index >= 0) {
            dir = dir_from->dirs[index];
        } else {
            FSC_ERROR("No such file or directory: %s", path);
            return -ENOENT;
        }
    }

    index = fsc_find_entry(dir_to->files, dir_to->nfiles, name_to,
                           fsc_file_cmp);
    if(index >= 0) {
        FSC_ERROR("'%s' file already exists", newpath);
        return -EEXIST;
    } else {
        index = fsc_find_entry(dir_to->dirs, dir_to->ndirs, name_to,
                               fsc_dir_cmp);
        if(index >= 0) {
            FSC_ERROR("'%s' directory already exists", newpath);
            return -EEXIST;
        }
    }

    /* We don't have 'orig_path' for destination - use given options */
    if(fsc_operation_rename(fsc, path, newpath)) {
        FSC_ERROR("Cannot add unlink operation");
        return -fsc_get_err(fsc);
    }

    if(file) {
        if(strcmp(file->name, name_to) && fsc_file_rename(file, name_to)) {
            FSC_ERROR("Cannot rename '%s' file", path);
            return -fsc_get_err(fsc);
        }
    } else {
        if(strcmp(dir->name, name_to) && fsc_dir_rename(dir, name_to)) {
            FSC_ERROR("Cannot rename '%s' directory", path);
            return -fsc_get_err(fsc);
        }
    }

    if(dir_from != dir_to) {
        if(file) {
            if(fsc_file_mv(file, dir_to)) {
                FSC_ERROR("Cannot move '%s' file", path);
                return -fsc_get_err(fsc);
            }
        } else {
            if(fsc_dir_mv(dir, dir_to)) {
                FSC_ERROR("Cannot move '%s' directory", path);
                return -fsc_get_err(fsc);
            }
        }
    }

    return 0;
} /* fsc_rename */

int fsc_link (const char *path, const char *newpath) {
    return -ENOTSUP;
} /* fsc_link */

int fsc_chmod (const char *path, mode_t mode) {
    return -ENOTSUP;
} /* fsc_chmod */

int fsc_chown (const char *path, uid_t uid, gid_t gid) {
    return -ENOTSUP;
} /* fsc_chown */

int fsc_truncate (const char *path, off_t length) {
    fsc_t *fsc = FSC_DATA;

    if(!strcmp(path, FSC_ABORT_FILE_PATH)) {
        FSC_VERBOSE("%s", path);
        /* Fake completion of the request, the 'abort' file can never be
         * empty
         */
        return 0;
    }

    return -ENOTSUP;
} /* fsc_truncate */

int fsc_open (const char *path, struct fuse_file_info *file_info) {
    int ret = -ENOMSG, file_locked = 1;
    size_t i;
    ssize_t index, fh = -1;
    char *filename;
    fsc_t *fsc = FSC_DATA;
    fsc_file_t *file;
    fsc_dir_t *dir;

    if(!path) {
        FSC_ERROR("NULL path");
        return -EINVAL;
    }
    if(*path == '\0') {
        FSC_ERROR("Empty path");
        return -ENOENT;
    }
    if(*path != '/') {
        FSC_ERROR("Not an absolute path");
        return -EINVAL;
    }
    if(!file_info) {
        FSC_ERROR("NULL file information structure");
        return -EINVAL;
    }
    FSC_DEBUG("%s", path);

    if(!strcmp(path, FSC_ABORT_FILE_PATH)) {
        file_info->fh = FSC_ABORT_FILE_FD;
        return 0;
    }

    file_info->fh = 0; /* Mark as not used */
    filename = strrchr(path, '/') + 1;

    if(fsc_ftw(fsc, path, &dir)) {
        FSC_ERROR("Cannot load filetree");
        ret = -fsc_get_err(fsc);
        goto fsc_open_err;
    }
    index = fsc_find_entry(dir->files, dir->nfiles, filename, fsc_file_cmp);
    if(index >= 0) {
        file = dir->files[index];
    } else {
        index = fsc_find_entry(dir->dirs, dir->ndirs, filename, fsc_dir_cmp);
        if(index >= 0) {
            FSC_ERROR("'%s' is a directory", path);
            ret = -EISDIR;
            goto fsc_open_err;
        } else {
            FSC_ERROR("No such file or directory");
            ret = -ENOENT;
            goto fsc_open_err;
        }
    }

    /* Follow all the links as link does not have 'orig_path' set */
    file = fsc_file_follow_links(file);
    if(!file) {
        FSC_ERROR("Cannot follow link(s)");
        goto fsc_open_err;
    }

    FSC_OBJECT_LOCK(file);
    file_locked = 1;

    pthread_mutex_lock(&fsc->fh_mutex);
    for(i=1; i<fsc->fh_limit; i++) {
        if(!fsc->fh_files[i]) {
            fsc->fh_files[i] = file;
            fh = i;
        }
    }
    pthread_mutex_unlock(&fsc->fh_mutex);
    if(fh == -1) {
        FSC_ERROR("Too many opened files");
        ret = -ENFILE;
        goto fsc_open_err;
    }
    FSC_DEBUG("File handle found: %lu", (unsigned long int)fh);

    if(file->fd < 0) {
        file->fd = open(file->orig_path, file_info->flags);
        if(file->fd < 0) {
            ret = -errno;
            FSC_ERROR("Cannot open '%s' file: %s", file->orig_path,
                      strerror(errno));
            goto fsc_open_err;
        }
    }
    fsc->fh_files[fh] = file;
    file_info->fh = fh;
    fh = 0;
    file->open_counter++;

    ret = 0;
fsc_open_err:
    if(file_locked) {
        FSC_OBJECT_UNLOCK(file);
    }
    if(fh >= 0) {
        pthread_mutex_lock(&fsc->fh_mutex);
        fsc->fh_files[fh] = NULL;
        pthread_mutex_unlock(&fsc->fh_mutex);
    }
    return ret;
} /* fsc_open */

int fsc_read (const char *path, char *buf, size_t size, off_t offset,
              struct fuse_file_info *file_info) {
    int ret;
    ssize_t bytes;
    uint64_t fh;
    char file_path[PATH_MAX];
    fsc_t *fsc = FSC_DATA;
    fsc_file_t *file;

    /* Since we work with 'file_info->fh', 'path' can be NULL */
    if(!buf) {
        FSC_ERROR("NULL buffer");
        return -EINVAL;
    }
    /* 'size' equal to 0 may be a correct value */
    if(!file_info) {
        FSC_ERROR("NULL file information structure");
        return -EINVAL;
    }
    if(path) {
        FSC_DEBUG("%s", path);
    } else {
        FSC_DEBUG("File descriptor: %lu", (unsigned long int)file_info->fh);
    }

    if(size == 0) {
        return 0;
    }

    if(file_info->fh == FSC_ABORT_FILE_FD) {
        if(offset) {
            return 0;
        }

        buf[0] = fsc->abort ? '1' : '0';
        return 1;
    }

    fh = file_info->fh;
    if(!fh) {
        FSC_ERROR("File '%s' not opened (fh: %lu)",
                  path ? path : "(null)", (unsigned long int)fh);
        return -EBADF;
    }
    if(fh >= fsc->fh_limit) {
        FSC_ERROR("File handle out of scope (%lu >= %lu)",
                  (unsigned long int)fh, (unsigned long int)fsc->fh_limit);
        return -EINVAL;
    }

    file = fsc->fh_files[fh];
    if(!file) {
        FSC_ERROR("No file under given handle: %lu", (unsigned long int)fh);
        return -EINVAL;
    }

    bytes = pread(file->fd, buf, size, offset);
    if(bytes) {
        if(bytes < 0) {
            ret = -errno;
            FSC_ERROR("Cannot read '%s' (fd: %d) file: %s",
                      path ? path : fsc_file_get_path(file, file_path,
                                                      sizeof(file_path)),
                      file->fd, strerror(errno));
            return ret;
        }
        return (int)bytes;
    }

    return 0;
} /* fsc_read */

int fsc_write (const char *path, const char *buf, size_t size, off_t offset,
               struct fuse_file_info *file_info) {
    fsc_t *fsc = FSC_DATA;

    if(file_info->fh == FSC_ABORT_FILE_FD) {
        if(size > 2) {
            FSC_ERROR("'%s' file only accepts 0 or 1 and whitechar ad the end",
                      FSC_ABORT_FILE_PATH);
            return -EINVAL;
        }

        if(size == 2 && buf[1] > ' ') { /* ' ' is the last whitechar in ASCII */
            FSC_ERROR("'%s' file only accpets whitechars as second byte (%d)",
                      FSC_ABORT_FILE_PATH, buf[1]);
            return -EINVAL;
        }

        if(buf[0] == '0') {
            FSC_DEBUG("Enabling execution of cached operations");
            fsc->abort = 0;
        } else if(buf[0] == '1') {
            FSC_DEBUG("Disabling execution of cached operations");
            fsc->abort = 1;
        } else {
            FSC_ERROR("'%s' file only accepts 0 or 1", FSC_ABORT_FILE_PATH);
            return EINVAL;
        }

        return size;
    }

    return -ENOTSUP;
} /* fsc_write */

int fsc_statfs (const char *path, struct statvfs *st) {
    return -ENOTSUP;
} /* fsc_statfs */

int fsc_flush (const char *path, struct fuse_file_info *file_info) {
    /* This is called on every close() call. As it is not this function purpose
     * to do any file clean up and we have no data to synchronize, this is just
     * NO-OP */
    /* Since we do nothing here - no argument checking is required */
    return 0;
} /* fsc_flush */

int fsc_release (const char *path, struct fuse_file_info *file_info) {
    uint64_t fh;
    char file_path[PATH_MAX];
    fsc_t *fsc = FSC_DATA;
    fsc_file_t *file;

    /* Since we work with 'file_info->fh', 'path' can be NULL */
    if(!file_info) {
        FSC_ERROR("NULL file information structure");
        return -EINVAL;
    }
    if(path) {
        FSC_DEBUG("%s", path);
    } else {
        FSC_DEBUG("File descriptor: %lu", (unsigned long int)file_info->fh);
    }

    /* path does not have to be set, 'fh' is always set */
    if(file_info->fh == FSC_ABORT_FILE_FD) {
        return 0;
    }

    fh = file_info->fh;
    if(!fh) {
        FSC_ERROR("File '%s' not opened (fh: %lu)",
                  path ? path : "(null)", (unsigned long int)fh);
        return -EBADF;
    }
    if(fh >= fsc->fh_limit) {
        FSC_ERROR("File handle out of scope (%lu >= %lu)",
                  (unsigned long int)fh, (unsigned long int)fsc->fh_limit);
        return -EINVAL;
    }

    file = fsc->fh_files[fh];
    if(!file) {
        FSC_ERROR("No file under given handle: %lu", (unsigned long int)fh);
        return -EINVAL;
    }

    pthread_mutex_lock(&fsc->fh_mutex);
    fsc->fh_files[fh] = NULL;
    pthread_mutex_unlock(&fsc->fh_mutex);

    FSC_OBJECT_LOCK(file);
    file->open_counter--;
    if(!file->open_counter) {
        if(file->fd < 0) {
            FSC_ERROR("[Internal] Invalid file descriptor in file structure:"
                      " %d", file->fd);
            FSC_INTERNAL_ERR();
        } else if(close(file->fd)) {
            FSC_ERROR("Cannot close '%s' (fd: %d) file: %s",
                      fsc_file_get_path(file, file_path, sizeof(file_path)),
                      file->fd, strerror(errno));
        }
        file->fd = -1;
    }
    FSC_OBJECT_UNLOCK(file);

    file_info->fh = 0; /* Mark as not used */

    return 0;
} /* fsc_release */

int fsc_fsync (const char *path, int datasync,
               struct fuse_file_info *file_info) {
    return -ENOTSUP;
} /* fsc_fsync */

int fsc_opendir (const char *path, struct fuse_file_info *file_info) {
    size_t i;
    char path2[PATH_MAX];
    fsc_dir_t *dir;
    fsc_t *fsc = FSC_DATA;

    if(!path) {
        FSC_ERROR("NULL path");
        return -EINVAL;
    }
    if(*path == '\0') {
        FSC_ERROR("Empty path");
        return -ENOENT;
    }
    if(*path != '/') {
        FSC_ERROR("Not an absolute path");
        return -EINVAL;
    }
    if(!file_info) {
        FSC_ERROR("NULL file information structure");
        return -EINVAL;
    }
    FSC_DEBUG("%s", path);

    file_info->fh = 0; /* Mark as not used */

    /* Slash at the end is required for fsc_ftw() to get into searched
     * directory - this way we do not have to use fsc_find_entry() */
    snprintf(path2, sizeof(path2), "%s%s", path, strcmp(path, "/") ? "/" : "");

    if(fsc_ftw(fsc, path2, &dir)) {
        FSC_ERROR("Cannot load filetree");
        return -fsc_get_err(fsc);
    }

    pthread_mutex_lock(&fsc->fh_mutex);
    for(i=1; i<fsc->fh_limit; i++) {
        if(!fsc->fh_dirs[i]) {
            fsc->fh_dirs[i] = dir;
            file_info->fh = i;
            break;
        }
    }
    pthread_mutex_unlock(&fsc->fh_mutex);
    if(!file_info->fh) {
        FSC_ERROR("Too many opened directories");
        return -ENFILE;
    }
    FSC_DEBUG("Directory handle found: %lu", (unsigned long int)file_info->fh);

    return 0;
} /* fsc_opendir */

int fsc_readdir (const char *path, void *buf, fuse_fill_dir_t filler,
                 off_t offset, struct fuse_file_info *file_info) {
    size_t i;
    uint64_t fh;
    fsc_dir_t *dir;
    fsc_t *fsc = FSC_DATA;

    /* Since we work with 'file_info->fh', 'path' can be NULL */
    if(path && *path == '\0') {
        FSC_ERROR("Empty path");
        return -ENOENT;
    }
    if(path && *path != '/') {
        FSC_ERROR("Not an absolute path");
        return -EINVAL;
    }
    if(!buf) {
        FSC_ERROR("NULL buffer");
        return -EINVAL;
    }
    if(!filler) {
        FSC_ERROR("NULL filler callback");
        return -EINVAL;
    }
    if(!file_info) {
        FSC_ERROR("NULL file information structure");
        return -EINVAL;
    }
    FSC_DEBUG("'%s', offset: %lld", path, (long long int)offset);

    fh = file_info->fh;
    if(!fh) {
        FSC_ERROR("Directory '%s' not opened (fh: %lu)",
                  path ? path : "(null)", (unsigned long int)fh);
        return -EBADF;
    }
    if(fh >= fsc->fh_limit) {
        FSC_ERROR("Directory handle out of scope (%lu >= %lu)",
                  (unsigned long int)fh, (unsigned long int)fsc->fh_limit);
        return -EBADF;
    }
    dir = fsc->fh_dirs[fh];
    if(!dir) {
        FSC_ERROR("No directory under given handle: %lu",
                  (unsigned long int)fh);
        return -EFAULT;
    }

    if(filler(buf, ".", NULL, 0)) {
        FSC_ERROR("Filler failed on current directory");
        return -ENOBUFS;
    }
    if(filler(buf, "..", NULL, 0)) {
        FSC_ERROR("Filler failed on parent directory");
        return -ENOBUFS;
    }

    for(i=0; i<dir->ndirs; i++) {
        if(filler(buf, dir->dirs[i]->name, NULL, 0)) {
            FSC_ERROR("Filler failed on '%s': buffer is full",
                      dir->dirs[i]->name);
            return -ENOBUFS;
        }
    }
    for(i=0; i<dir->nfiles; i++) {
        if(filler(buf, dir->files[i]->name, NULL, 0)) {
            FSC_ERROR("Filler failed on '%s': buffer is full",
                      dir->files[i]->name);
            return -ENOBUFS;
        }
    }

    return 0;
} /* fsc_readdir */

int fsc_releasedir (const char *path, struct fuse_file_info *file_info) {
    size_t fh;
    fsc_t *fsc = FSC_DATA;

    /* Since we work with 'file_info->fh', 'path' can be NULL */
    if(path && *path == '\0') {
        FSC_ERROR("Empty path");
        return -ENOENT;
    }
    if(path && *path != '/') {
        FSC_ERROR("Not an absolute path");
        return -EINVAL;
    }
    if(!file_info) {
        FSC_ERROR("NULL file information structure");
        return -EINVAL;
    }
    FSC_DEBUG("%s", path);

    fh = file_info->fh;
    if(!fh) {
        FSC_ERROR("Directory '%s' not opened (fh: %lu)",
                  path ? path : "(null)", (unsigned long int)fh);
        return -EBADF;
    }
    if(fh >= fsc->fh_limit) {
        FSC_ERROR("Directory handle out of scope (%lu >= %lu)",
                  (unsigned long int)fh, (unsigned long int)fsc->fh_limit);
        return -EBADF;
    }
    if(!fsc->fh_dirs[fh]) {
        FSC_ERROR("No directory under given handle: %lu",
                  (unsigned long int)fh);
        return -EFAULT;
    }

    pthread_mutex_lock(&fsc->fh_mutex);
    fsc->fh_dirs[fh] = NULL;
    pthread_mutex_unlock(&fsc->fh_mutex);

    return 0;
} /* fsc_releasedir */

void* fsc_fuse_init (struct fuse_conn_info *conn) {
    fsc_t *fsc = FSC_DATA;

#ifndef FSC_VALGRIND_FIX
    FSC_VERBOSE("Running local system commands");
    /* valgrind hangs inside this function on chdir() */
    if(fsc_command_run_locals(fsc)) {
        fprintf(stderr, "WARNING: Cannot run local commands. See logs for more "
                "details.\n");
        FSC_ERROR("Cannot run local commands");
        /* This is the only place local commands can be executed and it is void
         * function so no error can be returned. This can be changed if fsc run
         * fuse_main() itself and does not leave it to the user / main program
         */
    }
#endif /* FSC_VALGRIND_FIX */

    printf("FUSE started\n");
    FSC_LOG("FUSE started");

    return fsc;
} /* fsc_fuse_init */

void fsc_fuse_destroy (void *ptr) {
    fsc_t *fsc = FSC_DATA;

    printf("FUSE stopped\n");
    FSC_LOG("FUSE stopped");

    if(fsc_operations_run(fsc)) {
        FSC_ERROR("Cannot run operations");
    }
} /* fsc_fuse_destroy */

int fsc_access (const char *path, int mode) {
    return 0;
} /* fsc_ */

int fsc_create (const char *path, mode_t mode,
                struct fuse_file_info *file_info) {
    return -ENOTSUP;
} /* fsc_create */

int fsc_fgetattr (const char *path, struct stat *st,
                  struct fuse_file_info *file_info) {
    uint64_t fh;
    fsc_t *fsc = FSC_DATA;
    fsc_file_t *file;

    /* Since we work with 'file_info->fh', 'path' can be NULL */
    if(!st) {
        FSC_ERROR("NULL stat structure");
        return -EINVAL;
    }
    if(!file_info) {
        FSC_ERROR("NULL file information structure");
        return -EINVAL;
    }
    if(path) {
        FSC_DEBUG("%s", path);
    } else {
        FSC_DEBUG("File descriptor: %lu", (unsigned long int)file_info->fh);
    }

    if(file_info->fh == FSC_ABORT_FILE_FD) {
        ssize_t index = fsc_find_entry(fsc->root->files, fsc->root->nfiles,
                                       FSC_ABORT_FILE_PATH + 1, fsc_file_cmp);

        if(index < 0) {
            FSC_ERROR("[Internal] '%s' file not found", FSC_ABORT_FILE_PATH);
            FSC_INTERNAL_ERR();
            return -ENOMSG;
        }

        *st = fsc->root->files[index]->st;
        return 0;
    }

    fh = file_info->fh;
    if(!fh) {
        FSC_ERROR("File '%s' not opened (fh: %lu)",
                  path ? path : "(null)", (unsigned long int)fh);
        return -EBADF;
    }
    if(fh >= fsc->fh_limit) {
        FSC_ERROR("File handle out of scope (%lu >= %lu)",
                  (unsigned long int)fh, (unsigned long int)fsc->fh_limit);
        return -EINVAL;
    }

    file = fsc->fh_files[fh];
    if(!file) {
        FSC_ERROR("No file under given handle: %lu", (unsigned long int)fh);
        return -EINVAL;
    }

    *st = file->st;

    return 0;
} /* fsc_fgetattr */

int fsc_utimens (const char *path, const struct timespec tv[2]) {
    return -ENOTSUP;
} /* fsc_utimens */

