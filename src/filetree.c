#include <fsc.h>
#include <misc.h>
#include <filetree.h>
#include <filters.h>
#include <command.h>

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <dirent.h>
#include <sys/types.h>
#include <limits.h>
#include <unistd.h>
#include <fnmatch.h>

fsc_file_t* fsc_file_new (fsc_t *fsc, const char *path, const struct stat *st) {
    int err;
    ssize_t len;
    char link_path[PATH_MAX];
    const char *name;
    fsc_file_t *ret = NULL, *file = NULL;

    if(!fsc) {
        return ret;
    }
    if(!path) {
        FSC_ERR_SET(EINVAL, "NULL filename");
        return ret;
    }
    if(!st) {
        FSC_ERR_SET(EINVAL, "NULL file information");
        return ret;
    }

    file = (fsc_file_t*)calloc(1, sizeof(fsc_file_t));
    if(!file) {
        FSC_ERR_SET(ENOMEM, "Out of memory");
        return ret;
    }
    file->fsc = fsc;
    file->st = *st;
    file->fd = -1;

    if((err = pthread_mutex_init(&file->lock, NULL))) {
        FSC_ERR_SET(err, "Cannot initialize mutex: %s", strerror(err));
        free(file);
        return ret;
    }

    file->fdata_num = fsc->filters_num;
    file->fdata_max = fsc->filters_num;
    file->filters_data = calloc(file->fdata_max, sizeof(void*));
    if(!file->filters_data) {
        FSC_ERR_SET(ENOMEM, "Out of memory");
        goto fsc_file_new_err;
    }

    if(path[0] == '/') {
        file->orig_path = strdup(path);
        if(!file->orig_path) {
            FSC_ERR_SET(ENOMEM, "Out of memory");
            goto fsc_file_new_err;
        }
        name = strrchr(file->orig_path, '/'); /* There is at least one slash at
                                                 the very beginning */
        name++;

        if(S_ISLNK(file->st.st_mode)) {
            if((len = readlink(path, link_path, sizeof(link_path) - 1)) < 0) { /* One character for terminating NULL */
                FSC_ERR_SET(errno, "Cannot read '%s' link: %s", path,
                            strerror(errno));
                goto fsc_file_new_err;
            }
            link_path[len] = '\0';
            if(fsc_merge_paths(fsc, path, link_path)) {
                FSC_ERROR("Cannot merge paths");
                goto fsc_file_new_err;
            }
            file->link_path = strdup(link_path);
            if(!file->link_path) {
                FSC_ERR_SET(ENOMEM, "Out of memory");
                goto fsc_file_new_err;
            }
        }
    } else {
        name = path;
        /* In this case link should be set outside this function */
    }

    file->name = strdup(name);
    if(!file->name) {
        FSC_ERR_SET(ENOMEM, "Out of memory");
        goto fsc_file_new_err;
    }

    ret = file;
    file = NULL;
fsc_file_new_err:
    fsc_file_free(file);
    return ret;
} /* fsc_file_new */

void fsc_file_free (fsc_file_t *file) {
    int err;
    size_t i;
    fsc_t *fsc;
    fsc_filter_t *f;

    if(!file)
        return;
    fsc = file->fsc;

    free(file->orig_path);
    free(file->name);
    free(file->link_path);

    for(i=0; i<file->fdata_num; i++) {
        if(file->filters_data[i]) {
            if(i >= fsc->filters_num) {
                FSC_ERROR("[Internal] Filter data index out of scope");
                FSC_INTERNAL_ERR();
            } else {
                f = fsc->filters[i];
                if(!f->file_data_free) {
                    FSC_ERROR("Filter '%s' does not implement file data "
                              "destructor", fsc_filter_get_desc(f));
                } else {
                    f->file_data_free(fsc, f, file->filters_data[i]);
                }
            }
        }
    }
    free(file->filters_data);

    if((err = pthread_mutex_destroy(&file->lock))) {
        FSC_ERROR("Cannot destory mutex: %s", strerror(err));
    }

    free(file);
} /* fsc_file_free */

fsc_dir_t* fsc_dir_new (fsc_t *fsc, const char *path, const struct stat *st) {
    const char *name;
    fsc_dir_t *ret = NULL, *dir = NULL;

    if(!fsc) {
        return ret;
    }
    if(!path) {
        FSC_ERR_SET(EINVAL, "NULL filename");
        return ret;
    }

    dir = (fsc_dir_t*)calloc(1, sizeof(fsc_dir_t));
    if(!dir) {
        FSC_ERR_SET(ENOMEM, "Out of memory");
        return ret;
    }
    dir->fsc = fsc;
    dir->st = *st;

    if(path[0] == '/') {
        dir->orig_path = strdup(path);
        if(!dir->orig_path) {
            FSC_ERR_SET(ENOMEM, "Out of memory");
            goto fsc_dir_new_err;
        }
        name = strrchr(dir->orig_path, '/'); /* There is at least one slash at
                                                the very beginning */
        name++;
    } else {
        dir->orig_path = NULL;
        name = path;
    }

    dir->name = strdup(name);
    if(!dir->name) {
        FSC_ERR_SET(ENOMEM, "Out of memory");
        goto fsc_dir_new_err;
    }

    /* Directory which is a link should not store any files nor directories
     * The link should be followed in order to see files and directories */
    if(!S_ISLNK(st->st_mode)) {
        dir->maxfiles = FSC_ALLOC_ENTRIES;
        dir->files = (fsc_file_t**)calloc(dir->maxfiles, sizeof(fsc_file_t*));
        if(!dir->files) {
            FSC_ERR_SET(ENOMEM, "Out of memory");
            goto fsc_dir_new_err;
        }

        dir->maxdirs = FSC_ALLOC_ENTRIES;
        dir->dirs = (fsc_dir_t**)calloc(dir->maxdirs, sizeof(fsc_dir_t*));
        if(!dir->dirs) {
            FSC_ERR_SET(ENOMEM, "Out of memory");
            goto fsc_dir_new_err;
        }
    }
    /* If it is a link - it should be set outside this function */

    ret = dir;
    dir = NULL;
fsc_dir_new_err:
    fsc_dir_free(dir);
    return ret;
} /* fsc_dir_new */

void fsc_dir_free (fsc_dir_t *dir) {
    size_t i;

    if(!dir) {
        return;
    }
    free(dir->orig_path);
    free(dir->name);
    for(i=0; i<dir->nfiles; i++) {
        fsc_file_free(dir->files[i]);
    }
    free(dir->files);
    for(i=0; i<dir->ndirs; i++) {
        fsc_dir_free(dir->dirs[i]);
    }
    free(dir->dirs);
    free(dir);
} /* fsc_dir_free */

int fsc_file_add (fsc_dir_t *base_dir, fsc_file_t *file) {
    size_t i;
    fsc_t *fsc;

    if(!base_dir) {
        return -1;
    }
    fsc = base_dir->fsc;
    if(!file) {
        FSC_ERR_SET(EINVAL, "NULL file");
        return -1;
    }

    if(file->parent) {
        FSC_ERR_SET(EINVAL, "File already inside [another] directory");
        return -1;
    }
    if(base_dir->nfiles == base_dir->maxfiles &&
       fsc_resize(&base_dir->files, &base_dir->maxfiles, sizeof(fsc_file_t*))) {
        FSC_ERR_SET(ENOMEM, "Out of memory");
        return -1;
    }
    i = base_dir->nfiles;
    /* Indexes moved by 1 from logically better option to avoid using ssize_t
     * type */
    while(i > 0 && strcmp(base_dir->files[i - 1]->name, file->name) > 0) {
        base_dir->files[i] = base_dir->files[i - 1];
        i--;
    }
    base_dir->files[i] = file;
    base_dir->nfiles++;
    file->parent = base_dir;
    return 0;
} /* fsc_file_add */

int fsc_dir_add (fsc_dir_t *base_dir, fsc_dir_t *dir) {
    size_t i;
    fsc_t *fsc;

    if(!base_dir) {
        return -1;
    }
    fsc = base_dir->fsc;
    if(!dir) {
        FSC_ERR_SET(EINVAL, "NULL directory");
        return -1;
    }

    if(dir->parent) {
        FSC_ERR_SET(EINVAL, "Directory already inside [another] directory");
        return -1;
    }
    if(base_dir->ndirs == base_dir->maxdirs &&
       fsc_resize(&base_dir->dirs, &base_dir->maxdirs,
                  sizeof(base_dir->dirs[0]))) {
        FSC_ERR_SET(ENOMEM, "Out of memory");
        return -1;
    }
    i = base_dir->ndirs;
    /* Indexes moved by 1 from logically better option to avoid using ssize_t
     * type */
    while(i > 0 && strcmp(base_dir->dirs[i - 1]->name, dir->name) > 0) {
        base_dir->dirs[i] = base_dir->dirs[i - 1];
        i--;
    }
    base_dir->dirs[i] = dir;
    base_dir->ndirs++;
    dir->parent = base_dir;
    return 0;
} /* fsc_dir_add */

int fsc_file_get_filter_data (fsc_file_t *file, fsc_filter_t *filter,
                              void **data) {
    fsc_t *fsc;

    if(!file) {
        return -1;
    }

    fsc = file->fsc;

    if(!filter) {
        FSC_ERR_SET(EINVAL, "NULL filter");
        return -1;
    }
    if(!data) {
        FSC_ERR_SET(EINVAL, "NULL data handle");
        return -1;
    }

    if(filter->index == (size_t)-1) {
        FSC_ERR_SET(EINVAL, "Filter's index not set");
        return -1;
    }

    /* This can be true if any filter has been added after the scan */
    if(filter->index >= file->fdata_num) {
        FSC_ERR_SET(EINVAL, "Filter index out of scope");
        return -1;
    }

    *data = file->filters_data[filter->index];
    return 0;
} /* fsc_file_get_filter_data */

int fsc_file_set_filter_data (fsc_file_t *file, fsc_filter_t *filter,
                              void *data) {
    void *curr_data;
    fsc_t *fsc;

    if(!file) {
        return -1;
    }
    fsc = file->fsc;
    if(!filter) {
        FSC_ERR_SET(EINVAL, "NULL filter");
        return -1;
    }

    if(filter->index == (size_t)-1) {
        FSC_ERR_SET(EINVAL, "Filter's index not set");
        return -1;
    }

    /* This can be true if any filter has been added after the scan */
    if(filter->index >= file->fdata_num) {
        FSC_ERR_SET(EINVAL, "Filter index out of scope");
        return -1;
    }

    curr_data = file->filters_data[filter->index];
    if(curr_data && filter->file_data_free) {
        filter->file_data_free(fsc, filter, curr_data);
    }

    file->filters_data[filter->index] = data;
    return 0;
} /* fsc_file_set_filter_data */

fsc_dir_t* fsc_mkpath (fsc_dir_t *base_dir, const char *path) {
    char *workpath, *ptr, *dirname;
    const char delims[] = "/";
    ssize_t index;
    fsc_t *fsc;
    fsc_dir_t *ret = NULL;

    if(!base_dir) {
        return ret;
    }
    fsc = base_dir->fsc;
    if(!path) {
        FSC_ERR_SET(EINVAL, "NULL path");
        return ret;
    }

    /* In case all the directories already exist, this function works
     * exactly like fsc_ftw() */

    workpath = strdup(path);
    if(!workpath) {
        FSC_ERR_SET(ENOMEM, "Out of memory");
        return ret;
    }
    ptr = strrchr(workpath, '/');
    if(!ptr) {
        FSC_ERR_SET(EINVAL, "'/' not found in '%s'", path);
        goto fsc_mkpath_err;
    }
    *ptr = '\0'; /* Treat everything after last slash as a file */

    dirname = strtok(workpath, delims);
    while(dirname) {
        index = fsc_find_entry(base_dir->dirs, base_dir->ndirs, dirname,
                               fsc_dir_cmp);
        if(index < 0) { /* Create new directory */
            fsc_dir_t *newdir = fsc_dir_new(fsc, dirname, &FSC_DIR_ST);

            if(!newdir) {
                FSC_ERROR("Cannot create virtual directory");
                goto fsc_mkpath_err;
            }
            if(fsc_dir_add(base_dir, newdir)) {
                FSC_ERROR("Cannot add virtual directory");
                fsc_dir_free(newdir);
                goto fsc_mkpath_err;
            }
            base_dir = newdir;
        } else { /* Directory already exists, just go ahead */
            base_dir = base_dir->dirs[index];
        }

        dirname = strtok(NULL, delims);
    }

    ret = base_dir;
fsc_mkpath_err:
    free(workpath);
    return ret;
} /* fsc_mkpath */

void fsc_file_del (fsc_dir_t *dir, size_t index) {
    fsc_t *fsc;

    if(!dir) {
        return;
    }
    fsc = dir->fsc;

    if(index >= dir->nfiles) {
        FSC_ERR_SET(EINVAL, "Index out of bounds");
        return;
    }
    while(index + 1 < dir->nfiles) {
        dir->files[index] = dir->files[index + 1];
        index++;
    }
    dir->files[index]->parent = NULL;
    dir->files[index] = NULL;
    dir->nfiles--;
} /* fsc_file_del */

void fsc_dir_del (fsc_dir_t *dir, size_t index) {
    fsc_t *fsc;

    if(!dir) {
        return;
    }
    fsc = dir->fsc;

    if(index >= dir->ndirs) {
        FSC_ERR_SET(EINVAL, "Index out of bounds");
        return;
    }
    while(index + 1 < dir->ndirs) {
        dir->dirs[index] = dir->dirs[index + 1];
        index++;
    }
    dir->dirs[index]->parent = NULL;
    dir->dirs[index] = NULL;
    dir->ndirs--;
} /* fsc_dir_del */

static void fsc_get_path_recursive (fsc_t *fsc, fsc_dir_t *dir, char *buff,
                                    size_t buffsize, size_t *len) {
    if(dir->parent) {
        fsc_get_path_recursive(fsc, dir->parent, buff, buffsize, len);

        if(*len < buffsize) {
            snprintf(buff + *len, buffsize - *len, "/%s", dir->name);
            *len += strlen(dir->name) + 1; /* +1 for slash */
        }
    } else {
        /* Start the path with the mountpoint */
        snprintf(buff, buffsize, "%s", fsc->mountpoint);
        *len += strlen(fsc->mountpoint);
        /* Current directory is root which name is "/" - no point in putting
         * this into the path */
    }
} /* fsc_get_path_recursive */

char* fsc_file_get_path (fsc_file_t *file, char *buff, size_t buffsize) {
    size_t len = 0;
    fsc_t *fsc;

    if(!file) {
        return NULL;
    }
    fsc = file->fsc;
    if(!buff) {
        FSC_ERR_SET(EINVAL, "NULL buffer");
        return NULL;
    }
    if(buffsize == 0) {
        FSC_ERR_SET(EINVAL, "Buffer size of 0");
        return NULL;
    }

    fsc_get_path_recursive(fsc, file->parent, buff, buffsize, &len);
    if(len < buffsize) {
        snprintf(buff + len, buffsize - len, "/%s", file->name);
    }

    return buff;
} /* fsc_file_get_path */

char* fsc_dir_get_path (fsc_dir_t *dir, char *buff, size_t buffsize) {
    size_t len = 0;
    fsc_t *fsc;

    if(!dir) {
        return NULL;
    }
    fsc = dir->fsc;
    if(!buff) {
        FSC_ERR_SET(EINVAL, "NULL buffer");
        return NULL;
    }
    if(buffsize == 0) {
        FSC_ERR_SET(EINVAL, "Buffer size of 0");
        return NULL;
    }

    fsc_get_path_recursive(fsc, dir->parent, buff, buffsize, &len);
    if(len < buffsize) {
        snprintf(buff + len, buffsize - len, "/%s", dir->name);
    }

    return buff;
} /* fsc_dir_get_path */

fsc_file_t* fsc_file_link (fsc_file_t *file) {
    char path[PATH_MAX];
    fsc_t *fsc;
    fsc_file_t *ret = NULL, *link;

    if(!file) {
        return ret;
    }
    fsc = file->fsc;

    if(!fsc_file_get_path(file, path, sizeof(path))) {
        FSC_ERROR("Cannot get '%s' file path", file->name);
        return ret;
    }

    link = fsc_file_new(fsc, file->name, &file->st);
    if(!link) {
        FSC_ERROR("Cannot create virtual file");
        return ret;
    }
    link->st.st_mode |= S_IFLNK;

    link->link_path = strdup(path);
    if(!link->link_path) {
        FSC_ERR_SET(ENOMEM, "Out of memory");
        fsc_file_free(link);
        return ret;
    }

    ret = link;
    return ret;
} /* fsc_file_link */

fsc_dir_t* fsc_dir_link (fsc_dir_t *dir) {
    char path[PATH_MAX];
    fsc_t *fsc;
    fsc_dir_t *ret = NULL, *link;

    if(!dir) {
        return ret;
    }
    fsc = dir->fsc;

    if(!fsc_dir_get_path(dir, path, sizeof(path))) {
        FSC_ERROR("Cannot get '%s' directory path", dir->name);
        return ret;
    }

    link = fsc_dir_new(fsc, dir->name, &dir->st);
    if(!link) {
        FSC_ERROR("Cannot create virtual directory");
        return ret;
    }
    link->st.st_mode |= S_IFLNK;

    link->link_path = strdup(path);
    if(!link->link_path) {
        FSC_ERR_SET(ENOMEM, "Out of memory");
        fsc_dir_free(link);
        return ret;
    }

    link = ret;
    return ret;
} /* fsc_dir_link */

fsc_file_t* fsc_file_follow_links (fsc_file_t *file) {
    ssize_t index;
    char *path, *name;
    fsc_t *fsc;
    fsc_dir_t *dir;

    if(!file) {
        return NULL;
    }
    fsc = file->fsc;

    while(S_ISLNK(file->st.st_mode)) {
        if(!file->link_path) {
            FSC_ERR_SET(EINVAL, "Invalid link configuration");
            return NULL;
        }
        path = file->link_path;
        FSC_VERBOSE("Going to '%s'", path);

        name = strrchr(path, '/');
        if(!name) {
            FSC_ERR_SET(EINVAL, "'/' not found in '%s'", path);
            return NULL;
        }
        name++;

        if(strlen(path) <= strlen(fsc->mountpoint) + 1) {
            FSC_ERR_SET(EINVAL, "Link path to short: %s", path);
            return NULL;
        }
        if(fsc_ftw(fsc, path + strlen(fsc->mountpoint) + 1, &dir)) {
            FSC_ERROR("Cannot load filetree");
            return NULL;
        }

        index = fsc_find_entry(dir->files, dir->nfiles, name, fsc_file_cmp);
        if(index >= 0) {
            file = dir->files[index];
            /* Do not stop here, follow all the links */
        } else {
            index = fsc_find_entry(dir->dirs, dir->ndirs, name, fsc_dir_cmp);
            if(index >= 0) {
                FSC_ERR_SET(EISDIR, "'%s' is a directory", path);
                return NULL;
            } else {
                FSC_ERR_SET(ENOENT, "Dangling link: %s", path);
                return NULL;
            }
        }
    }

    return file;
} /* fsc_file_follow_links */

fsc_dir_t* fsc_dir_follow_links (fsc_dir_t *dir) {
    ssize_t index;
    char *path, *name;
    fsc_t *fsc;
    fsc_dir_t *ftw_dir;

    if(!dir) {
        return NULL;
    }
    fsc = dir->fsc;

    while(S_ISLNK(dir->st.st_mode)) {
        if(!dir->link_path) {
            FSC_ERR_SET(EINVAL, "Invalid link configuration");
            return NULL;
        }
        path = dir->link_path;
        FSC_VERBOSE("Going to '%s'", path);

        name = strrchr(path, '/');
        if(!name) {
            FSC_ERR_SET(EINVAL, "'/' not found in '%s'", path);
            return NULL;
        }
        name++;

        if(strlen(path) <= strlen(fsc->mountpoint) + 1) {
            FSC_ERR_SET(EINVAL, "Link path to short: %s", path);
            return NULL;
        }
        if(fsc_ftw(fsc, path + strlen(fsc->mountpoint) + 1, &ftw_dir)) {
            FSC_ERROR("Cannot load filetree");
            return NULL;
        }

        index = fsc_find_entry(ftw_dir->files, ftw_dir->nfiles, name,
                               fsc_file_cmp);
        if(index >= 0) {
            FSC_ERR_SET(ENOTDIR, "'%s' is not a directory", path);
            return NULL;
        } else {
            index = fsc_find_entry(ftw_dir->dirs, ftw_dir->ndirs, name,
                                   fsc_dir_cmp);
            if(index >= 0) {
                dir = ftw_dir->dirs[index];
                /* Do not stop here, follow all the links */
            } else {
                FSC_ERR_SET(ENOENT, "Dangling link: %s", path);
                return NULL;
            }
        }
    }

    return dir;
} /* fsc_dir_follow_link */

int fsc_file_rename (fsc_file_t *file, const char *newname) {
    char *tmp;
    fsc_t *fsc;
    fsc_dir_t *parent;

    if(!file) {
        return -1;
    }
    fsc = file->fsc;
    if(!newname) {
        FSC_ERR_SET(EINVAL, "NULL filename");
        return -1;
    }

    tmp = strdup(newname);
    if(!tmp) {
        FSC_ERR_SET(ENOMEM, "Out of memory");
        return -1;
    }

    free(file->name);
    file->name = tmp;

    if(file->parent) {
        parent = file->parent;
        qsort(parent->files, parent->nfiles, sizeof(fsc_file_t*), fsc_file_cmp_sort);
    }

    return 0;
} /* fsc_file_rename */

int fsc_dir_rename (fsc_dir_t *dir, const char *newname) {
    char *tmp;
    fsc_t *fsc;
    fsc_dir_t *parent;

    if(!dir) {
        return -1;
    }
    fsc = dir->fsc;
    if(!newname) {
        FSC_ERR_SET(EINVAL, "NULL directory name");
        return -1;
    }

    tmp = strdup(newname);
    if(!tmp) {
        FSC_ERR_SET(ENOMEM, "Out of memory");
        return -1;
    }
    free(dir->name);
    dir->name = tmp;

    if(dir->parent) {
        parent = dir->parent;
        qsort(parent->dirs, parent->ndirs, sizeof(fsc_dir_t*), fsc_dir_cmp_sort);
    }

    return 0;
} /* fsc_dir_rename */

int fsc_file_mv (fsc_file_t *file, fsc_dir_t *dest_dir) {
    int err;
    ssize_t index;
    char file_path[PATH_MAX];
    fsc_t *fsc;
    fsc_dir_t *parent;

    if(!file) {
        return -1;
    }
    fsc = file->fsc;
    if(!dest_dir) {
        FSC_ERR_SET(EINVAL, "NULL destination directory");
        return -1;
    }
    if(!file->parent) {
        FSC_ERR_SET(EINVAL, "File not within virtual filetree");
        return -1;
    }

    parent = file->parent;
    index = fsc_find_entry(parent->files, parent->nfiles, file->name,
                           fsc_file_cmp);
    if(index >= 0) {
        file->parent = NULL; /* Avoid error in 'fsc_file_add' */
        if((err = fsc_file_add(dest_dir, file))) {
            FSC_ERROR("Cannot add virtual file");
            file->parent = parent; /* Recover original value */
            return -1;
        }
        fsc_file_del(parent, index);
        /* 'fsc_file_del' removes 'parent' value */
        file->parent = dest_dir;
    } else {
        FSC_ERR_SET(ENOENT, "No such file: %s",
                    fsc_file_get_path(file, file_path, sizeof(file_path)));
        return -1;
    }
    return 0;
} /* fsc_file_mv */

int fsc_dir_mv (fsc_dir_t *dir, fsc_dir_t *dest_dir) {
    int err;
    ssize_t index;
    char dir_path[PATH_MAX];
    fsc_t *fsc;
    fsc_dir_t *parent;

    if(!dir) {
        return -1;
    }
    fsc = dir->fsc;
    if(!dest_dir) {
        FSC_ERR_SET(EINVAL, "NULL destination directory");
        return -1;
    }
/*    if(!dir->parent && dir != root) {
        FSC_ERR_SET(EINVAL, "Directory not within filesystem");
        return -1;
    }*/
    parent = dir->parent;
    index = fsc_find_entry(parent->dirs, parent->ndirs, dir->name, fsc_dir_cmp);
    if(index >= 0) {
        dir->parent = NULL; /* Avoid error in 'fsc_file_add' */
        if((err = fsc_dir_add(dest_dir, dir))) {
            FSC_ERROR("Cannot add virtual directory");
            dir->parent = parent; /* Recover original value */
            return -1;
        }
        fsc_dir_del(parent, index);
        /* 'fsc_dir_del' removes 'parent' value */
        dir->parent = dest_dir;
    } else {
        FSC_ERR_SET(ENOENT, "No such directory: %s",
                    fsc_dir_get_path(dir, dir_path, sizeof(dir_path)));
        return -1;
    }
    return 0;
} /* fsc_dir_mv */

int fsc_file_cmp (const void *files, size_t index, const char *file_name) {
    return strcmp(((const fsc_file_t* const*)files)[index]->name, file_name);
} /* fsc_file_cmp */

int fsc_dir_cmp (const void *dirs, size_t index, const char *dir_name) {
    return strcmp(((const fsc_dir_t* const*)dirs)[index]->name, dir_name);
} /* fsc_dir_cmp */

int fsc_file_cmp_sort (const void *ptr1, const void *ptr2) {
    return strcmp((*(const fsc_file_t* const*)ptr1)->name,
                  (*(const fsc_file_t* const*)ptr2)->name);
} /* fsc_file_cmp_sort */

int fsc_dir_cmp_sort (const void *ptr1, const void *ptr2) {
    return strcmp((*(const fsc_dir_t* const*)ptr1)->name,
                  (*(const fsc_dir_t* const*)ptr2)->name);
} /* fsc_dir_cmp_sort */

static int fsc_regexp_add (fsc_t *fsc, const char *path, int exclude) {
    fsc_regexp_t *regexp = NULL;

    if(!fsc) {
        return -EINVAL;
    }
    if(!path) {
        FSC_ERR_SET(EINVAL, "NULL path");
        return -1;
    }

    if(fsc->exl_num == fsc->exl_max &&
       fsc_resize(&fsc->exclusion_list, &fsc->exl_max, sizeof(fsc_regexp_t*))) {
        FSC_ERR_SET(ENOMEM, "Out of memory");
        return -1;
    }

    regexp = (fsc_regexp_t*)malloc(sizeof(fsc_regexp_t));
    if(!regexp) {
        FSC_ERR_SET(ENOMEM, "Out of memory");
        return -1;
    }
    regexp->pattern = strdup(path);
    if(!regexp->pattern) {
        FSC_ERR_SET(ENOMEM, "Out of memory");
        free(regexp);
        return -1;
    }
    regexp->exclude = exclude;

    fsc->exclusion_list[fsc->exl_num] = regexp;
    fsc->exl_num++;

    return 0;
} /* fsc_regexp_add */

int fsc_exclude_path (fsc_t *fsc, const char *path) {
    int ret;

    ret = fsc_regexp_add(fsc, path, 1);
    if(ret) {
        FSC_ERROR("Cannot add new regexp");
    }

    return ret;
} /* fsc_exclude_path */

int fsc_include_path (fsc_t *fsc, const char *path) {
    int ret;

    ret = fsc_regexp_add(fsc, path, 0);
    if(ret) {
        FSC_ERROR("Cannot add new regexp");
    }

    return ret;
} /* fsc_include_path */

int fsc_is_excluded (fsc_t *fsc, const char *path) {
    int ret = -1, is_excluded = 0;
    size_t i, path_len, pattern_len;
    char *pattern;
    fsc_regexp_t **list;

    if(!fsc) {
        return ret;
    }
    if(!path) {
        FSC_ERR_SET(EINVAL, "NULL path");
        return ret;
    }

    list = fsc->exclusion_list;
    for(i=0; i<fsc->exl_num; i++) {
        if(list[i]->exclude != is_excluded) {
            switch(fnmatch(list[i]->pattern,
                           path, 0)) {
                case 0:
                    is_excluded = list[i]->exclude;
                    break;
                case FNM_NOMATCH:
                    pattern = list[i]->pattern;
                    path_len = strlen(path);
                    pattern_len = strlen(pattern);

                    /* Path does not match the pattern
                     * Check whether it is included in longer pattern
                     * and eventually go ahead */
                    if(!list[i]->exclude && pattern_len > path_len &&
                       (path[path_len - 1] == '/' || pattern[path_len] == '/') &&
                       !strncmp(path, pattern, path_len)) {
                        return 0; /* Not excluded */
                    }

                    /* Exclude directories itself while excluding their whole
                     * content
                     * '+2' is for '/' and '*' at the end
                     */
                    if(list[i]->exclude && pattern_len == path_len + 2 &&
                       pattern[pattern_len - 2] == '/' &&
                       pattern[pattern_len - 1] == '*' &&
                       !strncmp(pattern, path, path_len)) {
                        is_excluded = list[i]->exclude;
                    }
                    break;
                default:
                    FSC_ERR_SET(errno, "Cannot match given path: %s",
                                strerror(errno));
                    goto fsc_is_excluded_err;
            }
        }
    }

    ret = is_excluded;
fsc_is_excluded_err:
    return ret;
} /* fsc_is_excluded */

static int fsc_init_inclusion (fsc_t *fsc) {
    unsigned int i;

    /* Manage inclusion and exclusion for the scan */
    for(i=0; i<fsc->args.include_given; i++) {
        switch(fsc->args.include_arg[i][0]) {
            case '-':
                if(fsc_exclude_path(fsc, fsc->args.include_arg[i] + 1)) {
                    FSC_ERROR("ERROR: Cannot exclude path: %s\n",
                              fsc->args.include_arg[i] + 1);
                    return -1;
                }
                break;
            case '+':
                if(fsc_include_path(fsc, fsc->args.include_arg[i] + 1)) {
                    FSC_ERROR("ERROR: Cannot include path: %s\n",
                              fsc->args.include_arg[i] + 1);
                    return -1;
                }
                break;
            default:
                FSC_ERROR("ERROR: Invalid inclusion pattern: %s\n",
                          fsc->args.include_arg[i]);
                return -1;
        }
    }

    return 0;
} /* fsc_init_inclusion */

static int fsc_read_filetree (fsc_t *fsc, fsc_dir_t *dir, char *path,
                              size_t len) {
    int ret = -1, excluded;
    size_t i;
    DIR *dir_fd = NULL;
    struct dirent *entry;
    struct stat st;
    fsc_file_t *file;
    fsc_dir_t *subdir;

    /* No parameters checking - internal function */

    dir_fd = opendir(path);
    if(!dir_fd) {
        FSC_ERR_SET(errno, "Cannot open '%s' directory: %s", path,
                    strerror(errno));
        goto fsc_read_filetree_err;
    }
    entry = readdir(dir_fd);
    while(entry) {
        if(strcmp(entry->d_name, ".") && strcmp(entry->d_name, "..")) {
            if(len + strlen(entry->d_name) + 1 >= PATH_MAX) {
                FSC_ERR_SET(ENAMETOOLONG, "File path too long");
                goto fsc_read_filetree_err;
            }
            snprintf(path + len, PATH_MAX - len, "%s", entry->d_name);

            excluded = fsc_is_excluded(fsc, path);
            if(excluded == 0) { /* Not excluded */
                if(lstat(path, &st)) {
                    FSC_ERR_SET(errno, "Cannot stat '%s' file: %s", path,
                                strerror(errno));
                    goto fsc_read_filetree_err;
                }
                if(S_ISDIR(st.st_mode)) {
                    subdir = fsc_dir_new(fsc, path, &st);
                    if(!subdir) {
                        FSC_ERROR("Cannot create virtual directory");
                        goto fsc_read_filetree_err;
                    }
                    if((ret = fsc_dir_add(dir, subdir))) {
                        FSC_ERROR("Cannot add virtual directory");
                        fsc_dir_free(subdir);
                        goto fsc_read_filetree_err;
                    }

                    for(i=0; i<fsc->filters_num; i++) {
                        if(fsc->filters[i]->run &&
                           fsc->filters[i]->run(fsc, fsc->filters[i], NULL,
                                                dir)) {
                            FSC_ERROR("Filter failed on file: %s", path);
                            /* No error returned since we want the program to
                             * continue its work and only log the problem */
                        }
                    }
                } else {
                    file = fsc_file_new(fsc, path, &st);
                    if(!file) {
                        FSC_ERROR("Cannot create virtual file");
                        goto fsc_read_filetree_err;
                    }
                    if(fsc_file_add(dir, file)) {
                        FSC_ERROR("Cannot add virtual file");
                        fsc_file_free(file);
                        goto fsc_read_filetree_err;
                    }

                    for(i=0; i<fsc->filters_num; i++) {
                        if(fsc->filters[i]->run &&
                           fsc->filters[i]->run(fsc, fsc->filters[i], file,
                                                NULL)) {
                            FSC_ERROR("Filter failed on directory: %s", path);
                            /* No error returned since we want the program to
                             * continue its work and only log the problem */
                        }
                    }
                }
            } else if(excluded < 0) {
                FSC_ERROR("Cannot test the inclusion of: %s", path);
                goto fsc_read_filetree_err;
            } /* else = path is excluded */
        }
        entry = readdir(dir_fd);
    }
    path[len] = '\0';
    if(closedir(dir_fd)) {
        FSC_ERR_SET(errno, "Cannot close '%s' dir: %s", path, strerror(errno));
        dir_fd = NULL;
        goto fsc_read_filetree_err;
    }
    dir_fd = NULL;
    if(dir->nfiles) {
        qsort(dir->files, dir->nfiles, sizeof(fsc_file_t*), fsc_file_cmp_sort);
    }
    if(dir->ndirs) {
        qsort(dir->dirs, dir->ndirs, sizeof(fsc_dir_t*), fsc_dir_cmp_sort);
    }
    for(i=0; i<dir->ndirs; i++) {
        snprintf(path + len, PATH_MAX - len, "%s/", dir->dirs[i]->name);
        if(fsc_read_filetree(fsc, dir->dirs[i], path,
                             len + strlen(dir->dirs[i]->name) + 1)) {
            /* Do not log an error to avoid spam in logs as this is
             * recursive function */
            /* Do not fail here, try to load the rest of the filetree */
        }
    }
    path[len] = '\0';

    ret = 0;
fsc_read_filetree_err:
    if(dir_fd && closedir(dir_fd)) {
        FSC_ERROR("Cannot close '%s' dir: %s", path, strerror(errno));
    }
    return ret;
} /* fsc_read_filetree */

int fsc_scan (fsc_t *fsc, const char *path) {
    int ret = -1;
    size_t i, len;
    char *ftw_path = NULL;
    fsc_dir_t *filetree_dir = NULL, *start_dir;

    if(!fsc) {
        return ret;
    }

    /* Run it here to make patterns given from commandline more important
     * than coded ones */
    if(fsc_init_inclusion(fsc)) {
        FSC_ERROR("Cannot process inclusion options");
        return ret;
    }

    filetree_dir = fsc_dir_new(fsc, FSC_MAIN_FILETREE_NAME, &FSC_DIR_ST);
    if(!filetree_dir) {
        FSC_ERROR("Cannot create virtual directory");
        return ret;
    }
    if(fsc_dir_add(fsc->root, filetree_dir)) {
        FSC_ERROR("Cannot add virtual directory");
        goto fsc_scan_err;
    }
    start_dir = filetree_dir;
    filetree_dir = NULL;

    ftw_path = (char*)malloc(PATH_MAX);
    if(!ftw_path) {
        FSC_ERR_SET(ENOMEM, "Out of memory");
        goto fsc_scan_err;
    }
    if(path) {
        len = snprintf(ftw_path, PATH_MAX, "%s", path);
    } else if(fsc->scan_root_path) {
        len = snprintf(ftw_path, PATH_MAX, "%s", fsc->scan_root_path);
    } else {
        len = snprintf(ftw_path, PATH_MAX, "/");
    }
    if(len >= PATH_MAX) {
        FSC_ERR_SET(ENAMETOOLONG, "Path too long: %s",
                    path ? path : fsc->scan_root_path);
        goto fsc_scan_err;
    }
    if(ftw_path[len - 1] != '/') {
        ftw_path[len] = '/';
        ftw_path[len + 1] = '\0';
        len++;
    }

    if(fsc_command_run_globals(fsc)) {
        FSC_ERROR("Cannot run global commands");
        goto fsc_scan_err;
    }

    for(i=0; i<fsc->filters_num; i++) {
        if(fsc->filters[i]->init &&
           fsc->filters[i]->init(fsc, fsc->filters[i])) {
            FSC_ERROR("Cannot initialize filter");
            goto fsc_scan_err;
        }
    }

    if(fsc_read_filetree(fsc, start_dir, ftw_path, len)) {
        FSC_ERROR("Cannot read filesystem");
        goto fsc_scan_err;
    }

    for(i=0; i<fsc->filters_num; i++) {
        if(fsc->filters[i]->finish &&
           fsc->filters[i]->finish(fsc, fsc->filters[i])) {
            FSC_ERROR("Cannot finalize filter");
            goto fsc_scan_err;
        }
    }

    ret = 0;
fsc_scan_err:
    free(ftw_path);
    fsc_dir_free(filetree_dir);
    return ret;
} /* fsc_scan */

int fsc_ftw (fsc_t *fsc, const char *path, fsc_dir_t **given_dir) {
    ssize_t index;
    char *ptr, *slash, *path2;
    fsc_dir_t *dir;

    if(!fsc) {
        return -EINVAL;
    }
    if(!path) {
        FSC_ERR_SET(EINVAL, "NULL directory path");
        return -1;
    }
    if(!given_dir) {
        FSC_ERR_SET(EINVAL, "NULL directory");
        return -1;
    }
    dir = fsc->root;
    path2 = strdup(path);
    if(!path2) {
        FSC_ERR_SET(ENOMEM, "Out of memory");
        return -1;
    }
    ptr = path2;
    if(*ptr == '/') {
        ptr++;
    }
    slash = strchr(ptr, '/');
    while(slash) {
        *slash = '\0';
        index = fsc_find_entry(dir->dirs, dir->ndirs, ptr, fsc_dir_cmp);
        if(index < 0) {
            if(fsc_find_entry(dir->files, dir->nfiles, ptr,
                              fsc_file_cmp) >= 0) {
                FSC_ERR_SET(ENOTDIR, "Not a directory: %s", path);
            } else {
                FSC_ERR_SET(ENOENT, "No such directory: %s", path);
            }
            free(path2);
            return -1;
        }
        dir = dir->dirs[index];
        *slash = '/';
        ptr = slash + 1;
        slash = strchr(ptr, '/');
    }
    free(path2);
    *given_dir = dir;
    return 0;
} /* fsc_ftw */

