#include <fsc.h>
#include <misc.h>
#include <filetree.h>
#include <operations.h>
#include <filters.h>
#include <json_tokens.h>

#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <limits.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>     /* getuid(), getgid() */
#include <sys/types.h>  /* getuid(), getgid() */
#include <ctype.h>      /* isalnum() */

#include <jansson.h>

#define FSC_MUTEX_FLAG_FH               0x1

static int fsc_init_os_id (fsc_t *fsc) {
    int ret = -1, fd = -1;
    char tmp;
    size_t i, done = 0;
    ssize_t bytes;
    char *os_id, *os_id_like;
    char buff[1024]; /* Should be enough to fit content
                        of /etc/os-release file */

    fsc->os_id_like_num = 0;
    fsc->os_id_like_max = FSC_ALLOC_ENTRIES;
    fsc->os_id_like = (char**)malloc(fsc->os_id_like_max);
    if(!fsc->os_id_like) {
        fprintf(stderr, "ERROR: Out of memory\n");
        return ret;
    }

    fd = open("/etc/os-release", O_RDONLY);
    if(fd < 0) {
        fprintf(stderr, "ERROR: Cannot open '/etc/os-release' file: %s\n",
                strerror(errno));
        goto fsc_init_os_id_err;
    }
    while(1) {
        bytes = read(fd, buff + done, sizeof(buff) - done);
        if(bytes) {
            if(bytes < 0) {
                fprintf(stderr, "ERROR: Cannot read from '/etc/os-release': "
                        "%s\n", strerror(errno));
                goto fsc_init_os_id_err;
            }
            done += bytes;
        } else {
            break; /* End of file */
        }
    }
    buff[done] = '\0';

    /* Find all strings first, cut them out after */
    os_id = strstr(buff, "ID=");
    os_id_like = strstr(buff, "ID_LIKE=");

    /* Read ID */
    if(!os_id) {
        fprintf(stderr, "ERROR: ID not found in '/etc/os-release'\n");
        goto fsc_init_os_id_err;
    }
    os_id += lenof("ID=");
    i = 0;
    while(isalnum(os_id[i])) {
        i++;
    }
    os_id[i] = '\0';

    fsc->os_id = strdup(os_id);
    if(!fsc->os_id) {
        fprintf(stderr, "ERROR: Out of memory\n");
        goto fsc_init_os_id_err;
    }

    /* Read ID_LIKE (possible multiple entries) */
    if(!os_id_like) {
        /* ID_LIKE is not a required key */
        if(fsc->args.verbose_flag) {
            fprintf(stderr, "ID_LIKE not present in '/etc/os-release'\n");
        }
    } else {
        os_id_like += lenof("ID_LIKE=");
        if(*os_id_like == '"') {
            os_id_like++;
        }

        while(1) {
            while(isalnum(os_id_like[i])) {
                i++;
            }
            tmp = os_id_like[i];
            os_id_like[i] = '\0';

            if(fsc->os_id_like_num == fsc->os_id_like_max &&
               fsc_resize(&fsc->os_id_like, &fsc->os_id_like_max,
                          sizeof(char*))) {
                fprintf(stderr, "ERROR: Out of memory\n");
                return -1;
            }
            fsc->os_id_like[fsc->os_id_like_num] = strdup(os_id_like);
            if(!fsc->os_id_like[fsc->os_id_like_num]) {
                fprintf(stderr, "ERROR: Out of memory\n");
                goto fsc_init_os_id_err;
            }
            fsc->os_id_like_num++;

            if(tmp == '"' || tmp == '\n') {
                break;
            } else {
                os_id_like += i + 1;
                i = 0;
            }
        }
    }

    ret = 0;
fsc_init_os_id_err:
    if(fd >= 0 && close(fd)) {
        fprintf(stderr, "ERROR: Cannot close '/etc/os-release' file: %s\n",
                strerror(errno));
    }
    return ret;
} /* fsc_init_is_id */

/* 'argc' is not needed since 'argv[0]' is always set */
static int fsc_init_fuse_flags (fsc_t *fsc, char *prog_name) {
    int err = ENOMSG;

    /* Program name */
    if(fuse_opt_add_arg(&fsc->fargs, prog_name)) {
        fprintf(stderr, "ERROR: Out of memory\n");
        err = ENOMEM;
        goto fsc_init_fuse_flags_err;
    }
    /* Mountpoint */
    if(fuse_opt_add_arg(&fsc->fargs, fsc->mountpoint)) {
        fprintf(stderr, "ERROR: Out of memory\n");
        err = ENOMEM;
        goto fsc_init_fuse_flags_err;
    }
    /* Always run in foreground - fsc is not a daemon */
    if(fuse_opt_add_arg(&fsc->fargs, "-f")) {
        fprintf(stderr, "ERROR: Out of memory\n");
        err = ENOMEM;
        goto fsc_init_fuse_flags_err;
    }
    /* Process FUSE-related arguments from gengetopt */
    if(fsc->args.fuse_single_threaded_flag &&
       fuse_opt_add_arg(&fsc->fargs, "-s")) {
        fprintf(stderr, "ERROR: Out of memory\n");
        err = ENOMEM;
        goto fsc_init_fuse_flags_err;
    }
    if(fsc->args.fuse_debug_flag &&
       fuse_opt_add_arg(&fsc->fargs, "-d")) {
        fprintf(stderr, "ERROR: Out of memory\n");
        err = ENOMEM;
        goto fsc_init_fuse_flags_err;
    }

    err = 0;
fsc_init_fuse_flags_err:
    return err;
} /* fsc_init_fuse_flags */

static int fsc_init_config_files (fsc_t *fsc) {
    int ret;
    size_t i;

    for(i=0; i<fsc->args.config_given; i++) {
        if((ret = fsc_cfg_load_file(fsc, fsc->args.config_arg[i]))) {
            fprintf(stderr, "ERROR: Cannot load config from '%s' file\n",
                    fsc->args.config_arg[i]);
            return ret;
        }
    }

    return 0;
} /* fsc_init_config_files */

static int fsc_init_logfile (fsc_t *fsc) {
    int err = ENOMSG, fd = -1;
    char *logpath = NULL;
    struct timeval tv;
    struct tm *tm;

    if(fsc->args.logfile_given) {
        /* 'logfile' from commandline has higher priority than from
         * configuration files */
        logpath = strdup(fsc->args.logfile_arg);
        if(!logpath) {
            fprintf(stderr, "ERROR: Out of memory");
            return ENOMEM;
        }

        /* Store correct logfile path */
        if(fsc->logfile_path) {
            free(fsc->logfile_path);
        }
        fsc->logfile_path = logpath;
    } else if(fsc->logfile_path) {
        /* Path from configuration file */
        logpath = fsc->logfile_path;
    }

    if(logpath) {
        fsc->logfile = fopen(logpath, "a+");
        if(!fsc->logfile) {
            err = errno;
            fprintf(stderr, "ERROR: Cannot open '%s' file: %s\n",
                    logpath, strerror(errno));
            return err;
        }
        return 0;
    }

    /* Generate logfile path only when no 'logfile' has been given in any
     * available way */
    if(gettimeofday(&tv, NULL)) {
        err = errno;
        fprintf(stderr, "ERROR: Cannot get current time: %s\n",
                strerror(errno));
        return err;
    }
    tm = localtime(&tv.tv_sec);
    if(!tm) {
        err = errno;
        fprintf(stderr, "ERROR: Cannot convert time value\n");
        return err;
    }

    logpath = (char*)malloc(lenof(FSC_TEMP_DIR) +
                                  lenof("fsc-YYYYMMDDHHMMSS-XXXXXX.log") + 1);
    if(!logpath) {
        fprintf(stderr, "ERROR: Out of memory\n");
        return ENOMEM;
    }
    sprintf(logpath, "%s/fsc-%04d%02d%02d%02d%02d%02d-XXXXXX.log",
            FSC_TEMP_DIR, tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday,
            tm->tm_hour, tm->tm_min, tm->tm_sec);

    if((fd = mkstemps(logpath, lenof(".log"))) < 0) {
        err = errno;
        fprintf(stderr, "ERROR: Cannot create unique file: %s\n",
                strerror(errno));
        goto fsc_init_logfile_err;
    }
    fsc->logfile = fdopen(fd, "a+");
    if(!fsc->logfile) {
        err = errno;
        fprintf(stderr, "ERROR: Cannot open stream to '%s' file: %s\n",
                logpath, strerror(errno));
        if(close(fd)) {
            fprintf(stderr, "ERROR: Cannot close (%d) file descriptor: "
                    "%s\n", fd, strerror(errno));
        }
        goto fsc_init_logfile_err;
    }
    /* After successful fdopen() call, 'fd' will be closed when the file
     * stream is closed */
    printf("Created default logfile: %s\n", logpath);

    err = 0;
fsc_init_logfile_err:
    free(logpath);
    return err;
} /* fsc_init_logfile */

static int fsc_init_scan_root (fsc_t *fsc) {
    char *scan_root;

    if(fsc->args.scan_root_given) {
        /* 'scan_root' from commandline has higher priority than in
         * configuration files */
        scan_root = strdup(fsc->args.scan_root_arg);
        if(!scan_root) {
            fprintf(stderr, "ERROR: Out of memory");
            return ENOMEM;
        }

        if(fsc->scan_root_path) {
            free(fsc->scan_root_path);
        }
        fsc->scan_root_path = scan_root;
    }

    return 0;
} /* fsc_init_scan_root */

static int fsc_init_filetree (fsc_t *fsc) {
    fsc_file_t *abort_file;

    fsc->root = fsc_dir_new(fsc, "/", &FSC_DIR_ST);
    if(!fsc->root) {
        fprintf(stderr, "ERROR: Cannot initialize filetree structure: %s\n",
                fsc->error_msg);
        return fsc->error;
    }
    free(fsc->root->name);
    fsc->root->name = strdup("/");
    if(!fsc->root->name) {
        fprintf(stderr, "ERROR: Out of memory\n");
        return ENOMEM;
    }

    abort_file = fsc_file_new(fsc, FSC_ABORT_FILE_PATH + 1, &FSC_FILE_ST);
    if(!abort_file) {
        fprintf(stderr, "ERROR: Cannot create new virtual file\n");
        return fsc->error;
    }
    abort_file->st.st_size = 1;
    if(fsc_file_add(fsc->root, abort_file)) {
        fprintf(stderr, "ERROR: Cannot add new virtual file to the "
                "filetree\n");
        fsc_file_free(abort_file);
        return fsc->error;
    }

    return 0;
} /* fsc_init_filetree */

static int fsc_init_alloc_arrays (fsc_t *fsc) {
    int err = ENOMSG;

    /* Exclusion/inclusion list */
    fsc->exl_max = FSC_ALLOC_ENTRIES;
    fsc->exclusion_list = (fsc_regexp_t**)calloc(fsc->exl_max,
                                                 sizeof(fsc_regexp_t*));
    if(!fsc->exclusion_list) {
        fprintf(stderr, "ERROR: Out of memory\n");
        return ENOMEM;
    }

    /* Filters */
    fsc->filters_max = FSC_ALLOC_ENTRIES;
    fsc->filters = (fsc_filter_t**)calloc(fsc->filters_max,
                                          sizeof(fsc_filter_t*));
    if(!fsc->filters) {
        fprintf(stderr, "ERROR: Out of memory\n");
        err = ENOMEM;
        goto fsc_init_alloc_arrays_err;
    }

    /* Operations history */
    fsc->op_max = FSC_ALLOC_ENTRIES;
    fsc->operations = (fsc_operation_t**)calloc(fsc->op_max,
                                                sizeof(fsc_operation_t*));
    if(!fsc->operations) {
        fprintf(stderr, "ERROR: Out of memory\n");
        err = ENOMEM;
        goto fsc_init_alloc_arrays_err;
    }

    /* File handles */
    fsc->fh_files = (fsc_file_t**)calloc(fsc->fh_limit, sizeof(fsc_file_t*));
    if(!fsc->fh_files) {
        fprintf(stderr, "ERROR: Out of memory\n");
        err = ENOMEM;
        goto fsc_init_alloc_arrays_err;
    }

    /* Directories handles */
    fsc->fh_dirs = (fsc_dir_t**)calloc(fsc->fh_limit, sizeof(fsc_dir_t*));
    if(!fsc->fh_dirs) {
        fprintf(stderr, "ERROR: Out of memory\n");
        err = ENOMEM;
        goto fsc_init_alloc_arrays_err;
    }

    /* Commands */
    fsc->cmd_max = FSC_ALLOC_ENTRIES;
    fsc->commands = (fsc_command_t**)calloc(fsc->cmd_max,
                                            sizeof(fsc_command_t*));
    if(!fsc->commands) {
        fprintf(stderr, "ERROR: Out of memory\n");
        err = ENOMEM;
        goto fsc_init_alloc_arrays_err;
    }

    err = 0;
fsc_init_alloc_arrays_err:
    return err;
} /* fsc_init_alloc_arrays */

fsc_t* fsc_init (int argc, char **argv) {
    int err = 0;
    fsc_t *ret = NULL, *fsc = NULL;

    /* Finish FSC_FILE_ST and FSC_DIR_ST initialization */
    FSC_FILE_ST.st_uid = FSC_DIR_ST.st_uid = getuid();
    FSC_FILE_ST.st_gid = FSC_DIR_ST.st_gid = getgid();

    fsc = (fsc_t*)calloc(1, sizeof(fsc_t));
    if(!fsc) {
        fprintf(stderr, "ERROR: Out of memory\n");
        errno = ENOMEM;
        return NULL;
    }
    if(cmdline_parser(argc, argv, &fsc->args)) {
        cmdline_parser_print_help();
        fprintf(stderr, "\nERROR: Incorrect usage\n");
        err = EINVAL;
        goto fsc_init_err;
    }
    if(fsc->args.inputs_num > 1) {
        cmdline_parser_print_help();
        fprintf(stderr, "\nERROR: Wrong number of arguments\n");
        err = EINVAL;
        goto fsc_init_err;
    }

    /* Loading config files requires few arrays to be already allocated,
     * e.g. exclusion list
     */
    if((err = fsc_init_alloc_arrays(fsc))) {
        fprintf(stderr, "ERROR: Cannot allocate required memory\n");
        goto fsc_init_err;
    }

    /* Configuration files have to parsed in order to create logfile because
     * logfile path can be specified in configuration files and not at command
     * line
     */
    if((err = fsc_init_config_files(fsc))) {
        fprintf(stderr, "ERROR: Cannot load configuration files\n");
        goto fsc_init_err;
    }

    /* We want logfile to be created as soon as possible */
    if((err = fsc_init_logfile(fsc))) {
        fprintf(stderr, "ERROR: Cannot initialize logfile\n");
        goto fsc_init_err;
    }
    /* From this place FSC_LOG(), FSC_ERROR() etc. macros can be used */

    if(fsc->args.verbose_flag) {
        fsc->args.debug_flag = 1;
    }
    fsc->fh_limit = fsc->args.open_limit_arg + 1; /* 0 is used as 'not set' */

    if((err = fsc_init_os_id(fsc))) {
        FSC_ERROR("Cannot get OS IDs");
        goto fsc_init_err;
    }

    /* Initialize magic library - required to gruop files by type */
    fsc->magic_cookie = magic_open(MAGIC_MIME_TYPE);
    if(!fsc->magic_cookie) {
        err = errno;
        FSC_ERROR("Cannot initialize magic library");
        goto fsc_init_err;
    }
    if(magic_load(fsc->magic_cookie, NULL)) {
        err = magic_errno(fsc->magic_cookie);
        FSC_ERROR("Cannot load magic database: %s",
                  magic_error(fsc->magic_cookie));
        goto fsc_init_err;
    }

    /* Initialize pthread stuff */
    if((err = pthread_mutex_init(&fsc->fh_mutex, NULL))) {
        FSC_ERROR("Cannot initialize fh_mutex: %s", strerror(err));
        goto fsc_init_err;
    }
    fsc->mutex_init |= FSC_MUTEX_FLAG_FH;

    if(fsc->args.inputs_num == 1) {
        /* Simplify and store mountpoint path */
        if(fsc->mountpoint) {
            free(fsc->mountpoint);
        }
        fsc->mountpoint = realpath(fsc->args.inputs[0], NULL);
        if(!fsc->mountpoint) {
            err = errno;
            FSC_ERROR("Cannot simplify mountpoint path: '%s': %s",
                      fsc->args.inputs[0], strerror(errno));
            goto fsc_init_err;
        }
        FSC_VERBOSE("Mountpoint simplified from '%s' to '%s'",
                    fsc->args.inputs[0], fsc->mountpoint);
    }

    if(!fsc->mountpoint) {
        cmdline_parser_print_help();
        FSC_ERROR("No mountpoint has been specified. Use unnamed command line "
                  "option or config file");
        err = EINVAL;
        goto fsc_init_err;
    }

    if((err = fsc_init_fuse_flags(fsc, argv[0]))) {
        FSC_ERROR("Cannot initialize FUSE options");
        goto fsc_init_err;
    }

    if((err = fsc_init_scan_root(fsc))) {
        FSC_ERROR("Cannot initialize scan root path");
        goto fsc_init_err;
    }

    /* Initialize filetree */
    if((err = fsc_init_filetree(fsc))) {
        FSC_ERROR("Cannot initialize filetree structure");
        goto fsc_init_err;
    }

    if(fsc_filters_init(fsc)) {
        FSC_ERROR("Cannot initialize filters");
        err = fsc_get_err(fsc);
        goto fsc_init_err;
    }

    /* Export final settings in the JSON format */
    if(fsc->args.export_json_given) {
        if((err = fsc_json_export(fsc, fsc->args.export_json_arg))) {
            FSC_ERROR("Cannot export settings to JSON file");
            goto fsc_init_err;
        }
    }

    ret = fsc;
    fsc = NULL;
fsc_init_err:
    fsc_free(fsc);
    errno = err;
    return ret;
} /* fsc_init */

void fsc_free (fsc_t *fsc) {
    int err;
    size_t i;

    if(!fsc) {
        return;
    }

    fuse_opt_free_args(&fsc->fargs);
    cmdline_parser_free(&fsc->args);
    magic_close(fsc->magic_cookie);

    if(fsc->logfile && fclose(fsc->logfile) == EOF) {
        fprintf(stderr, "ERROR: Cannot close '%s' file: %s\n",
                fsc->args.logfile_arg, strerror(errno));
    }

    /* This has to be freed before cleaning up the filters */
    fsc_dir_free(fsc->root);

    free(fsc->logfile_path);
    free(fsc->scan_root_path);
    free(fsc->mountpoint);

    free(fsc->os_id);
    if(fsc->os_id_like) {
        for(i=0; i<fsc->os_id_like_num; i++) {
            free(fsc->os_id_like[i]);
        }
        free(fsc->os_id_like);
    }

    if(fsc->exclusion_list) {
        for(i=0; i<fsc->exl_num; i++) {
            free(fsc->exclusion_list[i]->pattern);
            free(fsc->exclusion_list[i]);
        }
        free(fsc->exclusion_list); /* This does not have to be inside if() */
    }

    if(fsc->operations) {
        for(i=0; i<fsc->op_num; i++) {
            fsc_operation_free(fsc->operations[i]);
        }
        free(fsc->operations); /* This does not have to be inside if() */
    }

    if(fsc->filters) {
        for(i=0; i<fsc->filters_num; i++) {
            fsc_filter_free(fsc->filters[i]);
        }
        free(fsc->filters); /* This does not have to be inside if() */
    }

    if(fsc->commands) {
        for(i=0; i<fsc->cmd_num; i++) {
            fsc_command_free(fsc->commands[i]);
        }
        free(fsc->commands); /* This does not have to be inside if() */
    }

    free(fsc->fh_files);
    free(fsc->fh_dirs);

    if((fsc->mutex_init & FSC_MUTEX_FLAG_FH) &&
       (err = pthread_mutex_destroy(&fsc->fh_mutex))) {
        fprintf(stderr, "ERROR: Cannot destroy fh_mutex: %s\n",
                strerror(err));
    }
    fsc->mutex_init &= ~FSC_MUTEX_FLAG_FH;

    free(fsc);

    fsc_filters_clean();
} /* fsc_free */

int fsc_cfg_load_file (fsc_t *fsc, const char *path) {
    int ret;
    size_t i;
    json_t *json = NULL, *obj;
    json_error_t err;
    fsc_json_handle_t *handle;

    /* This function is called before logfile initialization in 'fsc_init()'
     * so FSC_ERR_SET cannot be used here */
    if(!path) {
        fprintf(stderr, "ERROR: NULL config filename\n");
        return EINVAL;
    }

    json = json_load_file(path, 0, &err);
    if(!json) {
        fprintf(stderr, "ERROR: Cannot parse json: %s (line: %d, column: %d)\n",
                err.text, err.line, err.column);
        return EINVAL;
    }
    if(!json_is_object(json)) {
        fprintf(stderr, "ERROR: Loaded json is not an object\n");
        ret = EINVAL;
        goto fsc_cfg_load_file_err;
    }

    for(i=0; i<json_tokens_size; i++) {
        handle = &json_tokens[i];

        obj = json_object_get(json, handle->key);
        if(obj) {
            /* There is no such type as JSON_BOOLEAN so JSON_TRUE or JSON_FALSE
             * has to be used and it requires special handling */
            if(json_typeof(obj) == handle->type ||
               ((handle->type == JSON_TRUE || handle->type == JSON_FALSE) &&
               (json_is_true(obj) || json_is_false(obj)))) {
                if((ret = handle->set_value(fsc, handle, obj))) {
                    goto fsc_cfg_load_file_err;
                }
            } else {
                fprintf(stderr, "ERROR: '%s' key has value of wrong type\n",
                        handle->key);
                ret = EINVAL;
                goto fsc_cfg_load_file_err;
            }
        }
        /* No 'else' beause this code is supposed to work with JSON without
         * every key being defined */
    }

    ret = 0;
fsc_cfg_load_file_err:
    if(json) {
        json_decref(json);
    }
    return ret;
} /* fsc_cfg_load_file */

void fsc_set_err (fsc_t *fsc, int error, const char *format, ...) {
    va_list vl;

    if(!fsc) {
        return;
    }
    va_start(vl, format);
    fsc->error = error;
    vsnprintf(fsc->error_msg, sizeof(fsc->error_msg), format, vl);
    va_end(vl);
} /* fsc_set_err */

void fsc_clear_err (fsc_t *fsc) {
    if(!fsc) {
        return;
    }
    fsc->error = 0;
    memset(fsc->error_msg, 0, sizeof(fsc->error_msg));
    fsc->error_msg[0] = '\0';
} /* fsc_clear_err */

int fsc_get_err (fsc_t *fsc) {
    return fsc ? fsc->error : -1;
} /* fsc_get_err */

const char* fsc_get_err_msg (fsc_t *fsc) {
    return fsc ? fsc->error_msg : NULL;
} /* fsc_get_err_msg */

fsc_dir_t* fsc_get_root (fsc_t *fsc) {
    return fsc ? fsc->root : NULL;
} /* fsc_get_root */

