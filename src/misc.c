#include "misc.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>

struct stat FSC_FILE_ST = {
        .st_dev = 0,                /* ID of device containing file */
        .st_ino = 0,                /* inode number */
        .st_mode = FSC_FILE_MODE,   /* protection */
        .st_nlink = 1,              /* number of hard links */
        .st_uid = 0,                /* user ID of owner */
        .st_gid = 0,                /* group ID of owner */
        /* UID and GID will be set in fsc_init() */
        .st_rdev = 0,               /* device ID (if special file) */
        .st_size = 0,               /* total size, in bytes */
        .st_blksize = 1024,         /* blocksize for filesystem I/O */
        .st_blocks = 0,             /* number of 512B blocks allocated */
        .st_atim = {
                .tv_sec = 0,
                .tv_nsec = 0
        },                          /* time of last access */
        .st_mtim = {
                .tv_sec = 0,
                .tv_nsec = 0
        },                          /* time of last modification */
        .st_ctim= {
                .tv_sec = 0,
                .tv_nsec = 0
        }                           /* time of last status change */
};

struct stat FSC_DIR_ST = {
        .st_dev = 0,                /* ID of device containing file */
        .st_ino = 0,                /* inode number */
        .st_mode = FSC_DIR_MODE,    /* protection */
        .st_nlink = 1,              /* number of hard links */
        .st_uid = 0,                /* user ID of owner */
        .st_gid = 0,                /* group ID of owner */
        /* UID and GID will be set in fsc_init() */
        .st_rdev = 0,               /* device ID (if special file) */
        .st_size = 0,               /* total size, in bytes */
        .st_blksize = 1024,         /* blocksize for filesystem I/O */
        .st_blocks = 0,             /* number of 512B blocks allocated */
        .st_atim = {
                .tv_sec = 0,
                .tv_nsec = 0
        },                          /* time of last access */
        .st_mtim = {
                .tv_sec = 0,
                .tv_nsec = 0
        },                          /* time of last modification */
        .st_ctim= {
                .tv_sec = 0,
                .tv_nsec = 0
        }                           /* time of last status change */
};

static const char truncated[] = "[...]";

void fsc_log (fsc_t *fsc, const char *fun_name, int log_level,
              const char *format_string, ...) {
    int len = 0, n;
    char buff[65536];
    int size = sizeof(buff) - sizeof(truncated) + 1;
    struct timeval tv;
    struct tm *tm = NULL;
    va_list vl;

    if(!fsc ||
       (log_level == FSC_LOG_LEVEL_DEBUG && !fsc->args.debug_flag) ||
       (log_level == FSC_LOG_LEVEL_VERBOSE && !fsc->args.verbose_flag))
        return;
    if(!gettimeofday(&tv, NULL)) {
        tm = localtime(&tv.tv_sec);
        if(tm)
            n = snprintf(buff, size, "%02d-%02d-%04d %02d:%02d:%02d.%03d ",
                         tm->tm_mday, tm->tm_mon + 1, tm->tm_year + 1900,
                         tm->tm_hour, tm->tm_min, tm->tm_sec,
                         (int)(tv.tv_usec / 1000));
    }
    if(!tm) {
        n = snprintf(buff, size, "dd-mm-yyyy hh:mm:ss ");
    }
    size -= n;
    len += n;
    if(fun_name && size > 0) {
        n = snprintf(buff + len, size, "[%s] ", fun_name);
        size -= n;
        len += n;
    }
    if(size > 0) {
        n = snprintf(buff + len, size, "%s: ",
                     log_level == FSC_LOG_LEVEL_ERROR ? "ERROR " : "");
        size -= n;
        len += n;
    }
    if(size > 0) {
        va_start(vl, format_string);
        n = vsnprintf(buff + len, size, format_string, vl);
        va_end(vl);
        size -= n;
    }
    if(size <= 0) {
        memcpy(buff + sizeof(buff) - sizeof(truncated), truncated,
               sizeof(truncated));
    }

    fprintf(fsc->logfile, "%s\n", buff);
    fflush(fsc->logfile);
} /* fsc_log */

int fsc_resize (void *ptr, size_t *size, size_t elsize) {
    void *new_ptr = realloc(*((void**)ptr),
                            (*size + FSC_ALLOC_ENTRIES) * elsize);
    if(!new_ptr) {
        return -1;
    }
    *((void**)ptr) = new_ptr;
    memset((char*)(*((void**)ptr)) + *size * elsize, 0,
           FSC_ALLOC_ENTRIES * elsize);
    *size += FSC_ALLOC_ENTRIES;
    return 0;
} /* fsc_resize */

ssize_t fsc_find_entry (const void *array, size_t size, const char *name,
                        int (*compare)(const void*, size_t, const char*)) {
    int tmp;
    ssize_t i;
    size_t from = 0, to;

    if(!array || !size || !name || !compare) {
        return -1;
    }

    to = size - 1;
    while(1) {
        i = (from + to) / 2;
        tmp = compare(array, i, name);
        if(!tmp) {
            return i;
        } else {
            if(from == to) {
                return -1;
            }
            if(tmp > 0) {
                to = i;
            } else {
                from = i + 1;
            }
        }
    }
} /* fsc_find_entry */

int fsc_merge_paths (fsc_t *fsc, const char *orig_path,
                     char link_path[PATH_MAX]) {
    int ret = -1;
    char *ptr, *dir = NULL, *name, result_path[PATH_MAX];

    if(!fsc) {
        return ret;
    }
    if(!orig_path || !link_path) {
        FSC_ERR_SET(EINVAL, "Invalid argument");
        return ret;
    }

    /* handle relative paths */
    if(link_path[0] != '/') {
        dir = strdup(orig_path);
        if(!dir) {
            FSC_ERR_SET(ENOMEM, "Out of memory");
            return ret;
        }

        /* get cwd path - drop eventual file name */
        ptr = strrchr(dir, '/');
        if(!ptr) {
            FSC_ERR_SET(EINVAL, "'/' not found in '%s'", dir);
            goto fsc_merge_paths_err;
        }
        *ptr = '\0';

        name = link_path;
        while(name[0] == '.') {
            if(!strncmp(name, "./", 2)) {
                name += 2;
            } else if(!strncmp(name, "../", 3)) {
                ptr = strrchr(dir, '/');
                if(!ptr) {
                    FSC_ERR_SET(EINVAL, "Unmergable paths: '%s', '%s'", dir,
                                name);
                    goto fsc_merge_paths_err;
                }
                *ptr = '\0';
                name += 3;
            }
        }
        /* Do not write directly to 'link_path' since 'name' is a pointer
         * within 'link_path'
         * 'dir' already has '/' at the beginning */
        snprintf(result_path, sizeof(result_path), "%s/%s%s/%s",
                 fsc->mountpoint, FSC_MAIN_FILETREE_NAME, dir, name);
    } else {
        /* Do not write directly to 'link_path' since we are using it as an
         * argument
         * 'link_path' already has '/' at the beginning */
        snprintf(result_path, sizeof(result_path), "%s/%s%s",
                 fsc->mountpoint, FSC_MAIN_FILETREE_NAME, link_path);
    }
    strcpy(link_path, result_path);


    ret = 0;
fsc_merge_paths_err:
    free(dir);
    return ret;
} /* fsc_merge_paths */

size_t fsc_strncpy (char *dest, const char *src, size_t length) {
    size_t bytes_copied = 0;

    length--; /* Make space for string termination */
    while(length && *src) {
        *dest = *src;
        dest++;
        src++;
        length--;
        bytes_copied++;
    }

    /* Be sure destination string is correctly terminated */
    *dest = '\0';
    bytes_copied++;

    return bytes_copied;
} /* fsc_strncpy */

int fsc_copy_file (fsc_t *fsc, const char *source, const char *dest) {
    int ret = -1, fd_src, fd_dst;
    ssize_t bytes;
    char buff[65536];

    if(!fsc) {
        return -1;
    }
    if(!source || !dest) {
        FSC_ERR_SET(EINVAL, "NULL path");
        return -1;
    }

    fd_src = open(source, O_RDONLY);
    if(fd_src < 0) {
        FSC_ERR_SET(errno, "Cannot open '%s' file: %s", source,
                    strerror(errno));
        return ret;
    }
    fd_dst = open(dest, O_WRONLY | O_CREAT | O_EXCL, 0600);
    if(fd_dst < 0) {
        FSC_ERR_SET(errno, "Cannot create '%s' file: %s", dest,
                    strerror(errno));
        goto fsc_copy_file_err;
    }

    while((bytes = read(fd_src, buff, sizeof(buff))) > 0) {
        if(write(fd_dst, buff, bytes) < 0) {
            FSC_ERR_SET(errno, "Cannot write to '%s' file: %s", dest,
                        strerror(errno));
            goto fsc_copy_file_err;
        }
    }

    if(bytes < 0) {
        FSC_ERR_SET(errno, "Cannot read from '%s' file: %s", source,
                    strerror(errno));
        goto fsc_copy_file_err;
    }

    ret = 0;
fsc_copy_file_err:
    close(fd_src);
    if(fd_dst >= 0) {
        close(fd_dst);
        unlink(dest);
    }
    return ret;
} /* fsc_copy_file */

int fsc_move_file (fsc_t *fsc, const char *source, const char *dest) {
    if(!fsc) {
        return -1;
    }
    if(!source || !dest) {
        FSC_ERR_SET(EINVAL, "NULL path");
        return -1;
    }

    if(rename(source, dest)) {
        if(errno == EXDEV) {
            FSC_DEBUG("Moving file between different filesystems");
            if(fsc_copy_file(fsc, source, dest)) {
                FSC_ERROR("Cannot copy '%s' file to '%s'", source, dest);
                return -1;
            }
            if(unlink(source)) {
                FSC_ERR_SET(errno, "Cannot unlink '%s' file: %s", source,
                            strerror(errno));
                /* Do not return the error, this is not critical */
            }
        } else {
            FSC_ERR_SET(errno, "Cannot rename '%s' to '%s': %s", source,
                        dest, strerror(errno));
            return -1;
        }
    }
    return 0;
} /* fsc_move_file */

