CC = gcc
LD = ld

SRC_DIR = src
INCLUDE_DIR = include
OBJ_DIR = obj
FILTERS_DIR = $(SRC_DIR)/filters

FSC_CFLAGS = -I $(INCLUDE_DIR)
FUSE_CFLAGS = $(shell pkg-config --cflags "fuse >= 2.7.0")

FUSE_LDFLAGS = $(shell pkg-config --libs "fuse >= 2.7.0")
OPENSSL_LDFLAGS = $(shell pkg-config --libs openssl)
JSON_LDFLAGS = $(shell pkg-config --libs jansson)
MAGIC_LDFLAGS = -lmagic

FLAGS = $(CFLAGS) $(FSC_CFLAGS) $(FUSE_CFLAGS)
LIBS = $(FUSE_LDFLAGS) $(OPENSSL_LDFLAGS) $(MAGIC_LDFLAGS) $(JSON_LDFLAGS)

all: main

fsc:
	$(CC) $(FLAGS) $(SRC_DIR)/fsc.c -c -o $(OBJ_DIR)/fsc.o $(LIBS)

filetree:
	$(CC) $(FLAGS) $(SRC_DIR)/filetree.c -c -o $(OBJ_DIR)/filetree.o $(LIBS)

misc:
	$(CC) $(FLAGS) $(SRC_DIR)/misc.c -c -o $(OBJ_DIR)/misc.o $(LIBS)

operations:
	$(CC) $(FLAGS) $(SRC_DIR)/operations.c -c -o $(OBJ_DIR)/operations.o $(LIBS)

syscalls:
	$(CC) $(FLAGS) $(SRC_DIR)/syscalls.c -c -o $(OBJ_DIR)/syscalls.o $(LIBS)

filters:
	$(CC) $(FLAGS) ${FILTERS_DIR}/base.c -c -o $(FILTERS_DIR)/base.o $(LIBS)
	$(CC) $(FLAGS) ${FILTERS_DIR}/zero.c -c -o $(FILTERS_DIR)/zero.o $(LIBS)
	$(CC) $(FLAGS) ${FILTERS_DIR}/sparse.c -c -o $(FILTERS_DIR)/sparse.o $(LIBS)
	$(CC) $(FLAGS) ${FILTERS_DIR}/media_type.c -c -o $(FILTERS_DIR)/media_type.o $(LIBS)
	$(CC) $(FLAGS) ${FILTERS_DIR}/duplicates.c -c -o $(FILTERS_DIR)/duplicates.o $(LIBS)
	$(LD) -r $(FILTERS_DIR)/*.o -o $(OBJ_DIR)/filters.o

json_tokens:
	$(CC) $(FLAGS) $(SRC_DIR)/json_tokens.c -c -o $(OBJ_DIR)/json_tokens.o $(LIBS)

command:
	$(CC) $(FLAGS) $(SRC_DIR)/command.c -c -o $(OBJ_DIR)/command.o $(LIBS)

main: fsc filetree misc operations syscalls filters json_tokens command
	$(CC) $(FLAGS) $(SRC_DIR)/main.c $(OBJ_DIR)/fsc.o $(OBJ_DIR)/filetree.o $(OBJ_DIR)/misc.o $(OBJ_DIR)/operations.o $(OBJ_DIR)/syscalls.o $(OBJ_DIR)/filters.o $(OBJ_DIR)/json_tokens.o $(OBJ_DIR)/command.o $(SRC_DIR)/cmdline.c -o fsc $(LIBS)

clean:
	rm -f fsc $(FILTERS_DIR)/*.o $(OBJ_DIR)/*.o

.PHONY: all clean
